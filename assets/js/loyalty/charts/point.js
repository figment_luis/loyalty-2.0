;
(async (d, c, $) => {
    c('Reporteador Puntos')

    $.get(BASE_URL + '/get-dates-for-points-history', function (response) {
        // Todos las acreditaciones relacionados con este ticket, fueron canceladas
        c(response)
        if (response.code == 200) {
            const selectStartDate = $("#start_date")
            const selectEndDate = $("#end_date")
            $("#selector_fecha").show();
            $.each(response.dates, function(index, value) {
                c(value.date)
                selectStartDate.append(new Option(value.date, value.date));
                selectEndDate.append(new Option(value.date, value.date));
            })
        }
    })

    let tablePuntosInicio = document.querySelector('#puntos-inicio-table');
    let tablePuntosFin = document.querySelector('#puntos-fin-table');

    let dataTablePuntosInicio = new simpleDatatables.DataTable(tablePuntosInicio, {
        labels: {
            placeholder: "Buscar registro",
            perPage: "{select} <label>Registros por página</label>",
            noRows: "No existen registros para mostrar",
            info: "Mostrando {start} a {end} de {rows} registros (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });

    let dataTablePuntosFin = new simpleDatatables.DataTable(tablePuntosFin, {
        labels: {
            placeholder: "Buscar registro",
            perPage: "{select} <label>Registros por página</label>",
            noRows: "No existen registros para mostrar",
            info: "Mostrando {start} a {end} de {rows} registros (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });

    $('#generar-historico').click(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)
        let formData = `start_date=${$("#start_date").val()}&end_date=${$("#end_date").val()}`;

        SA_confirmation_standar('¿Desea generar el reporte histórico?', 'El reporte solicitado se generará a partir de las siguientes fechas: Del ' + $("#start_date").val() + ' al ' + $("#end_date").val(), 'warning').then((result) => {
            if (result.isConfirmed) {
                $('.historico').hide();
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/get-historic`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded",
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    if (data.code == 200) {
                        infoHistoryPoints($("#start_date").val(), $("#end_date").val(), data)
                        fillDataTableFechaIncio(data, $("#start_date").val());
                        fillDataTableFechaFin(data, $("#end_date").val());
                        $('.historico').show();
                        SA_success_html(tplConfirmacionStandar(data.message))
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 500) {
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })

    $(d).on('click', '#btnTerminar', function () {
        // Cerrar modal SweetAlert
        Swal.close()
    });

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    function infoHistoryPoints(start_date, end_date, info) {
        $("#info-start-date")
            .empty()
            .append(`<h6>Información ${start_date}</h6>`)
            .append('<ul>')
            .append(`<li>Disponibles: ${info.inicio_data[0].Disponibles}</li>`)
            .append(`<li>Redimidos: ${info.inicio_data[0].Redimidos}</li>`)
            .append(`<li>Expirados: ${info.inicio_data[0].Expirados}</li>`)
            .append(`<li>En deuda: ${info.inicio_data[0]["En deuda"]}</li>`)
            .append('</ul>')
        $("#info-end-date")
            .empty()
            .append(`<h6>Información ${end_date}</h6>`)
            .append('<ul>')
            .append(`<li>Disponibles: ${info.fin_data[0].Disponibles}</li>`)
            .append(`<li>Redimidos: ${info.fin_data[0].Redimidos}</li>`)
            .append(`<li>Expirados: ${info.fin_data[0].Expirados}</li>`)
            .append(`<li>En deuda: ${info.fin_data[0]["En deuda"]}</li>`)
            .append('</ul>')
    }

    function fillDataTableFechaIncio(data, date) {
        $('.historico-inicio').text('Concentrado de puntos por cliente - Fecha de inicio: ' + date)
        dataTablePuntosInicio.destroy();
        dataTablePuntosInicio.init();
        dataTablePuntosInicio.import({
            type: "json",
            data: JSON.stringify(data.inicio_table)
        });
    }

    function fillDataTableFechaFin(data, date) {
        $('.historico-fin').text('Concentrado de puntos por cliente - Fecha de corte: ' + date)
        dataTablePuntosFin.destroy();
        dataTablePuntosFin.init();
        dataTablePuntosFin.import({
            type: "json",
            data: JSON.stringify(data.fin_table)
        });
    }

    $(".link").click(function (e) {
        e.preventDefault()
        e.stopPropagation()
        let url = $(this).data('url')
        downloadURI(url);
    })

    function downloadURI(uri)
    {
        let link = document.createElement("a");
        link.download = 'Untitled';
        link.href = uri;
        link.click();
    }

    $('#selectorPeriodo').change(function(e) {
        e.preventDefault();
        e.stopPropagation();

        let selectedYear = $(this).val();
        window.location = `${ BASE_URL }/graficas/puntos/${selectedYear}`;
    })
})(document, console.log, jQuery.noConflict())