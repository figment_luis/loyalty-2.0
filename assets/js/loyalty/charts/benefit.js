;
(async (d, c, $) => {
    //c('Reporteador Beneficios')

    let opcionesCertificadosRedimidosPorMes  = {
        series: [
            {
                name: "Socios registrados",
                data: certificados_redimidos_por_mes.data
            }
        ],
        chart: {
            type: "bar",
            height: 450
        },
        plotOptions: {
            bar: {
                horizontal: true
            }
        },
        dataLabels: {
            enabled: true
        },
        xaxis: {
            categories: certificados_redimidos_por_mes.labels
        }
    };
    let opcionesBeneficiosRedimidosPorMes  = {
        series: [
            {
                name: "Socios registrados",
                data: beneficios_redimidos_por_mes.data
            }
        ],
        chart: {
            type: "bar",
            height: 450
        },
        plotOptions: {
            bar: {
                horizontal: true
            }
        },
        dataLabels: {
            enabled: true
        },
        xaxis: {
            categories: beneficios_redimidos_por_mes.labels
        }
    };

    let tableCertificados = document.querySelector('#certificados-table');
    let tableBeneficios = document.querySelector('#beneficios-table');

    let dataTableCertificados = new simpleDatatables.DataTable(tableCertificados, {
        labels: {
            placeholder: "Buscar certificado",
            perPage: "{select} <label>Certificados por página</label>",
            noRows: "No existen certificados para mostrar",
            info: "Mostrando {start} a {end} de {rows} certificados (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableBeneficios = new simpleDatatables.DataTable(tableBeneficios, {
        labels: {
            placeholder: "Buscar beneficio",
            perPage: "{select} <label>Beneficios por página</label>",
            noRows: "No existen beneficios para mostrar",
            info: "Mostrando {start} a {end} de {rows} beneficios (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });

    let graficaCertificadosRedimidosPorMes = await new ApexCharts(document.getElementById('graficaCertificadosRedimidosPorMes'), opcionesCertificadosRedimidosPorMes);
    let graficaBeneficiosRedimidosPorMes = await new ApexCharts(document.getElementById('graficaBeneficiosRedimidosPorMes'), opcionesBeneficiosRedimidosPorMes);

    graficaCertificadosRedimidosPorMes.render()
    graficaBeneficiosRedimidosPorMes.render()

    $(".link").click(function (e) {
        e.preventDefault()
        e.stopPropagation()
        let url = $(this).data('url')
        downloadURI(url);
    })

    function downloadURI(uri)
    {
        let link = document.createElement("a");
        link.download = 'Untitled';
        link.href = uri;
        link.click();
    }

    $('#selectorPeriodo').change(function(e) {
        e.preventDefault();
        e.stopPropagation();

        let selectedYear = $(this).val();
        window.location = `${ BASE_URL }/graficas/beneficios/${selectedYear}`;
    })
})(document, console.log, jQuery.noConflict())