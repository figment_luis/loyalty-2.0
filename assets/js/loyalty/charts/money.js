;
((d, c, $) => {
    c('Graficas dinero')

    let opcionesDineroRedencionesPorGenero  = {
        series: dinero_redenciones_por_genero,
        labels: labels_dinero_redenciones_genero,
        colors: ['#435ebe','#55c6e8'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let opcionesDineroTicketsPorGenero  = {
        series: dinero_tickets_por_genero,
        labels: labels_dinero_tickets_genero,
        colors: ['#435ebe','#55c6e8'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let opcionesDineroTicketsPorNivel  = {
        series: dinero_tickets_por_nivel,
        labels: labels_dinero_tickets_nivel,
        colors: ['#435ebe','#55c6e8', '#f60'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let opcionesDineroRedencionesPorNivel  = {
        series: dinero_redenciones_por_nivel,
        labels: labels_dinero_redenciones_nivel,
        colors: ['#435ebe','#55c6e8', '#f60'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let opcionesDineroTicketsPorMes = {
        series: [
            {
                name: "Monto Total",
                data: dinero_tickets_por_mes.data
            }
        ],
        chart: {
            type: "area",
            height: 450,
            zoom: {
                enabled: false
            }
        },
        stroke: {
            show: true,
            curve: 'smooth'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        markers: {
            size: 7,
            colors: undefined,
            hover: {
                size: undefined,
                sizeOffset: 3
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (value, opts) {
                return new Intl.NumberFormat('es-MX', {style: "currency", currency: "MXN"}).format(parseFloat(value));
            },
            offsetY: -12,
        },
        xaxis: {
            categories: dinero_tickets_por_mes.labels,
        },
        yaxis: {
            labels: {
                formatter: function (value) {
                    return new Intl.NumberFormat('es-MX', {style: "currency", currency: "MXN"}).format(parseFloat(value));
                }
            },
        },
    }
    let opcionesDineroRedencionesPorMes = {
        series: [
            {
                name: "Redenciones por",
                data: dinero_redenciones_por_mes.data
            }
        ],
        chart: {
            type: "area",
            height: 450,
            zoom: {
                enabled: false
            }
        },
        stroke: {
            show: true,
            curve: 'smooth'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        markers: {
            size: 7,
            colors: undefined,
            hover: {
                size: undefined,
                sizeOffset: 3
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (value, opts) {
                return new Intl.NumberFormat('es-MX', {style: "currency", currency: "MXN"}).format(parseFloat(value));
            },
            offsetY: -12,
        },
        xaxis: {
            categories: dinero_redenciones_por_mes.labels,
        },
        yaxis: {
            labels: {
                formatter: function (value) {
                    return new Intl.NumberFormat('es-MX', {style: "currency", currency: "MXN"}).format(parseFloat(value));
                }
            },
        },
    }

    let opcionesDineroTicketsPorRangoDeEdad  = {
        series: dinero_tickets_por_rango_de_edad.data,
        labels: dinero_tickets_por_rango_de_edad.labels,
        colors: ['#F44336','#2196F3', '#FFEB3B', '#009688', '#FF9800'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        },
        dataLabels: {
            enabled: true,
            /*formatter: function (value, opts) {
                return parseFloat(value).toFixed(2);
            },
            offsetY: -12,*/
        },
    }

    let graficaDineroTicketsPorGenero = new ApexCharts(document.getElementById('graficaDineroTicketsPorGenero'), opcionesDineroTicketsPorGenero)
    let graficaDineroRedencionesPorGenero = new ApexCharts(document.getElementById('graficaDineroRedencionesPorGenero'), opcionesDineroRedencionesPorGenero)
    let graficaDineroTicketsPorNivel = new ApexCharts(document.getElementById('graficaDineroTicketsPorNivel'), opcionesDineroTicketsPorNivel)
    let graficaDineroRedencionesPorNivel = new ApexCharts(document.getElementById('graficaDineroRedencionesPorNivel'), opcionesDineroRedencionesPorNivel)
    let graficaDineroTicketsPorMes = new ApexCharts(document.getElementById('graficaDineroTicketsPorMes'), opcionesDineroTicketsPorMes)
    let graficaDineroRedencionesPorMes = new ApexCharts(document.getElementById('graficaDineroRedencionesPorMes'), opcionesDineroRedencionesPorMes)
    let graficaDineroTicketsPorRangoDeEdad = new ApexCharts(document.getElementById('graficaDineroTicketsPorRangoDeEdad'), opcionesDineroTicketsPorRangoDeEdad)

    graficaDineroTicketsPorGenero.render()
    graficaDineroRedencionesPorGenero.render()
    graficaDineroTicketsPorNivel.render()
    graficaDineroRedencionesPorNivel.render()
    graficaDineroTicketsPorMes.render()
    graficaDineroRedencionesPorMes.render()
    graficaDineroTicketsPorRangoDeEdad.render()

    $(".link").click(function (e) {
        e.preventDefault()
        e.stopPropagation()

        // Generar parametros de URL para los rangos de fecha
        let rangeInput = $('input[name="daterange"]').val();
        let format = rangeInput.replaceAll('/', '-')
        let dates = format.split(' - ')
        let pathRangeDates =  dates[0] + '/' + dates[1]
        let url = $(this).data('url') + '/' + pathRangeDates
        SA_confirmation_standar('¿Desea descargar el reporte?', 'El reporte solicitado se generará a partir de las siguientes fechas: Del ' + dates[0] + ' al ' + dates[1], 'warning').then((result) => {
            if (result.isConfirmed) {
                Swal.close()
                downloadURI(url)
            } else {
                SA_cancel()
            }
        })
    })

    function downloadURI(uri)
    {
        let link = document.createElement("a");
        link.download = 'Untitled';
        link.href = uri;
        link.click();
    }

    // Selector de Rango de Fechas
    $('input[name="daterange"]').daterangepicker({
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": " - ",
            "applyLabel": "Aplicar rango",
            "cancelLabel": "Desde el inicio del sistema",
            "fromLabel": "Del",
            "toLabel": "Al",
            "customRangeLabel": "Personalizar",
            "weekLabel": "Semana",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        "cancelClass": "btn-danger",
        "startDate": moment().subtract(1, 'month'),
        "endDate": moment()
    });
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        let fechaActual = moment().format('YYYY/MM/DD')
        let fechaInicio = '2021/01/01'
        $(this).val(fechaInicio + ' - ' + fechaActual);
    });

    $('#selectorPeriodo').change(function(e) {
        e.preventDefault();
        e.stopPropagation();

        let selectedYear = $(this).val();
        window.location = `${ BASE_URL }/graficas/dinero/${selectedYear}`;
    })

})(document, console.log, jQuery.noConflict())