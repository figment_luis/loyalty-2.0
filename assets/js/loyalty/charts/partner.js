;
((d, c, $) => {
    c('Graficas socios')

    let optionsVisitorsProfile  = {
        series: JSON.parse(ser),
        labels: JSON.parse(labels_socios_genero),
        colors: ['#435ebe','#55c6e8'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let optionsLevel  = {
        series: JSON.parse(niveles),
        labels: JSON.parse(labels_socios_nivel),
        colors: ['#435ebe','#55c6e8', '#55741A'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    c(JSON.parse(edades), JSON.parse(labels_socios_edad))
    let optionsedades  = {
        series: JSON.parse(edades),
        labels: JSON.parse(labels_socios_edad),
        colors: ['#A44336', '#F44336','#2196F3', '#FFEB3B', '#009688', '#E91E63', '#FF9800'],
        chart: {
            type: 'donut',
            width: '100%',
            height:'450px'
        },
        legend: {
            position: 'bottom'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '30%'
                }
            }
        }
    }
    let optionRegiones = {
        series: [
            {
                name: "Socios registrados",
                data: regiones.data
            }
        ],
        chart: {
            type: "bar",
            height: 450
        },
        plotOptions: {
            bar: {
                horizontal: true
            }
        },
        dataLabels: {
            enabled: true
        },
        xaxis: {
            categories: regiones.labels
        },
        colors: ['#43A047']
    };
    let optionMeses = {
        series: [
            {
                name: "Socios registrados",
                data: meses.data
            }
        ],
        chart: {
            type: "bar",
            height: 450
        },
        plotOptions: {
            bar: {
                horizontal: true
            }
        },
        dataLabels: {
            enabled: true
        },
        xaxis: {
            categories: meses.labels
        }
    };

    var chartVisitorsProfile = new ApexCharts(document.getElementById('chart-visitors-profile'), optionsVisitorsProfile)
    chartVisitorsProfile.render()
    var chartLevels = new ApexCharts(document.getElementById('chart-levels'), optionsLevel)
    chartLevels.render()
    var chartedades = new ApexCharts(document.getElementById('chart-edades'), optionsedades)
    chartedades.render()
    var charteregiones = new ApexCharts(document.getElementById('chart-regiones'), optionRegiones)
    charteregiones.render()
    var chartMeses = new ApexCharts(document.getElementById('chart-meses'), optionMeses)
    chartMeses.render()

    $(".link").click(function (e) {
        e.preventDefault()
        e.stopPropagation()

        // Generar parametros de URL para los rangos de fecha
        let rangeInput = $('input[name="daterange"]').val();
        let format = rangeInput.replaceAll('/', '-')
        let dates = format.split(' - ')
        let pathRangeDates =  dates[0] + '/' + dates[1]
        let url = $(this).data('url') + '/' + pathRangeDates

        if ($(this).data('select') == 'tienda') {
            let tienda = $("#selectorTienda").val()
            if (!tienda) {
                // todo: mostrar alerta de seleccionar tienda
                SA_error('Seleccione una tienda para proceder a generar el reporte solicitado')
                return true
            }
            url = $(this).data('url') + '/' + tienda + '/' + pathRangeDates
        }
        SA_confirmation_standar('¿Desea descargar el reporte?', 'El reporte solicitado se generará a partir de las siguientes fechas: Del ' + dates[0] + ' al ' + dates[1], 'warning').then((result) => {
            if (result.isConfirmed) {
                Swal.close()
                downloadURI(url)
            } else {
                SA_cancel()
            }
        })
    })


    function downloadURI(uri)
    {
        let link = document.createElement("a");
        link.download = 'Untitled';
        link.href = uri;
        link.click();
    }

    // Selector de Rango de Fechas
    $('input[name="daterange"]').daterangepicker({
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": " - ",
            "applyLabel": "Aplicar rango",
            "cancelLabel": "Desde el inicio del sistema",
            "fromLabel": "Del",
            "toLabel": "Al",
            "customRangeLabel": "Personalizar",
            "weekLabel": "Semana",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        "cancelClass": "btn-danger",
        "startDate": moment().subtract(1, 'month'),
        "endDate": moment()
    });
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        let fechaActual = moment().format('YYYY/MM/DD')
        let fechaInicio = '2021/01/01'
        $(this).val(fechaInicio + ' - ' + fechaActual);
    });

    $('#selectorPeriodo').change(function(e) {
        e.preventDefault();
        e.stopPropagation();

        let selectedYear = $(this).val();
        window.location = `${ BASE_URL }/graficas/socios/${selectedYear}`;
    })

})(document, console.log, jQuery.noConflict())