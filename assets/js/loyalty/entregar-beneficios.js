;
((d, c, $) => {
    // c('Entregar Beneficios')

    let client_id
    let card_id
    let level_id

    $('#btnImprimirTicketAlternative').hide()

    // Registrar socios en el plan de lealtad
    $('#formEntregaDeBeneficios').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)

        let formData = new FormData($(this)[0])
        client_id = formData.get('client_id')
        card_id = formData.get('card_id')
        level_id = formData.get('level_id')
        SA_confirmation_standar('¿Desea entregar el beneficio?', 'Usted esta a un paso de realizar un registro en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/redemption-benefit`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Socio registrado con éxito
                        // Cálculo Total de puntos redimidos
                        /*let $certificadoSeleccionado = $('#slcCertificate :selected')
                        let $puntosCertificado = $certificadoSeleccionado.data('points')
                        totalPuntosRedimidos += Number($puntosCertificado) * Number(formData.get('amount'))*/

                        // Activar botones de acción que aparecen dentro del modal de respuesta
                        $("#btnEntregarMasBeneficios").prop('disabled', false)
                        $("#btnImprimirTicket").prop('disabled', false)
                        // Mostrar modal de respuesta
                        SA_success_html(tplProcesoTerminadoEntregaDeBeneficios($("#info_client").text(), $('#slcBenefit').find(':selected').text()), 565)
                        $('#btnImprimirTicketAlternative').show()
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 412) {
                        // Error ocasionado por un conflicto en las reglas de canje para este certificado
                        // # de canjes por dia, semana, o mes
                        clearValidationErrors()
                        SA_error(data.message)
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })


    /*********************************************************************
     *
     *            Listeners: Botones personalizados para modal de
     *            proceso terminado
     *
     * ******************************************************************/

    $(d).on('click', '#btnImprimirTicket, #btnImprimirTicketAlternative', function (e) {
        $(this).prop('disabled', true)
        $("#btnEntregarMasBeneficios").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Redireccionar al usuario al asistente de impresion
        $.post(BASE_URL + '/imprimir/ticket/entrega-de-beneficios', {client_id: client_id, card_id: card_id}, function (response) {
            // No fue posible generar y registar el ticket en el sistema
            c(response)
            if (response.code == 400) {
                SA_error(response.message);
                return;
            }

            let data = {
                client: response.client,
                level: response.level,
                card_number: response.card_number,
                benefits: response.benefits,
                points: response.points,
                date: response.date,
                concierge: response.concierge,
            }
            let asistenteImpresion = window.open('', '', 'height=400,width=700')
            asistenteImpresion.document.write(plantillaTicketBeneficio(data))
            asistenteImpresion.document.close()
            setTimeout(() => {
                asistenteImpresion.print()
            }, 1000)

            let timerss = setInterval(() => {
                if (asistenteImpresion.closed) {
                    clearInterval(timerss)
                    // Redireccionar al dashboard
                    window.location.replace(BASE_URL + '/socios/panel-de-control')
                }
            }, 100)

        })
    });
    $(d).on('click', '#btnEntregarMasBeneficios', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Preparar Formulario, Actualizar puntos firmes, limpiar vista previa, y solicitar nuevamente los certificados que puede canjear con los puntos sobrantes
        $("#formEntregaDeBeneficios")[0].reset()
        $('#benefit-view').hide()

        // TODO: la entrega de un beneficio no resta puntos al socio

        // Volver a cargar el listado de beneficios, ya que algunos tienen stock
        // Llamar servicio que me retorne el listado de certificados que puede canjear el socio con sus nuevos puntos disponibles
        $.post(BASE_URL + '/get-banefits', {client_id: client_id}, function (response) {
            // Limpiar listado de certificados
            let $listaBeneficios = $('#slcBenefit')
            $listaBeneficios.empty();
            $listaBeneficios.append(new Option('Seleccione un beneficio', '', true, true))
            $.each(response.certificates, function(key, benefit) {
                if (benefit.level_id == level_id) {
                    $listaBeneficios.append($('<option />').val(benefit.id).text(benefit.title).data({
                        'title': benefit.title,
                        'subtitle': benefit.subtitle,
                        'stock': benefit.current_stock,
                        'description': benefit.description,
                        'image': BASE_URL + '/' +benefit.image,
                        'unlimited': benefit.unlimited_stock,
                        'day': benefit.description.rule_days,
                        'week': benefit.description.rule_week,
                        'month': benefit.description.rule_months,
                        'shop': benefit.description.rule_shop,
                    }))
                }
            })
            Swal.close()
        })
    });
    $(d).on('click', '#btnTerminar', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnEntregarMasBeneficios").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Redireccionar al usuario al asistente de impresion
        $.post(BASE_URL + '/imprimir/ticket/entrega-de-beneficios', {client_id: client_id, card_id: card_id}, function (response) {
            window.location.replace(BASE_URL + '/socios/panel-de-control')
        })
    });


    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    /*********************************************************************
     *
     *            Listeners: Actualizar vista previa con información
     *            del produto seleccionado
     *
     * ******************************************************************/

    $('#slcBenefit').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()
        $this = $(this).find(':selected');

        let $view = $('#benefit-view')

        $view.find('h5').text($this.data('title'))
        $view.find('.certificate__subtitle').text($this.data('subtitle'))
        $view.find('p').text($this.data('description'))

        $certificateRules = $('.certificate__rules').hide()
        $list_rules = $view.find('.certificate__rules-item')
        $list_rules.empty().hide()
        if ($this.data('day') || $this.data('week') || $this.data('month') || $this.data('shop')) {
            $certificateRules.show()
            if ($this.data('day'))
                $list_rules.append(`<li>Limitado a ${$this.data('day')} entregas por día.</li>`)
            if ($this.data('week'))
                $list_rules.append(`<li>Limitado a ${$this.data('week')} entregas por semana.</li>`)
            if ($this.data('month'))
                $list_rules.append(`<li>Limitado a ${$this.data('month')} entregas por mes.</li>`)
            if ($this.data('shop'))
                $list_rules.append(`<li>Limitado a socios que hayan acreditado un monto mínimo de $150 en compras durante los últimos 15 días.</li>`)
        } else {
            $certificateRules.hide()
        }

        if (Number($this.data('unlimited'))) {
            $view.find('.certificate__item').html('<i class="fas fa-cubes"></i> Stock ilimitado')
            // Resetear el comporamiento de validación aplicado a este componente
            $("#quantity").val('').attr('max', '').removeClass('is-invalid').removeClass('is-valid').parent().find('.invalid-feedback').hide()
        } else {
            $view.find('.certificate__item').html('<i class="fas fa-cubes"></i> ' + $this.data('stock') + ' Unidades disponibles')
            $("#quantity").val('').attr('max', Number($this.data('stock'))).removeClass('is-invalid').removeClass('is-valid').parent().find('.invalid-feedback').hide()
        }

        $view.find('img').prop('src', $this.data('image'))

        $view.show()

    })

    $('.certificate__rules-title').click(function (e) {
        e.preventDefault()
        e.stopPropagation()
        $(this).next().slideToggle()
    })



    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/

    const inputQuantity = d.getElementById('quantity')
    const selectBenefit = d.getElementById('slcBenefit')

    if (inputQuantity && selectBenefit) {
        addValidation(inputQuantity)
        addValidation(selectBenefit)
    }

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).addClass('is-invalid').parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`).show()
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return ""
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required

    }

})(document, console.log, jQuery.noConflict())