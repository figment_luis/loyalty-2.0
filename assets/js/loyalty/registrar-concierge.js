;
((d, c, $) => {
    //c('Registrar nuevo concierge')

    let client_id

    // Registrar socios en el plan de lealtad
    $('#formRegistrarConcierge').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        if ($('#password').val() !== $('#confirm_password').val()) {
            $('#confirm_password').focus()
            return false
        }

        statusProgress(true)

        let formData = new FormData($(this)[0])
        SA_confirmation_standar('¿Desea registrar este concierge?', 'Usted esta a un paso de realizar un registro en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/concierge-register`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Concierge registrado con éxito
                        SA_success_html(tplConfirmacionRegistarConcierge(data.message))
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        // Manejar opciones para acreditar puntos, ver estado de cuenta o registrar nuevo socio
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })

    $(d).on('click', '#btnRegistrarConcierge', function () {
        // Preparar formulario para un nuevo registro
        $('#formRegistrarConcierge')[0].reset()
        // Cerrar modal SweetAlert
        Swal.close()
    });
    $(d).on('click', '#btnTerminar', function () {
        // Redireccionar al panel de control
        window.location = BASE_URL + '/socios/panel-de-control/'
    });

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/


    const inputFirstName = d.getElementById('first_name')
    const inputLastName = d.getElementById('last_name')
    const inputPassword = d.getElementById('password')
    const inputConfirmPassword = d.getElementById('confirm_password')
    const inputEmail = d.getElementById('email')

    addValidation(inputFirstName)
    addValidation(inputLastName)
    addValidation(inputPassword)
    addValidation(inputConfirmPassword)
    addValidation(inputEmail)

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return element.dataset.bad
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.patternMismatch) return element.dataset.pattern
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.typeMismatch) return element.dataset.type

    }

    /*********************************************************************
     *
     *            Validar Contraseña y Confirmación de Contraseña
     *
     * ******************************************************************/

    let verifyPass = false
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val() && $('#password').val().length >=5 && $('#confirm_password').val().length >= 5) {
            $("#password").removeClass('is-invalid').addClass('is-valid')
            $("#confirm_password").removeClass('is-invalid').addClass('is-valid')
            verifyPass = true
        } else {
            $('#password').removeClass('is-valid').addClass('is-invalid')
            $('#confirm_password').removeClass('is-valid').addClass('is-invalid')
            verifyPass = false
        }
    });

})(document, console.log, jQuery.noConflict())