;
((d, c, $) => {
    // c('Estado de Cuenta')

    let tableTransacciones = document.querySelector('#transacciones-table');
    let tableTarjetas = document.querySelector('#tarjetas-table');
    let tableRedenciones = document.querySelector('#redenciones-table');
    let tableBeneficios = document.querySelector('#beneficios-table');
    let tablePuntosExpirados = document.querySelector('#puntos-expirados-table');

    let dataTableTransacciones = new simpleDatatables.DataTable(tableTransacciones, {
        labels: {
            placeholder: "Buscar transacciones",
            perPage: "{select} <label>Transacciones por página</label>",
            noRows: "No existen transacciones para mostrar",
            info: "Mostrando {start} a {end} de {rows} transacciones (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableTarjetas = new simpleDatatables.DataTable(tableTarjetas, {
        labels: {
            placeholder: "Buscar tarjetas",
            perPage: "{select} <label>Tarjetas por página</label>",
            noRows: "No existen tarjetas para mostrar",
            info: "Mostrando {start} a {end} de {rows} tarjetas (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableRedenciones = new simpleDatatables.DataTable(tableRedenciones, {
        labels: {
            placeholder: "Buscar redención",
            perPage: "{select} <label>Redenciones por página</label>",
            noRows: "No existen redenciones para mostrar",
            info: "Mostrando {start} a {end} de {rows} redenciones (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableBeneficios = new simpleDatatables.DataTable(tableBeneficios, {
        labels: {
            placeholder: "Buscar beneficio",
            perPage: "{select} <label>Beneficios por página</label>",
            noRows: "No existen beneficios para mostrar",
            info: "Mostrando {start} a {end} de {rows} beneficios (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTablePuntosExpirados = new simpleDatatables.DataTable(tablePuntosExpirados, {
        labels: {
            placeholder: "Buscar puntos expirados",
            perPage: "{select} <label>Beneficios por página</label>",
            noRows: "No existen puntos expirados para mostrar",
            info: "Mostrando {start} a {end} de {rows} puntos expirados (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
})(document, console.log, jQuery.noConflict())