;
((d, c, $) => {
    //c('Acreditar Puntos')

    let client_id
    let card_id
    let totalPuntosAcreditados = 0

    $('#btnImprimirTicketAlternative').hide()

    // Acreditar puntos en el plan de lealtad
    $('#formAcreditarPuntos').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)

        let formData = new FormData($(this)[0])
        let infoModal = {
            socio:   $("#info_client").text(),
            tarjeta: $("#info_card").text(),
            monto:   $("#amount").val(),
            tienda:  $("#slcStore option:selected").text(),
            puntos:  $("#puntos-a-acreditar").val(),
        }
        client_id = formData.get('client_id')
        card_id = formData.get('card_id')
        // Mostrar mensaje de notificación
        SA_confirmation_acreditarPuntos(tplNotificacionAcreditarPuntos(infoModal.socio, infoModal.tarjeta, infoModal.monto, infoModal.tienda, new Intl.NumberFormat('es-MX').format(infoModal.puntos))).then((result) => {
            if (result.isConfirmed) {
                // Mostrar indicador de progreso
                SA_progress()
                // Valorar uso de un setTimeout
                $.ajax({
                    url: `${BASE_URL}/receipt-register`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Socio registrado con éxito
                        totalPuntosAcreditados += Number(infoModal.puntos)
                        // Activar botones de acción que aparecen dentro del modal de respuesta
                        $("#btnAcreditarMasPuntos").prop('disabled', false)
                        $("#btnImprimirTicket").prop('disabled', false)
                        // Mostrar modal de respuesta
                        SA_success_html(tplProcesoTerminadoAcreditarPuntos(infoModal.socio, new Intl.NumberFormat('es-MX').format(infoModal.puntos), new Intl.NumberFormat('es-MX').format(totalPuntosAcreditados)))
                        $('#btnImprimirTicketAlternative').show()
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        if (data.errors.encrypted) {
                            SA_error(data.errors.encrypted)
                        } else {
                            // Errores de validación
                            $.each(data.errors, function(key, value) {
                                $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                            })
                        }
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })

    /*********************************************************************
     *
     *            Listeners: Botones personalizados para modal de
     *            proceso terminado
     *
     * ******************************************************************/

    $(d).on('click', '#btnAcreditarMasPuntos', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()
        $('#formAcreditarPuntos')[0].reset()
        // TODO: Llamar a un servicio que me entrege los datos actualizados del socio
        $.get(BASE_URL + '/consultar-puntos/socio/' + client_id, function (response) {
            $('#info_points_available').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_available)) + ' Puntos disponibles')
            $('#info_points_reddeemed').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_redeemed)) + ' Puntos redimidos')
            $('#info_points_total').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_available) + Number(response.points.points_redeemed)) + ' Puntos totales')
            $('#info_level').fadeOut().fadeIn().text('Nivel ' + response.client.level)
            Swal.close()
        })
    })

    $(d).on('click', '#btnImprimirTicket, #btnImprimirTicketAlternative', function (e) {
        $(this).prop('disabled', true)
        $("#btnAcreditarMasPuntos").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()
        $.post(BASE_URL + '/imprimir/ticket/acreditar-puntos', {client_id: client_id, card_id: card_id}, function (response) {
            // No fue posible generar y registar el ticket en el sistema
            c(response);
            if (response.code == 400) {
                SA_error(response.message);
                return;
            }

            let data = {
                client: response.client,
                level: response.level,
                card_number: response.card_number,
                transactions: response.transactions,
                points: response.points,
                date: response.date,
                concierge: response.concierge,
            }
            let asistenteImpresion = window.open('', '', 'height=400,width=700')
            asistenteImpresion.document.write(plantillaTicketAcreditacion(data))
            asistenteImpresion.document.close()
            setTimeout(() => {
                asistenteImpresion.print()
            }, 1000)
            let timerss = setInterval(() => {
                if (asistenteImpresion.closed) {
                    clearInterval(timerss)
                    // Redireccionar al dashboard
                    window.location.replace(BASE_URL + '/socios/panel-de-control')
                }
            }, 100)
        })
    })

    $(d).on('click', '#btnTerminar', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnAcreditarMasPuntos").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()
        $.post(BASE_URL + '/imprimir/ticket/acreditar-puntos', {client_id: client_id, card_id: card_id}, function (response) {
            window.location.replace(BASE_URL + '/socios/panel-de-control')
        })
    })




    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    // Listener para calcular puntos a acreditar y notificar si la cantidad ingresada realmente es superior a $50,000
    $("#amount").on('animationend', function() {
        $("#amount").removeClass('animate__animated animate__shakeX')
    })
    let showToast = true;
    $('#amount').keyup(function (e) {
        // Prevenir que las acciones de esta función se ejecuten al presionar teclas que no sean numéricas
        let k = e.which;
        if ( (k < 48 || k > 57) && (k < 96 || k > 105) && k!=8) {
            e.preventDefault();
            return false;
        }

        // Calcular puntos a acreditar
        let monto = $(this).val().replace(/,/g, '');
        let calculoPuntosAcreditar = (Math.floor(monto / 50)) * parseFloat(FACTOR)
        $('#puntos-a-acreditar').val(calculoPuntosAcreditar)

        // Si el monto de compra es superior a $50,000. Enviar una notificación al usuario si la cantidad ingresada es la correcta
        if (monto > 50000) {
            // Efecto Animate.css
            setTimeout(function() {
                $("#amount").addClass('animate__animated animate__shakeX')
            }, 10)
            // Mostrar mensaje via Toast
            if ($("#amount").val().length <= 6) {
                Toastify({
                    text: `¿Estas seguro que $${$('#amount').val()} es el monto correcto?`,
                    duration: 4000,
                    gravity:"bottom",
                    position: "center",
                    //close:true,
                    backgroundColor: "#008eab",
                    stopOnFocus: true
                }).showToast();
                showToast = true
            } else {
                // Prevenir que se muestren varios mensajes de alerta de forma acoplada
                // El campo solo acepta una longitud de 7 caracteres, y como se está dentro de un evento keyup
                // Esta sección se ejecutaría cada vez que se presione una tecla, saturando así la pantalla de muchos mensajes
                if (showToast) {
                    Toastify({
                        text: `¿Estas seguro que $${$('#amount').val()} es el monto correcto?`,
                        duration: 4000,
                        gravity:"bottom",
                        position: "center",
                        //close:true,
                        backgroundColor: "#008eab",
                        stopOnFocus: true
                    }).showToast();
                }
                // Si el usuario decide presionar más teclas numéricas (aunque no tengan efecto sobre el control), la próxima vez ya no mestro el Toast
                showToast = false;
            }
        } else {
            showToast = true
        }
    })




    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/

    // Fecha de emisión de ticket (Válido por 15 días después de la fecha de compra)
    const controlDate = d.getElementById('date')
    let date_from  = moment().subtract(15, 'day').format('YYYY-MM-DDTHH:mm')
    let date_to = moment().format('YYYY-MM-DDTHH:mm')
    controlDate.min = date_from
    controlDate.max = date_to

    // Monto de la compra
    const cleaveControlAmount = new Cleave('#amount', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    })



    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/

    const inputNumber = d.getElementById('number')
    const inputAmount = d.getElementById('amount')
    const selectStore = d.getElementById('slcStore')
    const inputDate = d.getElementById('date')

    addValidation(inputNumber)
    addValidation(inputAmount)
    addValidation(selectStore)
    addValidation(inputDate)

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return ""
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.patternMismatch) return element.dataset.pattern
    }



})(document, console.log, jQuery.noConflict())