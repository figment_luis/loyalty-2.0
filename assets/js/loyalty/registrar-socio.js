;
((d, c, $) => {
    //c('Registrar nuevo socio')

    let client_id

    // Registrar socios en el plan de lealtad
    $('#formRegistrarUsuario').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        if ($('#password').val() !== $('#confirm_password').val()) {
            $('#confirm_password').focus()
            return false
        }

        // Validar la existencia de una tarjeta en el sistema, implica una validación ajena a HTML5
        if ($("#card_number").hasClass("is-invalid")) return true;

        statusProgress(true)

        let formData = new FormData($(this)[0])
        SA_confirmation_standar('¿Desea registrar este socio?', 'Usted esta a un paso de realizar un registro en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/client-register`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Socio registrado con éxito
                        client_id = data.status.client_id
                        SA_success_html(tplConfirmacionRegistarSocio(data.message))
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        // Manejar opciones para acreditar puntos, ver estado de cuenta o registrar nuevo socio
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })

    // Solicitar número de tarjeta digital
    $('#btnCardDigitalNumber').click(function (e) {
        const $this = $(this);

        e.preventDefault()
        e.stopPropagation()

        $this.prop('disabled', true)

        // Desactivar boton
        $.ajax({
            url: `${BASE_URL}/generate-card-number`,
            type: 'GET',
            dataType: 'json',
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        }).done(function(data, textStatus, jqXHR) {
            // Mostrar código generado
            $('#card_number').val(data.card_number).prop('readonly', true)
            inputCardNumber.dispatchEvent(new Event('input'))
            // Generar campo para especificar el nivel de tarjeta
            $('#levelCard').show()
            $('#slcLevel').prop('disabled', false)
            // Limpiar cualquier error generado previamente en este campo
            //$("[name='card_number']").removeClass('is-invalid').parent().find('.invalid-feedback').remove()

        }).fail(function(jqXHR, textStatus, errorThrown) {
            SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
        }).always(function () {
            //$this.prop('disabled', false)
        })
    })

    // Verifciar que el Número de Tarjeta Física sea válido (se encuentre disponible en el sistema)
    $('#card_number').blur(function (e) {
        e.preventDefault()
        e.stopPropagation()
        $this = $(this)
        if ($this.val().length != 15 || $this.prop('readonly')) return true

        let card_number = $(this).val()
        $.post(BASE_URL + '/validate-card', {number: card_number}, function (response) {
            //c(response)
            if (response.code == 200) {
                if (createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = false
                    $this.removeClass('is-invalid').addClass('is-valid').parent().find('.invalid-feedback').remove()
                }
            } else if (response.code == 404) {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>El número de tarjeta no esta disponible en el sistema</div>`)
                }
            } else if (response.code == 406) {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>El número de tarjeta ya se encuentra asignada a otro socio</div>`)
                }
            } else {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>Servicio no disponible</div>`)
                }
            }
        })
    })


    $(d).on('click', '#btnRegistrarSocio', function () {
        // Preparar formulario para un nuevo registro
        $('#formRegistrarUsuario')[0].reset()

        $('#btnCardDigitalNumber').prop('disabled', false)
        $('#card_number').prop('readonly', false)

        $('#levelCard').hide()
        $('#slcLevel').prop('disabled', true)
        // Cerrar modal SweetAlert
        Swal.close()
    });
    $(d).on('click', '#btnAcreditarPuntos', function () {
        // Redireccionar al usuario a la ventana de acreditar puntos
        window.location = BASE_URL + '/socios/acreditar-puntos/' + client_id
    });
    $(d).on('click', '#btnTerminar', function () {
        // Redireccionar al panel de control
        window.location = BASE_URL + '/socios/panel-de-control/'
    });

    // Cancelar registro
    $('#btnCancel').click(function (e) {
        $('#btnCardDigitalNumber').prop('disabled', false)
        $('#card_number').prop('readonly', false)
        $('#levelCard').hide()
    })

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }
    
    /*********************************************************************
     *
     *            Listeners: Estado / Municipio / Colonia
     *
     * ******************************************************************/

    $('#slcEstado').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()

        // Deshabilitar campos secundarios cuando cambia la selección de estado
        let $municipio = $('#slcMunicipio')
        let $colonia = $('#slcColonia')
        $municipio.prop('disabled', true).removeClass('is-valid').val('')
        $colonia.prop('disabled', true).removeClass('is-valid').val('')

        let stateID = $(this).val()
        $.post(BASE_URL + '/get-municipalities', {state: stateID}, function (response, status) {
            //c(response)
            $municipio.prop('disabled', false)
            $municipio.empty()
            $municipio.append('<option value="" selected disabled>Seleccione un municipio</option>');
            $.each(response.states, function(key, value) {
                $municipio.append(new Option(value.name, value.id, false, false));
            })
        })
    })
    $('#slcMunicipio').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()

        let $colonia = $('#slcColonia')
        $colonia.prop('disabled', true).removeClass('is-valid').val('')

        let municipalityID = $(this).val()
        $.post(BASE_URL + '/get-suburbs', {municipality: municipalityID}, function (response, status) {
            //c(response)
            $colonia.prop('disabled', false)
            $colonia.empty()
            $colonia.append('<option value="" selected disabled>Seleccione una colonia</option>');
            $.each(response.states, function(key, value) {
                $colonia.append(new Option(value.name, value.id, false, false));
            })
        })
    })

    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/

    // Números telefónicos
    /*const cleaveControlMobile = new Cleave('#mobile', {
        phone: true,
        phoneRegionCode: 'MX'
    })
    const cleaveControlTelephone = new Cleave('#telephone', {
        phone: true,
        phoneRegionCode: 'MX'
    })*/

    // Número de tarjeta
    const cleaveControlCardNumber = new Cleave('#card_number', {
        delimiter: '-',
        blocks: [3, 3, 3, 3],
        uppercase: true
    })
    /*const controlDate = d.getElementById('date')
    let date_from  = moment().subtract(15, 'day').format('YYYY-MM-DDTHH:mm')
    let date_to = moment().format('YYYY-MM-DDTHH:mm')
    controlDate.max = date_to
    controlDate.min = date_from*/

    // Monto de la compra

    /*const cleaveControlTelephone = new Cleave('#amount', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    })*/






    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/


    const inputFirstName = d.getElementById('first_name')
    const inputLastName = d.getElementById('last_name')
    const inputCardNumber = d.getElementById('card_number')
    const selectCardLevel = d.getElementById('slcLevel')
    const selectGender = d.getElementById('slcGender')
    const inputBirthday = d.getElementById('birthday')
    const inputPassword = d.getElementById('password')
    const inputConfirmPassword = d.getElementById('confirm_password')
    const selectEstado = d.getElementById('slcEstado')
    const selectMunicipio = d.getElementById('slcMunicipio')
    const selectColonia = d.getElementById('slcColonia')
    const inputCodigoPostal = d.getElementById('codigo_postal')
    const inputCalleAvenida = d.getElementById('street')
    const inputNumExterior = d.getElementById('exterior')
    const inputNumInterior = d.getElementById('interior')
    const inputEmail = d.getElementById('email')
    const inputEmailAlternative = d.getElementById('email_alternative')
    const inputMobile = d.getElementById('mobile')
    const inputTelephone = d.getElementById('telephone')
    const checkTermsAndConditions = d.getElementById('terms_and_conditions')

    addValidation(inputFirstName)
    addValidation(inputLastName)
    addValidationCards(inputCardNumber)
    addValidation(selectCardLevel)
    addValidation(selectGender)
    addValidation(inputBirthday)
    addValidation(inputPassword)
    addValidation(inputConfirmPassword)
    addValidation(selectEstado)
    addValidation(selectMunicipio)
    addValidation(selectColonia)
    addValidation(inputCodigoPostal)
    addValidation(inputCalleAvenida)
    addValidation(inputNumExterior)
    addValidation(inputNumInterior)
    addValidation(inputEmail)
    addValidation(inputEmailAlternative)
    addValidation(inputMobile)
    addValidation(inputTelephone)
    addValidation(checkTermsAndConditions)

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    let createdElementValidationErrorsCard = false // El elemento DOM para mostrar mensajes de validaciópn sobre la existencia de una tarjeta, puede ser creado vía AJAX
    function addValidationCards(element) {
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrorsCard) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrorsCard = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrorsCard) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrorsCard = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrorsCard = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return element.dataset.bad
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.patternMismatch) return element.dataset.pattern
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.typeMismatch) return element.dataset.type

    }



    /*********************************************************************
     *
     *            Validar Contraseña y Confirmación de Contraseña
     *
     * ******************************************************************/

    let verifyPass = false
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val() && $('#password').val().length >=5 && $('#confirm_password').val().length >= 5) {
            $("#password").removeClass('is-invalid').addClass('is-valid')
            $("#confirm_password").removeClass('is-invalid').addClass('is-valid')
            verifyPass = true
        } else {
            $('#password').removeClass('is-valid').addClass('is-invalid')
            $('#confirm_password').removeClass('is-valid').addClass('is-invalid')
            verifyPass = false
        }
    });


    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/

    // Fecha de nacimiento (Solo se pueden registrar socios mayores de edad
    const controlDate = d.getElementById('birthday')
    let date_to  = moment().subtract(18, 'year').format('YYYY-MM-DD')
    let from  = moment().subtract(90, 'year').format('YYYY-MM-DD')
    controlDate.max = date_to
    controlDate.min = from

})(document, console.log, jQuery.noConflict())