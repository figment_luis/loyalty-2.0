;
((d, c, $) => {
    // c('Editar socio')

    let client_id

    // Actualizar socios en el plan de lealtad
    $('#formEditarUsuario').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        if ($('#password').val() !== $('#confirm_password').val()) {
            $('#confirm_password').focus()
            return false
        }

        statusProgress(true)

        let formData = new FormData($(this)[0])
        client_id = formData.get('client_id')
        SA_confirmation_standar('¿Desea actualizar este socio?', 'Usted esta a un paso de realizar una actualización en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/editar-socio`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Socio registrado con éxito
                        SA_success_html(tplConfirmacionEditarSocio(data.message))
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else {
                SA_cancel()
                statusProgress(false)
            }
        })
    })


    $(d).on('click', '#btnAcreditarPuntos', function () {
        // Redireccionar al usuario a la ventana de acreditar puntos
        window.location = BASE_URL + '/socios/acreditar-puntos/' + client_id
    });
    $(d).on('click', '#btnTerminar', function () {
        // Redireccionar al panel de control
        window.location = BASE_URL + '/socios/panel-de-control/'
    });

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    /*********************************************************************
     *
     *            Listeners: Estado / Municipio / Colonia
     *
     * ******************************************************************/

    $('#slcEstado').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()

        // Deshabilitar campos secundarios cuando cambia la selección de estado
        let $municipio = $('#slcMunicipio')
        let $colonia = $('#slcColonia')
        $municipio.prop('disabled', true).removeClass('is-valid').val('')
        $colonia.prop('disabled', true).removeClass('is-valid').val('')

        let stateID = $(this).val()
        $.post(BASE_URL + '/get-municipalities', {state: stateID}, function (response, status) {
            c(response)
            $municipio.prop('disabled', false)
            $municipio.empty()
            $municipio.append('<option value="" selected disabled>Seleccione un municipio</option>');
            $.each(response.states, function(key, value) {
                $municipio.append(new Option(value.name, value.id, false, false));
            })
        })
    })
    $('#slcMunicipio').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()

        let $colonia = $('#slcColonia')
        $colonia.prop('disabled', true).removeClass('is-valid').val('')

        let municipalityID = $(this).val()
        $.post(BASE_URL + '/get-suburbs', {municipality: municipalityID}, function (response, status) {
            c(response)
            $colonia.prop('disabled', false)
            $colonia.empty()
            $colonia.append('<option value="" selected disabled>Seleccione una colonia</option>');
            $.each(response.states, function(key, value) {
                $colonia.append(new Option(value.name, value.id, false, false));
            })
        })
    })

    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/







    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/


    const inputFirstName = d.getElementById('first_name')
    const inputLastName = d.getElementById('last_name')
    const selectGender = d.getElementById('slcGender')
    const inputBirthday = d.getElementById('birthday')
    const selectEstado = d.getElementById('slcEstado')
    const selectMunicipio = d.getElementById('slcMunicipio')
    const selectColonia = d.getElementById('slcColonia')
    const inputCodigoPostal = d.getElementById('codigo_postal')
    const inputCalleAvenida = d.getElementById('street')
    const inputNumExterior = d.getElementById('exterior')
    const inputNumInterior = d.getElementById('interior')
    const inputEmail = d.getElementById('email')
    const inputEmailAlternative = d.getElementById('email_alternative')
    const inputMobile = d.getElementById('mobile')
    const inputTelephone = d.getElementById('telephone')
    const checkTermsAndConditions = d.getElementById('terms_and_conditions')


    addValidation(inputFirstName)
    addValidation(inputLastName)
    addValidation(selectGender)
    addValidation(inputBirthday)
    addValidation(selectEstado)
    addValidation(selectMunicipio)
    addValidation(selectColonia)
    addValidation(inputCodigoPostal)
    addValidation(inputCalleAvenida)
    addValidation(inputNumExterior)
    addValidation(inputNumInterior)
    addValidation(inputEmailAlternative)
    addValidation(inputMobile)
    addValidation(inputTelephone)
    addValidation(checkTermsAndConditions)

    if (inputEmail) {
        addValidation(inputEmail)
    }

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            //c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return element.dataset.bad
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.patternMismatch) return element.dataset.pattern
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.typeMismatch) return element.dataset.type

    }



    /*********************************************************************
     *
     *            Validar Contraseña y Confirmación de Contraseña
     *
     * ******************************************************************/

    let verifyPass = false
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val() && $('#password').val().length >=5 && $('#confirm_password').val().length >= 5) {
            $("#password").removeClass('is-invalid').addClass('is-valid')
            $("#confirm_password").removeClass('is-invalid').addClass('is-valid')
            verifyPass = true
        } else if ($('#password').val().length == 0 && $('#confirm_password').val().length == 0) {
            $("#password").removeClass('is-invalid').removeClass('is-valid')
            $("#confirm_password").removeClass('is-invalid').removeClass('is-valid')
            verifyPass = true
        } else {
            $('#password').removeClass('is-valid').addClass('is-invalid')
            $('#confirm_password').removeClass('is-valid').addClass('is-invalid')
            verifyPass = false
        }
    });

    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/

    // Fecha de nacimiento (Solo se pueden registrar socios mayores de edad
    const controlDate = d.getElementById('birthday')
    let date_to  = moment().subtract(18, 'year').format('YYYY-MM-DD')
    let from  = moment().subtract(90, 'year').format('YYYY-MM-DD')
    controlDate.max = date_to
    controlDate.min = from

})(document, console.log, jQuery.noConflict())