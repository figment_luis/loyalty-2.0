function tplNotificacionAcreditarPuntos(socio, tarjeta, monto, tienda, puntos_a_acreditar)
{
    return `
    <table class="table table-striped table-sm tpl-table" >
    <tbody>
        <tr>
            <td>Socio</td>
            <td>${socio}</td>
        </tr>
        <tr>
            <td>Tarjeta</td>
            <td>${tarjeta}</td>
        </tr>
        <tr>
            <td>Monto de la compra</td>
            <td>$ ${monto}</td>
        </tr>
        <tr>
            <td>Tienda</td>
            <td>${tienda}</td>
        </tr>
        <tr>
            <td>Puntos a acreditar</td>
            <td>${puntos_a_acreditar}</td>
        </tr>
    </tbody>  
    </table>
    `
}

function tplNotificacionCancelarAcreditacion(socio, monto, tienda, puntos_a_cancelar, fecha)
{
    return `
    <table class="table table-striped table-sm tpl-table" >
    <tbody>
        <tr>
            <td>Socio</td>
            <td>${socio}</td>
        </tr>
        <tr>
            <td>Fecha de compra</td>
            <td>${fecha}</td>
        </tr>
        <tr>
            <td>Monto de la compra</td>
            <td>$ ${monto}</td>
        </tr>
        <tr>
            <td>Tienda</td>
            <td>${tienda}</td>
        </tr>
        <tr>
            <td>Puntos a cancelar</td>
            <td>${puntos_a_cancelar}</td>
        </tr>
    </tbody>  
    </table>
    `
}

function tplNotificacionCancelarRedencion(socio, monto, certificado, puntos_a_regresar, fecha)
{
    return `
    <table class="table table-striped table-sm tpl-table" >
    <tbody>
        <tr>
            <td>Socio</td>
            <td>${socio}</td>
        </tr>
        <tr>
            <td>Fecha de operación</td>
            <td>${fecha}</td>
        </tr>
        <tr>
            <td>Certificado</td>
            <td>${certificado}</td>
        </tr>
        <tr>
            <td>Unidades canjeadas</td>
            <td>${monto}</td>
        </tr>
        <tr>
            <td>Puntos a regresar</td>
            <td>${puntos_a_regresar}</td>
        </tr>
    </tbody>  
    </table>
    `
}

function tplNotificacionCancelarBeneficio(socio, monto, beneficio, fecha)
{
    return `
    <table class="table table-striped table-sm tpl-table" >
    <tbody>
        <tr>
            <td>Socio</td>
            <td>${socio}</td>
        </tr>
        <tr>
            <td>Fecha de operación</td>
            <td>${fecha}</td>
        </tr>
        <tr>
            <td>Beneficio</td>
            <td>${beneficio}</td>
        </tr>
        <tr>
            <td>Unidades canjeadas</td>
            <td>${monto}</td>
        </tr>
    </tbody>  
    </table>
    `
}

function tplProcesoTerminadoAcreditarPuntos(socio, puntos_a_acreditar, puntos_acreditados)
{
    return `
    <p>Se han acreditado ${puntos_acreditados} puntos</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Puntos acreditados</td>
                <td>${puntos_a_acreditar}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnAcreditarMasPuntos" class="btn btn-light me-4">Acreditar más puntos al mismo socio</button>
                <button id="btnImprimirTicket" class="btn btn-primary">Imprimir Ticket</button>
            </div>
            <p class="my-3">--- O ---</p>
            <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
        </div>
    </div>
    `
}

function tplProcesoTerminadoCancelarAcreditacion(socio, puntos_cancelados)
{
    return `
    <p>Se han cancelado ${puntos_cancelados} puntos</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Puntos cancelados</td>
                <td>${puntos_cancelados}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnMasOperaciones" class="btn btn-light me-4">Cancelar más operaciones al mismo socio</button>
                <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
            </div>
        </div>
    </div>
    `
}

function tplProcesoTerminadoCancelarRedencion(socio, puntos_a_regresar)
{
    return `
    <p>Se han regresado ${puntos_a_regresar} puntos</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Puntos regresados</td>
                <td>${puntos_a_regresar}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnMasOperaciones" class="btn btn-light me-4">Cancelar más operaciones al mismo socio</button>
                <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
            </div>
        </div>
    </div>
    `
}

function tplProcesoTerminadoCancelarBeneficio(socio, beneficio)
{
    return `
    <p>Se ha cancelado el beneficio</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Beneficio cancelado</td>
                <td>${beneficio}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnMasOperaciones" class="btn btn-light me-4">Cancelar más operaciones al mismo socio</button>
                <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
            </div>
        </div>
    </div>
    `
}

function tplProcesoTerminadoRedenciones(socio, puntos_redimidos, certificado)
{
    return `
    <p>Se han redimido ${puntos_redimidos} puntos</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Certificado entregado</td>
                <td>${certificado}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnRedimirMasPuntos" class="btn btn-light me-4">Redimir más puntos al mismo socio</button>
                <button id="btnImprimirTicket" class="btn btn-primary">Imprimir Ticket</button>
            </div>
            <p class="my-3">--- O ---</p>
            <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
        </div>
    </div>
    `
}

function tplProcesoTerminadoEntregaDeBeneficios(socio, beneficio)
{
    return `
    <p>Se ha entregado el siguiente beneficio ${beneficio}</p>
    <div class="tpl-table-options">
        <table class="table table-striped table-sm tpl-table" >
        <tbody>
            <tr>
                <td>Socio</td>
                <td>${socio}</td>
            </tr>
            <tr>
                <td>Beneficio entregado</td>
                <td>${beneficio}</td>
            </tr>
        </tbody>  
        </table>
        <div class="tpl-options text-center mt-3">
            <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
            <div class="d-flex justify-content-center">
                <button id="btnEntregarMasBeneficios" class="btn btn-light me-4">Entregar más beneficios al mismo socio</button>
                <button id="btnImprimirTicket" class="btn btn-primary">Imprimir Ticket</button>
            </div>
            <p class="my-3">--- O ---</p>
            <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
        </div>
    </div>
    `
}

function tplConfirmacionRegistarSocio(message)
{
    return `
    <p>${message}</p>
    <div class="tpl-options text-center mt-3">
        <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
        <div class="d-flex justify-content-center">
            <button id="btnRegistrarSocio" class="btn btn-light me-4">Registrar nuevo socio</button>
            <button id="btnAcreditarPuntos" class="btn btn-primary">Acreditar puntos</button>
        </div>
        <p class="my-3">--- O ---</p>
        <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
    </div>
    `
}

function tplConfirmacionStandar(message)
{
    return `
    <p>${message}</p>
    <div class="tpl-options text-center mt-3">
        <button id="btnTerminar" class="btn btn-primary">Aceptar</button>
    </div>
    `
}

function tplConfirmacionRegistarConcierge(message)
{
    return `
    <p>${message}</p>
    <div class="tpl-options text-center mt-3">
        <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
        <div class="d-flex justify-content-center">
            <button id="btnRegistrarConcierge" class="btn btn-light me-4">Registrar otro concierge</button>
            <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
        </div>
    </div>
    `
}

function tplConfirmacionEditarSocio(message)
{
    return `
    <p>${message}</p>
    <div class="tpl-options text-center mt-3">
        <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
        <div class="d-flex justify-content-center">
            <button id="btnTerminar" class="btn btn-secondary me-4">Terminar</button>
            <button id="btnAcreditarPuntos" class="btn btn-primary">Acreditar puntos a este socio</button>
        </div>
    </div>
    `
}

function tplConfirmacionCambioDeTarjeta(message)
{
    return `
    <p>${message}</p>
    <div class="tpl-options text-center mt-3">
        <h5 class="my-4">¿Qué deseas hacer ahora?</h5>
        <div class="d-flex justify-content-center">
            <button id="btnConsultarEstadoDeCuenta" class="btn btn-light me-4">Consultar estado de cuenta</button>
            <button id="btnAcreditarPuntos" class="btn btn-primary">Acreditar puntos</button>
        </div>
        <p class="my-3">--- O ---</p>
        <button id="btnTerminar" class="btn btn-secondary m-0">Terminar</button>
    </div>
    `
}


function plantillaTicketAcreditacion(data, reimpresion = false)
{
    let listado = ''
    let puntosAcreditatos = 0
    data.transactions.forEach((transaction) => {
        listado += `<tr><td>${transaction.store} ${Number(transaction.is_canceled) ? ' ----- <strong>(cancelado)</strong> -----' : ''}</td><td>$ ${new Intl.NumberFormat('es-MX').format(Number(transaction.amount).toFixed(2))}</td><td>${new Intl.NumberFormat('es-MX').format(transaction.points)}</td></tr>`
        puntosAcreditatos += Number(transaction.points)
    })
    const HEADERS = `
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Nivel: </strong><span class="uppercase">${data.level}</span></p>
        <p><strong>Puntos disponibles: </strong><span class="uppercase">${data.points}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora: </strong><span class="uppercase">${data.date}</span></p>
    `
    const HEADERS_REPRINT = `
        <p class="reprint"><strong>REIMPRESIÓN DE TICKET CON FECHA DEL: ${data.old_date}</strong></p>
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora de reimpresion: </strong><span class="uppercase">${data.date}</span></p>
    `
    const TEMPLATE =  `
<!DOCTYPE html>
<html>
    <head>
        <title>Ticket Loyalty 2.0</title>
        <link rel="stylesheet" href="${BASE_URL}/assets/css/loyalty/ticket-acreditaciones.css">
    </head>
    <body>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        ${ reimpresion ? HEADERS_REPRINT : HEADERS }
        <table>
            <thead>
                <tr>
                    <th>Tienda</th>
                    <th>Monto de la compra</th>
                    <th>Puntos acreditados</th>
               </tr>
            </thead>
            <tbody>
                ${listado}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"><strong>Total de puntos acreditados por esta transacción</strong></td>
                    <td><strong>${new Intl.NumberFormat('es-MX').format(puntosAcreditatos)}</strong></td>
                </tr>
            </tfoot>
        </table>
        <div class="firma">
            <hr>
            <p>FIRMA CLIENTE</p>
        </div>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        <div class="footer">
            <p class=""><strong>Dirección: </strong>${APP_TICKET_ADDRESS}</p>
            <p>${APP_TICKET_URL}</p>
            <p>${APP_TICKET_EMAIL}</p>
            <p class="copyright">${APP_TICKET_COPYRIGHT}</p>
        </div>
        </body>
 </html>`
    return TEMPLATE
}


function plantillaTicketRedencion(data, reimpresion = false)
{
    let listado = ''
    data.certificates.forEach((certificate) => {
        listado += `<tr><td>${certificate.title} ${Number(certificate.is_canceled) ? ' ----- <strong>(cancelado)</strong> -----' : ''}</td><td>${certificate.amount}</td><td>${new Intl.NumberFormat('es-MX').format(certificate.points)}</td></tr>`
    })
    const HEADERS = `
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Nivel: </strong><span class="uppercase">${data.level}</span></p>
        <p><strong>Puntos disponibles: </strong><span class="uppercase">${data.points}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora: </strong><span class="uppercase">${data.date}</span></p>
    `
    const HEADERS_REPRINT = `
        <p class="reprint"><strong>REIMPRESIÓN DE TICKET CON FECHA DEL: ${data.old_date}</strong></p>
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora de reimpresion: </strong><span class="uppercase">${data.date}</span></p>
    `
    const TEMPLATE =  `
<!DOCTYPE html>
<html>
    <head>
        <title>Ticket Loyalty 2.0</title>
        <link rel="stylesheet" href="${BASE_URL}/assets/css/loyalty/ticket-redenciones.css">
    </head>
    <body>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        ${ reimpresion ? HEADERS_REPRINT : HEADERS }
        <table>
            <thead>
                <tr>
                    <th>Artículo redimido</th>
                    <th>Cantidad</th>
                    <th>Puntos</th>
               </tr>
            </thead>
            <tbody>
                ${listado}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"><strong>Total de puntos redimidos por esta transacción</strong></td>
                    <td><strong>${data.points_redeemed}</strong></td>
                </tr>
            </tfoot>
        </table>
        <div class="firma">
            <hr>
            <p>FIRMA CLIENTE</p>
        </div>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        <div class="footer">
            <p class=""><strong>Dirección: </strong>${APP_TICKET_ADDRESS}</p>
            <p>${APP_TICKET_URL}</p>
            <p>${APP_TICKET_EMAIL}</p>
            <p class="copyright">${APP_TICKET_COPYRIGHT}</p>
        </div>
        </body>
 </html>
        `
    return TEMPLATE
}


function plantillaTicketBeneficio(data, reimpresion = false)
{
    let listado = ''
    data.benefits.forEach((benefit) => {
        listado += `<tr><td>${benefit.title} ${Number(benefit.is_canceled) ? ' ----- <strong>(cancelado)</strong> -----' : ''}</td><td>${benefit.subtitle}</td><td>${benefit.amount}</td></tr>`
    })
    const HEADERS = `
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Nivel: </strong><span class="uppercase">${data.level}</span></p>
        <p><strong>Puntos disponibles: </strong><span class="uppercase">${data.points}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora: </strong><span class="uppercase">${data.date}</span></p>
    `
    const HEADERS_REPRINT = `
        <p class="reprint"><strong>REIMPRESIÓN DE TICKET CON FECHA DEL: ${data.old_date}</strong></p>
        <p><strong>Cliente: </strong><span class="uppercase">${data.client}</span></p>
        <p><strong>Tarjeta: </strong><span class="uppercase">${data.card_number}</span></p>
        <p><strong>Concierge: </strong><span class="uppercase">${data.concierge}</span></p>
        <p><strong>Fecha y hora de reimpresión: </strong><span class="uppercase">${data.date}</span></p>
    `
    const TEMPLATE =  `
<!DOCTYPE html>
<html>
    <head>
        <title>Ticket Loyalty 2.0</title>
        <link rel="stylesheet" href="${BASE_URL}/assets/css/loyalty/ticket-beneficios.css">
    </head>
    <body>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        ${ reimpresion ? HEADERS_REPRINT : HEADERS }
        <table>
            <thead>
                <tr>
                    <th>Beneficio entregado</th>
                    <th>Tienda participante</th>
                    <th>Cantidad</th>
               </tr>
            </thead>
            <tbody>
                ${listado}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"><strong>Recuerda que tus compras se convertien en beneficios</strong></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        <div class="firma">
            <hr>
            <p>FIRMA CLIENTE</p>
        </div>
        <img src="${BASE_URL}/assets/images/logotipo-ticket.svg" alt="Logotipo">
        <div class="footer">
            <p class=""><strong>Dirección: </strong>${APP_TICKET_ADDRESS}</p>
            <p>${APP_TICKET_URL}</p>
            <p>${APP_TICKET_EMAIL}</p>
            <p class="copyright">${APP_TICKET_COPYRIGHT}</p>
        </div>
        </body>
 </html>
        `
    return TEMPLATE
}