;
((d, c, $) => {
    //c('Redenciones')

    let totalPuntosRedimidos = 0
    let client_id
    let card_id

    $('#btnImprimirTicketAlternative').hide()

    // Registrar redención en el plan de lealtad
    $('#formRedenciones').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)

        let formData = new FormData($(this)[0])
        client_id = formData.get('client_id')
        card_id = formData.get('card_id')
        SA_confirmation_standar('¿Desea acreditar el beneficio?', 'Usted esta a un paso de realizar un registro en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/redemption-register`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Redención registrada con éxito
                        // Cálculo Total de puntos redimidos
                        let $certificadoSeleccionado = $('#slcCertificate :selected')
                        let $puntosCertificado = $certificadoSeleccionado.data('points')
                        totalPuntosRedimidos += Number($puntosCertificado) * Number(formData.get('amount'))
                        // Activar botones de acción que aparecen dentro del modal de respuesta
                        $("#btnRedimirMasPuntos").prop('disabled', false)
                        $("#btnImprimirTicket").prop('disabled', false)
                        // Mostrar modal de respuesta
                        SA_success_html(tplProcesoTerminadoRedenciones($("#info_client").text(), new Intl.NumberFormat('es-MX').format(totalPuntosRedimidos), $('#slcCertificate').find(':selected').text()))
                        $('#btnImprimirTicketAlternative').show()
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })


    /*********************************************************************
     *
     *            Listeners: Botones personalizados para modal de
     *            proceso terminado
     *
     * ******************************************************************/

    $(d).on('click', '#btnImprimirTicket, #btnImprimirTicketAlternative', function (e) {
        $(this).prop('disabled', true)
        $("#btnRedimirMasPuntos").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Redireccionar al usuario al asistente de impresion
        $.post(BASE_URL + '/imprimir/ticket/redenciones', {client_id: client_id, card_id: card_id, points_redeemed: totalPuntosRedimidos}, function (response) {
            // No fue posible generar y registar el ticket en el sistema
            if (response.code == 400) {
                SA_error(response.message);
                return;
            }

            let data = {
                client: response.client,
                level: response.level,
                card_number: response.card_number,
                certificates: response.certificates,
                points: response.points,
                date: response.date,
                concierge: response.concierge,
                points_redeemed: new Intl.NumberFormat('es-MX').format(Number(totalPuntosRedimidos))
            }
            let asistenteImpresion = window.open('', '', 'height=400,width=700')
            asistenteImpresion.document.write(plantillaTicketRedencion(data))
            asistenteImpresion.document.close()
            setTimeout(() => {
                asistenteImpresion.print()
            }, 1000)

            let timerss = setInterval(() => {
                if (asistenteImpresion.closed) {
                    clearInterval(timerss)
                    // Redireccionar al dashboard
                    window.location.replace(BASE_URL + '/socios/panel-de-control')
                }
            }, 100)

        })
    });
    $(d).on('click', '#btnRedimirMasPuntos', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Preparar Formulario, Actualizar puntos firmes, limpiar vista previa, y solicitar nuevamente los certificados que puede canjear con los puntos sobrantes
        $("#formRedenciones")[0].reset()
        $('#certificate-view').hide()

        // TODO: Llamar a un servicio que me entrege los datos actualizados del socio
        $.get(BASE_URL + '/consultar-puntos/socio/' + client_id, function (responsePointsAvailable) {
            $('#info_points_available').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(responsePointsAvailable.points.points_available)) + ' Puntos disponibles')
            $('#info_points_reddeemed').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(responsePointsAvailable.points.points_redeemed)) + ' Puntos redimidos')
            $('#info_points_total').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(responsePointsAvailable.points.points_available) + Number(responsePointsAvailable.points.points_redeemed)) + ' Puntos totales')
            $('#info_level').fadeOut().fadeIn().text('Nivel ' + responsePointsAvailable.client.level)

            // c(responsePointsAvailable)
            // Llamar servicio que me retorne el listado de certificados que puede canjear el socio con sus nuevos puntos disponibles
            $.post(BASE_URL + '/get-certificates', {client_id: client_id}, function (response) {
                // Limpiar listado de certificados
                let $listaCertificados = $('#slcCertificate')
                $listaCertificados.empty();
                $listaCertificados.append(new Option('Seleccione un certificado', '', true, true))
                $.each(response.certificates, function(key, certificate) {
                    $listaCertificados.append($('<option />').val(certificate.id).text(certificate.title).data({
                        'title': certificate.title,
                        'subtitle': certificate.subtitle,
                        'description': certificate.description,
                        'image': BASE_URL + '/' +certificate.image,
                        'points': certificate.points,
                        'stock': certificate.current_stock,
                        'points-available': responsePointsAvailable.points.points_available,
                    }))
                })
                //c(response)
                Swal.close()
            })
        })
    });
    $(d).on('click', '#btnTerminar', function (e) {
        $(this).prop('disabled', true)
        $("#btnImprimirTicket").prop('disabled', true)
        $("#btnRedimirMasPuntos").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // Redireccionar al usuario al asistente de impresion
        $.post(BASE_URL + '/imprimir/ticket/redenciones', {client_id: client_id, card_id: card_id, points_redeemed: totalPuntosRedimidos}, function (response) {
            window.location.replace(BASE_URL + '/socios/panel-de-control')
        })
    })


    // Cancelar registro
    $('#btnCancel').click(function (e) {
        $('#btnCardDigitalNumber').prop('disabled', false)
        $('#card_number').prop('readonly', false)
        $('#levelCard').hide()
    })

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    /*********************************************************************
     *
     *            Listeners: Actualizar vista previa con información
     *            del certificado seleccionado
     *
     * ******************************************************************/

    $('#slcCertificate').on('change', function (e) {
        e.preventDefault()
        e.stopPropagation()
        $this = $(this).find(':selected');

        let $view = $('#certificate-view')

        $view.find('h5').text($this.data('title'))
        $view.find('h6').text($this.data('subtitle'))
        $view.find('p').text($this.data('description'))
        $view.find('li:first').html('<i class="fas fa-coins"></i> Puntos: ' + $this.data('points'))

        if (Number($this.data('stock')) === 1) {
            $view.find('li:last').html('<i class="fas fa-cubes"></i> ' + $this.data('stock') + ' Unidad disponible')
        } else {
            $view.find('li:last').html('<i class="fas fa-cubes"></i> ' + $this.data('stock') + ' Unidades disponibles')
        }
        $view.find('img').prop('src', $this.data('image'))

        $view.show()

        // Preparar componente
        let cantidadMaxima = calcularCantidadMaximaParaRedimir($this.data('points-available'), $this.data('points'))
        // c('Cantidad máxima', cantidadMaxima, 'Puntos disponibles', $("#info_points_available").text(), 'Puntos del certificado', $this.data('points'))

        if (cantidadMaxima > $this.data('stock')) {
            cantidadMaxima = Number($this.data('stock'));
        }
        // c('Cantidad máxima', cantidadMaxima, 'Puntos disponibles', $("#info_points_available").text(), 'Puntos del certificado', $this.data('points'))

        // Resetear el comportamiento de validación aplicado a este componente. El mensaje solo se oculta
        $("#quantity").val('').attr('max', cantidadMaxima).removeClass('is-invalid').removeClass('is-valid').parent().find('.invalid-feedback').hide()
        $("#points_to_redimir").val('')
        /*c(Number($this.data('points')), Number($("#quantity").val()))
        let calculo = Number($this.data('points')) * Number($("#quantity").val())
        $("#points_to_redimir").val(calculo)*/
    })

    $("#quantity").change(function(e) {
        let points = Number($('#slcCertificate').find(':selected').data('points'))
        let calculo = points * $(this).val()
        $("#points_to_redimir").val(calculo)
    })


    /*********************************************************************
     *
     *            Calcular cuantos certificados del tipo
     *            seleccionado puede redimir un usuario con base
     *            a sus puntos disponbibles
     *
     * ******************************************************************/
    function calcularCantidadMaximaParaRedimir($points_user, $points_certificate)
    {
        return Math.trunc(Number($points_user) / Number($points_certificate))
    }


    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/

    const inputQuantity = d.getElementById('quantity')
    const selectCertificate = d.getElementById('slcCertificate')

    if (inputQuantity && selectCertificate) {
        addValidation(inputQuantity)
        addValidation(selectCertificate)
    }


    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    // Se agrega nuevamente la clase dado que estos elementos pueden resetearse y volver a su estado original. En ese sentido solo ocultamos el mensaje
                    $(element).addClass('is-invalid').parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`).show()
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return ""
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required

    }

})(document, console.log, jQuery.noConflict())