;
((d, c, $) => {
    //c('Reimprimir Tickets')

    let tableRedenciones = document.querySelector('#redenciones-table');
    let tableBeneficios = document.querySelector('#beneficios-table');
    let tableTransacciones = document.querySelector('#transacciones-table');

    let dataTableRedenciones = new simpleDatatables.DataTable(tableRedenciones, {
        labels: {
            placeholder: "Buscar ticket",
            perPage: "{select} <label>Tickets por página</label>",
            noRows: "No existen tickets para mostrar",
            info: "Mostrando {start} a {end} de {rows} tickets (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableBeneficios = new simpleDatatables.DataTable(tableBeneficios, {
        labels: {
            placeholder: "Buscar ticket",
            perPage: "{select} <label>Tickets por página</label>",
            noRows: "No existen tickets para mostrar",
            info: "Mostrando {start} a {end} de {rows} tickets (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableTransacciones = new simpleDatatables.DataTable(tableTransacciones, {
        labels: {
            placeholder: "Buscar ticket",
            perPage: "{select} <label>Tickets por página</label>",
            noRows: "No existen tickets para mostrar",
            info: "Mostrando {start} a {end} de {rows} tickets (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });


    $(d).on('click', '.btnReimprimirTicketCertificado', function (e) {
        e.preventDefault()
        e.stopPropagation()

        let ticket_id = $(this).data('ticket')
        let client = $(this).data('client')
        let card_number = $(this).data('card_number')
        let points_redeemed = $(this).data('points_redeemed')
        let old_date = $(this).data('old_date')
        SA_confirmation_standar('¿Desea reimprimir este ticket?', 'Solo debería imprimir este ticket si realmente es necesario').then((result) => {
            if (result.isConfirmed) {
                // Reimprimnir
                $.post(BASE_URL + '/reimprimir/ticket/redencion', {ticket_id: ticket_id}, function (response) {
                    // Todos los certificados relacionados con este ticket, fueron cancelados
                    if (response.certificates.length == 0) {
                        SA_error('Los certificados relacionados con este ticket fueron cancelados en el sistema');
                        return;
                    }

                    let data = {
                        client: client,
                        card_number: card_number,
                        certificates: response.certificates,
                        date: response.date,
                        old_date: old_date,
                        concierge: response.concierge,
                        points_redeemed: points_redeemed,
                    }
                    let asistenteImpresion = window.open('', '', 'height=400,width=700')
                    asistenteImpresion.document.write(plantillaTicketRedencion(data, true))
                    asistenteImpresion.document.close()
                    setTimeout(() => {
                        asistenteImpresion.print()
                    }, 1000)
                    let timerss = setInterval(() => {
                        if (asistenteImpresion.closed) {
                            clearInterval(timerss)
                            // Redireccionar al dashboard
                            window.location.replace(BASE_URL + '/socios/panel-de-control')
                        }
                    }, 100)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })

    $(d).on('click', '.btnReimprimirTicketBeneficio', function (e) {
        e.preventDefault()
        e.stopPropagation()

        let ticket_id = $(this).data('ticket')
        let client = $(this).data('client')
        let card_number = $(this).data('card_number')
        let old_date = $(this).data('old_date')
        SA_confirmation_standar('¿Desea reimprimir este ticket?', 'Solo debería imprimir este ticket si realmente es necesario').then((result) => {
            if (result.isConfirmed) {
                // Reimprimnir
                $.post(BASE_URL + '/reimprimir/ticket/beneficio', {ticket_id: ticket_id}, function (response) {
                    // Todos los beneficios relacionados con este ticket, fueron cancelados
                    if (response.benefits.length == 0) {
                        SA_error('Los beneficios relacionados con este ticket fueron cancelados en el sistema');
                        return;
                    }

                    let data = {
                        client: client,
                        card_number: card_number,
                        benefits: response.benefits,
                        date: response.date,
                        old_date: old_date,
                        concierge: response.concierge,
                    }
                    let asistenteImpresion = window.open('', '', 'height=400,width=700')
                    asistenteImpresion.document.write(plantillaTicketBeneficio(data, true))
                    asistenteImpresion.document.close()
                    setTimeout(() => {
                        asistenteImpresion.print()
                    }, 1000)
                    let timerss = setInterval(() => {
                        if (asistenteImpresion.closed) {
                            clearInterval(timerss)
                            // Redireccionar al dashboard
                            window.location.replace(BASE_URL + '/socios/panel-de-control')
                        }
                    }, 100)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })

    $(d).on('click', '.btnReimprimirTicketAcreditacionPuntos', function (e) {
        e.preventDefault()
        e.stopPropagation()

        let ticket_id = $(this).data('ticket')
        let client = $(this).data('client')
        let card_number = $(this).data('card_number')
        let credited_points = $(this).data('credited_points')
        let old_date = $(this).data('old_date')
        SA_confirmation_standar('¿Desea reimprimir este ticket?', 'Solo debería imprimir este ticket si realmente es necesario').then((result) => {
            if (result.isConfirmed) {
                // Reimprimnir
                $.post(BASE_URL + '/reimprimir/ticket/acreditacion', {ticket_id: ticket_id}, function (response) {
                    // Todos las acreditaciones relacionados con este ticket, fueron canceladas
                    c(response)
                    if (response.transactions.length == 0) {
                        SA_error('Las acreditaciones relacionadas con este ticket fueron canceladas en el sistema');
                        return;
                    }

                    let data = {
                        client: client,
                        card_number: card_number,
                        transactions: response.transactions,
                        date: response.date,
                        old_date: old_date,
                        concierge: response.concierge,
                        credited_points: credited_points,
                    }
                    let asistenteImpresion = window.open('', '', 'height=400,width=700')
                    asistenteImpresion.document.write(plantillaTicketAcreditacion(data, true))
                    asistenteImpresion.document.close()
                    setTimeout(() => {
                        asistenteImpresion.print()
                    }, 1000)
                    let timerss = setInterval(() => {
                        if (asistenteImpresion.closed) {
                            clearInterval(timerss)
                            // Redireccionar al dashboard
                            window.location.replace(BASE_URL + '/socios/panel-de-control')
                        }
                    }, 100)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })
})(document, console.log, jQuery.noConflict())