;
((d, c, $) => {
    //c('Buscador de socios')

    let title = null
    let url = null
    let url_redirect = null

    // Ocultar por defecto el listado de coincidencias
    $('#searchModal #modalBodyResults').hide()

    // Resetear formulario y comportamiento de opciones por defecto en el modal de búsqueda
    $("#searchModal").on("hidden.bs.modal", function(){
        $(this).find('form')[0].reset();
        clearMessagesError()
        $('#searchModal #modalBodyResults').hide()
    });

    $("#searchModal").on("show.bs.modal", function(event){
        let button   = event.relatedTarget
        url_redirect = button.getAttribute('data-bs-redirect')
        url          = BASE_URL + '/' + button.getAttribute('data-bs-url')
        title        = button.getAttribute('data-bs-title')
        $(this).find('.modal-title').text(title)
    });

    // Limpiar caja de texto y ocultar resultados al seleccionar un tipo de busqueda diferente
    $('#searchModal input[type="radio"]').change(function (e) {
        $('#searchModal #query').val('')
        $('#searchModal #modalBodyResults').hide()
    })

    // Buscar socio con base al criterio de búsqueda seleccionado
    $('#searchModal form').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)

        let formData = new FormData($(this)[0])
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        }).done(function(data, textStatus, jqXHR) {
            clearMessagesError()
            if (data.code == 200) {
                // Para consultas de tipo Nombre o Teléfono, mostrar en el modal el listado de coincidencias
                let typeSearch = formData.get('type')
                if (typeSearch == 'name' || typeSearch == 'telephone') {
                    $('#searchModal tbody').empty();
                    // Generar listado de coincidencias
                    $.each(data.results, function(key, user) {
                        // c(user)
                        $('#searchModal tbody').append(`
                        <tr>
                            <td>${user.nombre}</td>
                            <td>${user.email}</td>
                            <td>${user.telephone}</td>
                            <td><a href="${BASE_URL}/${url_redirect}/${user.client_id}" class="btn btn-sm btn-primary">Seleccionar</a></td>
                        </tr>
                        `)
                    })

                    $('#searchModal #modalBodyResults').show()
                } else {
                    // Tarjeta localizada en el sistema (Mostrar GUI de Listado de Premios)
                    window.location = `${BASE_URL}/${url_redirect}/${data.results[0].client_id}`
                }
            } else if (data.code == 404) {
                // No se localizó el cliente con los datos proporcionados
                $("#searchModal #search-messages").show().html('<i class="fas fa-exclamation-triangle me-2"></i>' + data.message)
            } else if (data.code == 500) {
                // Error interno en el servidor
                $("#searchModal #search-messages").show().html('<i class="fas fa-exclamation-triangle me-2"></i> Servicio no disponible')
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // Error interno en el servidor
            $("#searchModal #search-messages").show().html('<i class="fas fa-exclamation-triangle me-2"></i> Servicio no disponible')
        }).always(function () {
            statusProgress(false)
        })
    })

    // Limpiar mensajes de error de validación
    function clearMessagesError()
    {
        $("#searchModal #search-messages").hide().text('')
    }

    // Establecer
    function statusProgress(show)
    {
        if (show) {
            $('#searchModal #btnSearch').prop('disabled', true)
            //$('#loader').show()
        } else {
            $('#searchModal #btnSearch').prop('disabled', false)
            //$('#loader').hide()
        }
    }

})(document, console.log, jQuery.noConflict())