;
((d, c, $) => {
    $('#formLogin').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        statusProgress(true)

        let formData = new FormData($(this)[0])
        $.ajax({
            url: `${BASE_URL}/login-validation`,
            type: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        }).done(function(data, textStatus, jqXHR) {
            clearValidationErrors()
            if (data.code == 200) {
                // Credenciales de acceso correctas
                window.location = `${BASE_URL}/socios/panel-de-control`
            } else if (data.code == 400) {
                // Error con las credenciales de acceso
                $('#login-messages').text(data.message);
            } else if (data.code == 406) {
                // Errores de validación
                $.each(data.validation, function(key, value) {
                    $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                })
            } else if (data.code == 500) {
                clearValidationErrors()
                SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            clearValidationErrors()
            SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
        }).always(function () {
            statusProgress(false)
        })
    })

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').hide();
        $('.login-messages').text('');
        $('.is-invalid').removeClass('is-invalid')
    }

    // Establecer comportamiento para el procesamiento de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnLogin').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnLogin').prop('disabled', false)
            $('#loader').hide()
        }
    }

    const inputEmail = d.getElementById('email')
    const inputPassword = d.getElementById('password')

    addValidation(inputEmail)
    addValidation(inputPassword)

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                } else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return ""
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.patternMismatch) return element.dataset.pattern
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.typeMismatch) return element.dataset.type

    }
})(document, console.log, jQuery.noConflict())