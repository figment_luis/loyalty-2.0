;
((d, c, $) => {
    //c('Cambiar Tarjeta')

    let tipoTarjeta = 'Física';

    // Registrar socios en el plan de lealtad
    $('#formCambiarTarjeta').submit(function (e) {
        e.preventDefault()
        e.stopPropagation()

        // Validar la existencia de una tarjeta en el sistema, implica una validación ajena a HTML5
        if ($("#card_number").hasClass("is-invalid")) return true;

        statusProgress(true)

        let formData = new FormData($(this)[0])
        formData.set('card_type', tipoTarjeta);
        SA_confirmation_standar('¿Desea cambiar la tarjeta?', 'Usted esta a un paso de realizar una modificación en el sistema', 'warning').then((result) => {
            if (result.isConfirmed) {
                SA_progress()
                // Llamada AJAX
                $.ajax({
                    url: `${BASE_URL}/cambiar-tarjeta`,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).done(function(data, textStatus, jqXHR) {
                    clearValidationErrors()
                    if (data.code == 200) {
                        // Cambio de tarjeta éxitoso
                        SA_success_html(tplConfirmacionCambioDeTarjeta(data.message))
                        // Adjuntar eventos para botones personalizados en el template de SweetAlert
                        $(d).on('click', '#btnConsultarEstadoDeCuenta', function () {
                            // Redireccionar al usuario a la ventana de estado de cuenta
                            window.location = BASE_URL + '/socios/estado-de-cuenta/' + formData.get('client_id')
                        });
                        $(d).on('click', '#btnAcreditarPuntos', function () {
                            // Redireccionar al usuario a la ventana de acreditar puntos
                            window.location = BASE_URL + '/socios/acreditar-puntos/' + formData.get('client_id')
                        });
                        $(d).on('click', '#btnTerminar', function () {
                            // Redireccionar al panel de control
                            window.location = BASE_URL + '/socios/panel-de-control/'
                        });


                        /*$('#formCambiarTarjeta')[0].reset()

                        $('#btnCardDigitalNumber').prop('disabled', false)
                        $('#card_number_current').val(formData.get('card_number'))*/
                    } else if (data.code == 400) {
                        // Error del tipo 400 del lado del servidor (fuera de servicio)
                        SA_error(data.message)
                    } else if (data.code == 406) {
                        // Errores de validación
                        SA_error(data.message)
                        $.each(data.errors, function(key, value) {
                            $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${value}</div>`)
                        })
                    } else if (data.code == 500) {
                        clearValidationErrors()
                        SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    clearValidationErrors()
                    SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
                }).always(function () {
                    statusProgress(false)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
                statusProgress(false)
            }
        })
    })

    // Solicitar número de tarjeta digital
    $('#btnCardDigitalNumber').click(function (e) {
        const $this = $(this);
        e.preventDefault()
        e.stopPropagation()
        // Desactivar boton
        $this.prop('disabled', true)
        $.ajax({
            url: `${BASE_URL}/generate-card-number`,
            type: 'GET',
            dataType: 'json',
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        }).done(function(data, textStatus, jqXHR) {
            // Mostrar código generado
            $('#card_number').val(data.card_number).prop('readonly', true)
            inputCardNumber.dispatchEvent(new Event('input'))
            tipoTarjeta = 'Digital';
        }).fail(function(jqXHR, textStatus, errorThrown) {
            SA_error('Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.')
            // Hanilitar botón para hacer más intentos
            $this.prop('disabled', false)
        }).always(function () {

        })
    })

    // Verifciar que el Número de Tarjeta Física sea válido (se encuentre disponible en el sistema)
    $('#card_number').blur(function (e) {
        e.preventDefault()
        e.stopPropagation()
        $this = $(this)
        if ($this.val().length != 15 || $this.prop('readonly')) return true

        let card_number = $(this).val()
        $.post(BASE_URL + '/validate-card', {number: card_number}, function (response) {
            //c(response)
            if (response.code == 200) {
                if (createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = false
                    $this.removeClass('is-invalid').addClass('is-valid').parent().find('.invalid-feedback').remove()
                }
            } else if (response.code == 404) {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>El número de tarjeta no esta disponible en el sistema</div>`)
                }
            } else if (response.code == 406) {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>El número de tarjeta ya se encuentra asignada a otro socio</div>`)
                }
            } else {
                if (!createdElementValidationErrorsCard) {
                    createdElementValidationErrorsCard = true
                    $this.addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>Servicio no disponible</div>`)
                }
            }
        })
    })

    // Cancelar registro
    $('#btnCancel').click(function (e) {
        $('#btnCardDigitalNumber').prop('disabled', false)
        $('#card_number').prop('readonly', false)
    })

    // Limpiar mensajes de error de validación
    function clearValidationErrors()
    {
        $('.invalid-feedback').remove();
        $('.is-invalid').removeClass('is-invalid')
        $('.is-valid').removeClass('is-valid')
    }

    // Establecer indicador de progreso de solicitud
    function statusProgress(show)
    {
        if (show) {
            $('#btnSubmit').prop('disabled', true)
            $('#loader').show()
        } else {
            $('#btnSubmit').prop('disabled', false)
            $('#loader').hide()
        }
    }

    /*********************************************************************
     *
     *            Formatear campos de formulario
     *
     * ******************************************************************/

    // Número de tarjeta
    const cleaveControlCardNumber = new Cleave('#card_number', {
        delimiter: '-',
        blocks: [3, 3, 3, 3],
        uppercase: true
    })



    /*********************************************************************
     *
     *            Validar Patrones de información
     *
     * ******************************************************************/


    const inputCardNumber = d.getElementById('card_number')
    const selectReason = d.getElementById('slcReason')

    addValidationCard(inputCardNumber)
    addValidation(selectReason)

    function addValidation(element) {
        let createdElementValidationErrors = false
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrors) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrors = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrors) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrors = true
                }  else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrors = false
            }
        })
    }

    let createdElementValidationErrorsCard = false // El elemento DOM para mostrar mensajes de validaciópn, puede ser creado vía AJAX (valicación de tarjeta en el sistema)
    function addValidationCard(element) {
        element.addEventListener('invalid', function (e) {
            e.preventDefault();
            if (!createdElementValidationErrorsCard) {
                $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                createdElementValidationErrorsCard = true
            }
        })
        element.addEventListener('input', function (e) {
            // c(this.validity);
            if (!this.checkValidity()) {
                if (!createdElementValidationErrorsCard) {
                    $(element).addClass('is-invalid').removeClass('is-valid').parent().append(`<div class="invalid-feedback"><i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}</div>`)
                    createdElementValidationErrorsCard = true
                }  else {
                    // Ya existe el elemento que muestra el mensaje de error, solo cambiamos el mensaje
                    $(element).parent().find(".invalid-feedback").html(`<i class="bx bx-radio-circle"></i>${getMessageErrorValidation(this)}`)
                }
            } else {
                $(element).removeClass('is-invalid').addClass('is-valid')
                $(element).parent().find('.invalid-feedback').remove()
                createdElementValidationErrorsCard = false
            }
        })
    }

    function getMessageErrorValidation(element)
    {
        if (element.validity.badInput) return ""
        if (element.validity.rangeOverflow) return element.dataset.rangeoverflow
        if (element.validity.rangeUnderflow) return element.dataset.rangeunderflow
        if (element.validity.stepMismatch) return element.dataset.step
        if (element.validity.valueMissing) return element.dataset.required
        if (element.validity.tooLong) return element.dataset.toolong
        if (element.validity.tooShort) return element.dataset.toshort
        if (element.validity.patternMismatch) return element.dataset.pattern
    }

})(document, console.log, jQuery.noConflict())