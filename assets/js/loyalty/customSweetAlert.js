async function SA_confirmation_standar(title, text)
{
    return Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Si, deseo continuar',
        cancelButtonText: 'No, cancelar',
        confirmButtonColor: '#008eab',
        cancelButtonColor: '#6c757d',
        reverseButtons: true,
    })
}

async function SA_confirmation_acreditarPuntos(template)
{
    return Swal.fire({
        title: '¿Deseas acreditar los puntos?',
        html: template,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Si, deseo continuar',
        cancelButtonText: 'No, cancelar',
        confirmButtonColor: '#008eab',
        cancelButtonColor: '#6c757d',
        reverseButtons: true,
    })
}

async function SA_confirmation_cancelarAcreditacion(template)
{
    return Swal.fire({
        title: '¿Deseas cancelar esta acreditación?',
        html: template,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Si, deseo continuar',
        cancelButtonText: 'No, cancelar',
        confirmButtonColor: '#008eab',
        cancelButtonColor: '#6c757d',
        reverseButtons: true,
    })
}

async function SA_confirmation_cancelarRedencion(template)
{
    return Swal.fire({
        title: '¿Deseas cancelar esta redención?',
        html: template,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Si, deseo continuar',
        cancelButtonText: 'No, cancelar',
        confirmButtonColor: '#008eab',
        cancelButtonColor: '#6c757d',
        reverseButtons: true,
    })
}

async function SA_confirmation_cancelarBeneficio(template)
{
    return Swal.fire({
        title: '¿Deseas cancelar este beneficio?',
        html: template,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Si, deseo continuar',
        cancelButtonText: 'No, cancelar',
        confirmButtonColor: '#008eab',
        cancelButtonColor: '#6c757d',
        reverseButtons: true,
    })
}

async function SA_confirmation_certificate(title, text)
{
    return Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        confirmButtonText: 'Imprimir Ticket',
        confirmButtonColor: '#008eab',
    })
}

async function SA_cancel()
{
    return Swal.fire({
        title: 'Proceso cancelado',
        text: 'No se realizaron modificaciones en el sistema',
        icon: 'info',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#008eab',
    })
}

async function SA_progress() {
    return Swal.fire({
        title: "Procesando tu solicitud",
        text: "Espera un momento por favor.",
        imageUrl: BASE_URL + "/assets/images/svg/loader-modal.svg",
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
    });
}

async function SA_success_html(template, width = 550) {
    return Swal.fire({
        icon: 'success',
        title: 'Proceso terminado',
        html: template,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showConfirmButton: false,
        width: width,
    });
}

async function SA_error(message) {
    return Swal.fire({
        icon: 'error',
        title: 'Lo sentimos',
        html: message,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#008eab',
    });
}