;
((d, c, $) => {
    //c('Reimprimir Tickets')
    let client_id = null

    let tableAcreditaciones = document.querySelector('#acreditaciones-table');
    let tableRedenciones = document.querySelector('#redenciones-table');
    let tableBeneficios = document.querySelector('#beneficios-table');

    let dataTableAcreditaciones = new simpleDatatables.DataTable(tableAcreditaciones, {
        labels: {
            placeholder: "Buscar ticket",
            perPage: "{select} <label>Tickets por página</label>",
            noRows: "No existen tickets para mostrar",
            info: "Mostrando {start} a {end} de {rows} tickets (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableRedenciones = new simpleDatatables.DataTable(tableRedenciones, {
        labels: {
            placeholder: "Buscar redenciòn",
            perPage: "{select} <label>Redenciones por página</label>",
            noRows: "No existen redenciones para mostrar",
            info: "Mostrando {start} a {end} de {rows} redenciones (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });
    let dataTableBeneficios = new simpleDatatables.DataTable(tableBeneficios, {
        labels: {
            placeholder: "Buscar beneficio",
            perPage: "{select} <label>Beneficios por página</label>",
            noRows: "No existen beneficios para mostrar",
            info: "Mostrando {start} a {end} de {rows} beneficios (Página {page} de {pages} páginas)",
        },
        'perPage': 5,
    });


    $(d).on('click', '.btnCancelarAcreditacion', function (e) {
        e.preventDefault()
        e.stopPropagation()
        let $that = $(this);

        client_id = $(this).data('client_id')
        let receipt_id = $(this).data('receipt_id')
        let client = $(this).data('client')
        let store = $(this).data('store')
        let amount = $(this).data('amount')
        let date = $(this).data('date')
        let points = $(this).data('points')
        SA_confirmation_cancelarAcreditacion(tplNotificacionCancelarAcreditacion(client, amount, store, points, date)).then((result) => {
            if (result.isConfirmed) {
                SA_progress();
                // Cancelar acreditación
                $.post(BASE_URL + '/cancel-receipt', {receipt_id: receipt_id, client_id: client_id, description: 'El cliente se equivocó de recibo'}, function (response) {
                    c(response)
                    if (response.code == 200) {
                        SA_success_html(tplProcesoTerminadoCancelarAcreditacion(client, points))
                        $('#receipt-' + receipt_id).remove();
                    } else {
                        SA_error(response.message)
                    }
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })

    $(d).on('click', '.btnCancelarRedencion', function (e) {
        e.preventDefault()
        e.stopPropagation()
        let $that = $(this);

        client_id = $(this).data('client_id')
        let redemption_id = $(this).data('redemption_id')
        let client = $(this).data('client')
        let certificate = $(this).data('certificate')
        let amount = $(this).data('amount')
        let date = $(this).data('date')
        let points = $(this).data('points')
        SA_confirmation_cancelarRedencion(tplNotificacionCancelarRedencion(client, amount, certificate, points, date)).then((result) => {
            if (result.isConfirmed) {
                SA_progress();
                // Cancelar acreditación
                $.post(BASE_URL + '/cancel-redemption', {redemption_id:redemption_id, client_id: client_id, description: 'El cliente se equivocó en la entrega'}, function (response) {
                    c(response)
                    if (response.code == 200) {
                        SA_success_html(tplProcesoTerminadoCancelarRedencion(client, points))
                        $('#redemption-' + redemption_id).remove();
                    } else {
                        SA_error(response.message)
                    }
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })

    $(d).on('click', '.btnCancelarBeneficio', function (e) {
        e.preventDefault()
        e.stopPropagation()
        let $that = $(this);

        client_id = $(this).data('client_id')
        let redemption_id = $(this).data('redemption_id')
        let client = $(this).data('client')
        let benefit = $(this).data('benefit')
        let amount = $(this).data('amount')
        let date = $(this).data('date')
        let points = $(this).data('points')
        SA_confirmation_cancelarBeneficio(tplNotificacionCancelarBeneficio(client, amount, benefit, date)).then((result) => {
            if (result.isConfirmed) {
                SA_progress();
                // Cancelar acreditación
                $.post(BASE_URL + '/cancel-benefit', {redemption_id:redemption_id, client_id: client_id, description: 'El cachorro está embrujado'}, function (response) {
                    c(response)
                    if (response.code == 200) {
                        SA_success_html(tplProcesoTerminadoCancelarBeneficio(client, benefit))
                        $('#benefit-' + redemption_id).remove();
                    } else {
                        SA_error(response.message)
                    }
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                SA_cancel()
            }
        })
    })



    /*********************************************************************
     *
     *            Listeners: Botones personalizados para modal de
     *            proceso terminado
     *
     * ******************************************************************/

    $(d).on('click', '#btnMasOperaciones', function (e) {
        $(this).prop('disabled', true)
        $("#btnTerminar").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        // TODO: llamar servicio para actualizar datos del socio en pantalla
        $.get(BASE_URL + '/consultar-puntos/socio/' + client_id, function (response) {
            $('#info_points_available').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_available)) + ' Puntos disponibles')
            $('#info_points_reddeemed').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_redeemed)) + ' Puntos redimidos')
            $('#info_points_total').fadeOut().fadeIn().text(new Intl.NumberFormat('es-MX').format(Number(response.points.points_available) + Number(response.points.points_redeemed)) + ' Puntos totales')
            $('#info_level').fadeOut().fadeIn().text('Nivel ' + response.client.level)
            Swal.close()
        })
    })

    $(d).on('click', '#btnTerminar', function (e) {
        $(this).prop('disabled', true)
        $("#btnMasOperaciones").prop('disabled', true)

        e.preventDefault()
        e.stopPropagation()

        window.location.replace(BASE_URL + '/socios/panel-de-control')
    })


})(document, console.log, jQuery.noConflict())