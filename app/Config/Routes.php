<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Pages');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override(function() {
    return view('pages/404');
});
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.


/*--------------Frontend Routes-------------*/

$routes->get('/', 'Services\Login::login');
$routes->post('logout', 'Services\Login::logout');

$routes->group('/', ['namespace' => 'App\Controllers\Frontend', 'filter' => 'auth'], function ($routes) {

    $routes->get('reportes/pruebas', 'PartnerReportController::sociosPorNivelTable');

    $routes->get('socios/panel-de-control', 'PageController::index');
    $routes->get('socios/acreditar-puntos/(:num)', 'PageController::acreditarPuntos/$1');
    $routes->get('socios/redenciones/(:num)', 'PageController::redenciones/$1');
    $routes->get('socios/entrega-de-beneficios/(:num)', 'PageController::entregaDeBeneficios/$1');
    $routes->get('socios/estado-de-cuenta/(:num)', 'PageController::estadoDeCuenta/$1');
    $routes->get('socios/catalogo-de-premios/(:num)', 'PageController::catalogoDePremios/$1');

    $routes->get('clientes/registrar-usuario', 'PageController::registrarUsuario');
    $routes->get('clientes/tarjetas-y-beneficios', 'PageController::tarjetasBeneficios');
    $routes->get('clientes/listado-de-premios', 'PageController::listadoDePremios');
    $routes->get('clientes/listado-de-beneficios', 'PageController::listadoDeBeneficios');


    $routes->get('operaciones/cambiar-tarjeta/(:num)', 'PageController::cambiarTarjeta/$1');
    $routes->get('operaciones/editar-socio/(:num)', 'PageController::editarSocio/$1');
    $routes->get('operaciones/reimprimir-tickets/(:num)', 'PageController::reimprimirTickets/$1');
    $routes->get('operaciones/cancelaciones/(:num)', 'PageController::cancelaciones/$1');
    $routes->get('operaciones/registrar-concierge', 'PageController::registrarConcierge');

    // Impresión y Reimpresión de tickets
    $routes->post('imprimir/ticket/redenciones', 'TicketController::imprimirTicketRedencion');
    $routes->post('imprimir/ticket/acreditar-puntos', 'TicketController::imprimirTicketAcreditacion');
    $routes->post('imprimir/ticket/entrega-de-beneficios', 'TicketController::imprimirTicketEntregaDeBeneficios');
    $routes->post('reimprimir/ticket/redencion', 'TicketController::reimprimirTicketRedencion');
    $routes->post('reimprimir/ticket/acreditacion', 'TicketController::reimprimirTicketAcreditacion');
    $routes->post('reimprimir/ticket/beneficio', 'TicketController::reimprimirTicketBeneficio');

    // TODO: Mover a un controlador correcto, esto forma parte del backend
    $routes->post('cambiar-tarjeta', 'ServiceController::cambiarTarjeta');
    $routes->post('editar-socio', 'ServiceController::editarSocio');
    $routes->get('consultar-puntos/socio/(:num)', 'ServiceController::consultarPuntos/$1');

    // Gráficas y Reportes (Socios)
    $routes->get('graficas/socios/(:segment)', 'PartnerChartController::index/$1');
    $routes->get('reportes/socios/(:segment)/(:segment)', 'PartnerReportController::sociosRegistrados/$1/$2');
    $routes->get('reportes/socios/tienda/(:segment)/(:segment)/(:segment)', 'PartnerReportController::sociosComprasPorTienda/$1/$2/$3');
    $routes->get('reportes/socios/genero/(:segment)/(:segment)/(:segment)', 'PartnerReportController::sociosPorGenero/$1/$2/$3');
    $routes->get('reportes/socios/nivel/(:segment)/(:segment)/(:segment)', 'PartnerReportController::sociosPorNivel/$1/$2/$3');
    $routes->get('reportes/socios/rangoedad/(:segment)/(:segment)/(:segment)', 'PartnerReportController::sociosPorRangoDeEdad/$1/$2/$3');
    // Gráficas y Reportes (Dinero en Tickets y Redenciones)
    $routes->get('graficas/dinero/(:segment)', 'MoneyChartController::index/$1');
    $routes->get('reportes/dinero/tickets/(:segment)/(:segment)', 'MoneyReportController::dineroTickets/$1/$2');
    $routes->get('reportes/dinero/tickets/generos/(:segment)/(:segment)/(:segment)', 'MoneyReportController::dineroTicketsPorGenero/$1/$2/$3');
    $routes->get('reportes/dinero/tickets/niveles/(:segment)/(:segment)/(:segment)', 'MoneyReportController::dineroTicketsPorNivel/$1/$2/$3');
    $routes->get('reportes/dinero/tickets/rango-edad/(:segment)/(:segment)/(:segment)', 'MoneyReportController::dineroTicketsPorRangoDeEdad/$1/$2/$3');
    $routes->get('reportes/dinero/redenciones/(:segment)/(:segment)', 'MoneyReportController::dineroRedenciones/$1/$2');
    $routes->get('reportes/dinero/redenciones/genero/(:segment)/(:segment)/(:segment)', 'MoneyReportController::dineroRedencionesPorGenero/$1/$2/$3');
    $routes->get('reportes/dinero/redenciones/nivel/(:segment)/(:segment)/(:segment)', 'MoneyReportController::dineroRedencionesPorNivel/$1/$2/$3');
    // Gráficas y Reportes (Beneficios entregados)
    $routes->get('graficas/beneficios/(:segment)', 'BenefitChartController::index/$1');
    $routes->get('reportes/certificados/(:segment)', 'BenefitReportController::certificadosEntregados/$1');
    $routes->get('reportes/certificados/periodo/(:segment)', 'BenefitReportController::certificadosEntregadosPorPeriodo/$1');

    $routes->get('reportes/beneficios/(:segment)', 'BenefitReportController::beneficiosEntregados/$1');
    $routes->get('reportes/beneficios/periodo/(:segment)', 'BenefitReportController::beneficiosEntregadosPorPeriodo/$1');
    $routes->get('reportes/beneficios', 'BenefitReportController::beneficiosEntregados');
    // Gráficas y Reportes (Puntos)
    $routes->get('graficas/puntos/(:segment)', 'PointChartController::index/$1');
    $routes->get('reportes/puntos', 'PointReportController::puntos');
});


/*--------------Backend Routes-------------*/

$routes->get('/generate-card-number', 'Services/ClientRegister::generateNewCard');
$routes->get('/save-points-history', 'Services/PointsHistory::saveData');
$routes->get('/get-dates-for-points-history', 'Services/PointsHistory::getDates');


$routes->post('/get-historic', 'Services/PointsHistory::getHistoric');

$routes->post('/login-validation', 'Services/Login::loginValidation');
$routes->post('/client-register', 'Services/ClientRegister::clientRegister');
$routes->post('/client-search', 'Services/ClientFunctions::searchClient');
$routes->post('/validate-card', 'Services/ClientFunctions::checkCardInfo');


$routes->post('/get-client', 'Services/ClientFunctions::getClientInfo');
$routes->post('/receipt-register', 'Services/ReceiptRegister::receiptRegister');
$routes->post('/redemption-register', 'Services/RedemptionsFunctions::redemptionRegister');
$routes->post('/redemption-benefit', 'Services/RedemptionsFunctions::redemptionBenefit');

$routes->post('/get-municipalities', 'Services/ClientFunctions::getMunicipalitiesByState');
$routes->post('/get-suburbs', 'Services/ClientFunctions::getSuburbsByMunicipalities');

$routes->post('/get-certificates', 'Services/ClientFunctions::getCertificates');
$routes->post('/get-banefits', 'Services/ClientFunctions::getBenefits');

$routes->post('/cancel-receipt', 'Services/Cancelations::cancelReceipt');
$routes->post('/cancel-redemption', 'Services/Cancelations::cancelRedemption');
$routes->post('/cancel-benefit', 'Services/Cancelations::cancelBenefit');

$routes->post('/concierge-register', 'Services/Users::userRegister');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
