<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class PartnerChartModel extends Model
{
    public function __construct($year)
    {
        helper('custom');
        $this->db = db_connect('default');
        $this->year = $year;
    }

    public function clientesTotales()
    {
        $builder = $this->db->table('clients as c');

        $clientes_totales = $builder->where('enabled', 1)->where('available', 1)->countAllResults();
        return $clientes_totales;
    }

    public function clientesTotalesPorPeriodoSeleccionado()
    {
        $builder = $this->db->table('clients as c');

        $clientes_totales = $builder->where('YEAR(created_at)', $this->year)->where('enabled', 1)->where('available', 1)->countAllResults();
        return $clientes_totales;
    }

    public function listarTiendas()
    {
        $builder = $this->db->table('stores');
        $tiendas = $builder->where('enabled', 1)->orderBy('name')->get()->getResult();
        return $tiendas;
    }

    public function sociosRegistradosPorGenero()
    {
        $builder = $this->db->table('clients as c');

        //$clientes_totales = $builder->where('enabled', 1)->where('available', 1)->countAll();
        $clientes_por_genero = $builder->select('i.gender, COUNT(*) as total')->groupBy('i.gender')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('YEAR(c.created_at)', $this->year)
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->orderBy('i.gender')
            ->get()->getResult();
        $generos = [];
        $labels = [];
        foreach ($clientes_por_genero as $g) {
            array_push($generos, round(($g->total * 100 / $this->clientesTotalesPorPeriodoSeleccionado())));
            array_push($labels, ($g->gender) == 'F' ? 'Mujeres' : 'Hombres');
        }
        //dd($clientes_por_genero, $this->clientesTotalesPorPeriodoSeleccionado());
        return [
            'porcentajes' => $generos,
            'labels' => $labels,
            'clientes' => $clientes_por_genero,
            'concentrado' => $this->concentradoSociosRegistradosPorGenero()
        ];

    }

    public function sociosRegistradosPorNivel()
    {
        $builder = $this->db->table('clients as c');

        $clientes_por_nivel = $builder->select('l.name, COUNT(*) as total')->groupBy('l.name')
            ->join('cards', 'c.id = cards.client_id')
            ->join('card_levels as l', 'l.id = cards.card_level_id')
            ->join('card_status as s', 's.id = cards.card_status_id')
            ->where('YEAR(c.created_at)', $this->year)
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('cards.enabled', 1)
            ->where('s.name', 'Habilitada')
            ->get()->getResult();
        $niveles = [];
        $labels = [];
        foreach ($clientes_por_nivel as $g) {
            array_push($niveles, round(($g->total * 100 / $this->clientesTotalesPorPeriodoSeleccionado())));
            array_push($labels, $g->name);
        }
        return [
            'porcentajes' => $niveles,
            'labels' => $labels,
            'clientes' => $clientes_por_nivel,
            'concentrado' => $this->concentradoSociosRegistradosPorNivel()
        ];
    }

    public function sociosRegistradosPorRangoDeEdad()
    {
        $builder = $this->db->table('clients as c');

        $clientes_por_edad = $builder->select('
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 0 AND 17, 1, 0)) as "0-17",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 18 AND 29, 1, 0)) as "18-29",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 30 AND 39, 1, 0)) as "30-39",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 40 AND 49, 1, 0)) as "40-49",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 50 AND 59, 1, 0)) as "50-59",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 60 AND 69, 1, 0)) as "60-69",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 70 AND 79, 1, 0)) as "70-79",
        ')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('YEAR(c.created_at)', $this->year)
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();
        $edades = [];
        $labels = [];
        //dd($clientes_por_edad, $this->clientesTotalesPorPeriodoSeleccionado());
        foreach ($clientes_por_edad[0] as $clave => $valor) {
            if ($valor) {
                array_push($edades, round(($valor * 100 / $this->clientesTotalesPorPeriodoSeleccionado())));
                array_push($labels, "De " . str_replace('-', ' a ', $clave) . " años");
            }
        }

        return [
            'porcentajes' => $edades,
            'labels' => $labels,
            'clientes' => $clientes_por_edad,
            'concentrado' => $this->concentradoSociosRegistradosPorRangoDeEdad()
        ];
    }

    public function sociosRegistradosPorRegion()
    {
        $builder = $this->db->table('clients as c');

        $clientes_por_regiones = $builder->select('states.name, COUNT(*) as total')->groupBy('states.name')
            ->join('client_info as i', 'c.id = i.client_id')
            ->join('states', 'states.id = i.estado')
            ->where('YEAR(c.created_at)', $this->year)
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();
        $data_regiones = [];
        $regiones_name = [];
        $regiones_total = [];
        foreach ($clientes_por_regiones as $region) {
            array_push($regiones_name, $region->name);
            array_push($regiones_total, $region->total);
        }
        $data_regiones['data'] = $regiones_total;
        $data_regiones['labels'] = $regiones_name;
        return [
            'data' => $data_regiones,
            'clientes' => $clientes_por_regiones
        ];
    }

    public function sociosRegistradosPorMes()
    {
        $builder = $this->db->table('clients as c');
        $clientes_por_periodo = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('mes')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('YEAR(c.created_at)', $this->year)
            ->get()->getResult();
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $periodos = [];
        $chart_periodo = [];
        $periodo_data = [];
        $periodo_labels = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($clientes_por_periodo as $periodo) {
                if ($periodo->mes == $index + 1) {
                    array_push($periodos, ['mes' => $mes, 'total' => $periodo->total]);
                    array_push($periodo_data, $periodo->total);
                    array_push($periodo_labels, $mes);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($periodos, ['mes' => $mes, 'total' => 0]);
                array_push($periodo_data, 0);
                array_push($periodo_labels, $mes);
            }
        }
        $chart_periodo['data'] = $periodo_data;
        $chart_periodo['labels'] = $periodo_labels;
        return [
            'data' => $chart_periodo,
            'clientes' => $clientes_por_periodo
        ];
    }

    private function concentradoSociosRegistradosPorNivel()
    {
        $builder = $this->db->table('clients as c');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $periodo = [];
        foreach ($niveles as $nivel):
            $clientes_por_nivel = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('MONTH(c.created_at)')
                ->join('cards', 'c.id = cards.client_id')
                ->join('card_levels as l', 'l.id = cards.card_level_id')
                ->join('card_status as s', 's.id = cards.card_status_id')
                ->where('l.name', $nivel)
                ->where('YEAR(c.created_at)', $this->year)
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->where('cards.enabled', 1)
                ->where('s.name', 'Habilitada')
                ->get()->getResult();

            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($clientes_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($periodo, $data);
        endforeach;
        $niveles = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $periodo
        ];
        return $niveles;
    }

    private function concentradoSociosRegistradosPorGenero()
    {
        $builder = $this->db->table('clients as c');

        $generos = ['M', 'F'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($generos as $genero):
            $clientes_por_genero = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('MONTH(c.created_at)')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('i.gender', $genero)
                ->where('YEAR(c.created_at)', $this->year)
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->orderBy('i.gender')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($clientes_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function concentradoSociosRegistradosPorRangoDeEdad()
    {
        $builder = $this->db->table('clients as c');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $edades = ['0 AND 17', '18 AND 29','30 AND 39','40 AND 49','50 AND 59','60 AND 69','70 AND 79'];

        $clientes = [];
        foreach ($edades as $index => $edad):
            $clientes_por_edad = $builder->select('
                MONTH(c.created_at) as mes,
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "'. $edad.'",
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "total",
            ')->groupBy('MONTH(c.created_at)')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('YEAR(c.created_at)', $this->year)
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes):
                $encontrado = false;
                foreach ($clientes_por_edad as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            endforeach;
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'edades' => ['De 0 a 17 años', 'De 18 a 29 años','De 30 a 39 años','De 40 a 49 años','De 50 a 59 años','De 60 a 69 años','De 70 a 79 años'],
            'backgrounds' => ['bg-info', 'bg-primary', 'bg-danger', 'bg-secondary', 'bg-success', 'bg-warning', 'bg-dark'],
            'periodo' => $clientes
        ];
        return $informacion;
    }
}
