<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class MoneyChartModel extends Model
{
    public function __construct($year)
    {
        helper('custom');
        $this->db = db_connect('default');
        $this->year = $year;
    }

    public function dineroIngresadoEnTickets($year = null)
    {
        $builder = $this->db->table('receipts');
        $dinero_en_tickets = null;

        if ($year) {
            $dinero_en_tickets = $builder->selectSum('amount')->where('YEAR(date)', $year)->where('enabled', 1)->get()->getRow()->amount;
        } else {
            $dinero_en_tickets = $builder->selectSum('amount')->where('enabled', 1)->get()->getRow()->amount;
        }

        return $dinero_en_tickets;
    }

    public function dineroEntregadoEnRedenciones($year = null)
    {
        $builder = $this->db->table('certificates');
        $dinero_en_redenciones = null;

        if($year) {
            $dinero_en_redenciones = $builder->select('SUM(certificates.price * redemptions.amount) AS total')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->where('redemptions.enabled', 1)
                ->where('YEAR(redemptions.created_at)', $year)
                ->get()->getRow()->total;
        } else {
            $dinero_en_redenciones = $builder->select('SUM(certificates.price * redemptions.amount) AS total')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->where('redemptions.enabled', 1)
                ->get()->getRow()->total;
        }

        return $dinero_en_redenciones;
    }

    public function dineroEnTicketsPorGenero()
    {
        $builder = $this->db->table('receipts');

        $dinero_en_tickets_por_genero = $builder->select('SUM(receipts.amount) AS total, info.gender')->groupBy('info.gender')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->where('YEAR(receipts.date)', $this->year)
            ->where('receipts.enabled', 1)
            ->where('transactions.enabled', 1)
            ->orderBy('info.gender')
            ->get()->getResult();
        $grafica_dinero_tickets_genero = [];
        $labels = [];
        foreach ($dinero_en_tickets_por_genero as $genero) {
            array_push($grafica_dinero_tickets_genero, (float) bcdiv(($genero->total * 100 / $this->dineroIngresadoEnTickets($this->year)),1,2));
            array_push($labels, $genero->gender == 'F' ? 'Mujeres': 'Hombres');
        }
        return [
            'porcentajes' => $grafica_dinero_tickets_genero,
            'labels' => $labels,
            'dinero' => $dinero_en_tickets_por_genero,
            'concentrado' => $this->concentradoDineroEnTicketsPorGenero()
        ];
    }

    public function dineroEnRedencionesPorGenero()
    {
        $builder = $this->db->table('certificates');

        $dinero_en_redenciones_por_genero = $builder->select('SUM(certificates.price * redemptions.amount) AS total, info.gender')->groupBy('info.gender')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->where('redemptions.enabled', 1)
            ->where('YEAR(redemptions.created_at)', $this->year)
            ->orderBy('info.gender')
            ->get()->getResult();
        $grafica_dinero_redenciones_genero = [];
        $labels = [];
        foreach ($dinero_en_redenciones_por_genero as $genero) {
            array_push($grafica_dinero_redenciones_genero, (float) bcdiv(($genero->total * 100 / $this->dineroEntregadoEnRedenciones($this->year)),1,2));
            array_push($labels, $genero->gender == 'F' ? 'Mujeres': 'Hombres' );
        }
        return [
            'porcentajes' => $grafica_dinero_redenciones_genero,
            'labels' => $labels,
            'dinero' => $dinero_en_redenciones_por_genero,
            'concentrado' => $this->concentradoDineroEnRedencionesPorGenero()
        ];
    }

    public function dineroEnTicketsPorNivel()
    {
        $builder = $this->db->table('receipts');

        $dinero_en_tickets_por_nivel = $builder->select('SUM(receipts.amount) AS total, level.name')->groupBy('level.name')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->where('YEAR(receipts.date)', $this->year)
            ->where('receipts.enabled', 1)
            ->where('transactions.enabled', 1)
            ->orderBy('level.id')
            ->get()->getResult();
        $grafica_dinero_en_tickets_por_nivel = [];
        $labels = [];
        foreach ($dinero_en_tickets_por_nivel as $nivel) {
            array_push($grafica_dinero_en_tickets_por_nivel, (float) bcdiv(($nivel->total * 100 / $this->dineroIngresadoEnTickets($this->year)), 1,2));
            array_push($labels, $nivel->name);
        }
        return [
            'porcentajes' => $grafica_dinero_en_tickets_por_nivel,
            'labels' => $labels,
            'dinero' => $dinero_en_tickets_por_nivel,
            'concentrado' => $this->concentradoDineroEnTicketsPorNivel()
        ];
    }

    public function dineroEnRedencionesPorNivel()
    {
        $builder = $this->db->table('certificates');

        $dinero_en_redenciones_por_nivel = $builder->select('SUM(certificates.price * redemptions.amount) AS total, level.name')->groupBy('level.name')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->where('redemptions.enabled', 1)
            ->where('YEAR(redemptions.created_at)', $this->year)
            ->orderBy('level.id')
            ->get()->getResult();
        $grafica_dinero_en_redenciones_por_nivel = [];
        $labels = [];
        foreach ($dinero_en_redenciones_por_nivel as $nivel) {
            array_push($grafica_dinero_en_redenciones_por_nivel, (float) bcdiv(($nivel->total * 100 / $this->dineroEntregadoEnRedenciones($this->year)), 1, 2));
            array_push($labels, "Nivel " .$nivel->name);
        }
        return [
            'porcentajes' => $grafica_dinero_en_redenciones_por_nivel,
            'labels' => $labels,
            'dinero' => $dinero_en_redenciones_por_nivel,
            'concentrado' => $this->concentradoDineroEnRedencionesPorNivel()
        ];
    }

    public function dineroEnTicketsPorMes()
    {
        $builder = $this->db->table('receipts');

        $dinero_en_tickets_por_mes_db = $builder->select('SUM(amount) AS total, MONTH(date) AS month_numeric')->groupBy('month_numeric')
            ->where('enabled', 1)
            ->where('YEAR(date)', $this->year)
            ->get()->getResult();

        // Agregar meses sin reporte de tickets
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $dinero_en_tickets_por_mes = [];
        $grafica_dinero_en_tickets_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($dinero_en_tickets_por_mes_db as $registro) {
                if ($index + 1 == $registro->month_numeric) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($dinero_en_tickets_por_mes, $object);
                    array_push($grafica_dinero_en_tickets_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($dinero_en_tickets_por_mes, $object);
                array_push($grafica_dinero_en_tickets_por_mes['data'], 0);
            }
        }
        return [
            'data' => $grafica_dinero_en_tickets_por_mes,
            'dinero' => $dinero_en_tickets_por_mes
        ];
    }

    public function dineroEnRedencionesPorMes()
    {
        $builder = $this->db->table('certificates');

        $dinero_en_redenciones_por_mes_db = $builder->select('SUM(certificates.price * redemptions.amount) AS total, MONTH(redemptions.created_at) AS month_numeric')->groupBy('month_numeric')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->where('redemptions.enabled', 1)
            ->where('YEAR(redemptions.created_at)', $this->year)
            ->get()->getResult();

        // Agregar meses sin reporte de tickets
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $dinero_en_redenciones_por_mes = [];
        $grafica_dinero_en_redenciones_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($dinero_en_redenciones_por_mes_db as $registro) {
                if ($index + 1 == $registro->month_numeric) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($dinero_en_redenciones_por_mes, $object);
                    array_push($grafica_dinero_en_redenciones_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($dinero_en_redenciones_por_mes, $object);
                array_push($grafica_dinero_en_redenciones_por_mes['data'], 0);
            }
        }
        return [
            'data' => $grafica_dinero_en_redenciones_por_mes,
            'dinero' => $dinero_en_redenciones_por_mes
        ];
    }

    public function dineroEnTicketsPorRangoDeEdad()
    {
        $builder = $this->db->table('receipts as r');

        $dinero_en_tickets_por_rango_de_edad = $builder->select('
            SUM(r.amount) as total, 
            CASE 
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 0 AND 17) THEN \'0 a 17\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 18 AND 25) THEN \'18 a 25\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 26 AND 35) THEN \'26 a 35\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 36 AND 45) THEN \'36 a 45\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 46 AND 55) THEN \'46 a 55\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 56 AND 65) THEN \'56 a 65\'
                WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 66 AND 90) THEN \'66 a 90\'
            END as rango')->groupBy('rango')
            ->join('transactions as t', 't.receipt_id = r.id')
            ->join('clients as c', 'c.id = t.client_id')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('YEAR(r.date)', $this->year)
            ->where('r.enabled', 1)
            ->where('t.enabled', 1)
            ->get()->getResult();
        $grafica_dinero_en_tickets_por_rango_de_edad = ['data' => [], 'labels' => []];
        $labels = [];
        foreach ($dinero_en_tickets_por_rango_de_edad as $edad) {
            //if ($edad) {
                array_push($grafica_dinero_en_tickets_por_rango_de_edad['data'], (float)bcdiv(($edad->total * 100 / $this->dineroIngresadoEnTickets($this->year)), 1, 2));
                array_push($grafica_dinero_en_tickets_por_rango_de_edad['labels'], "De " . str_replace('-', ' a ', $edad->rango) . " años");
                //array_push($labels, "De " . str_replace('-', ' a ', $edad->rango) . " años");
           // }
        }
        return [
            'porcentajes' => $grafica_dinero_en_tickets_por_rango_de_edad,
            'labels' => $labels,
            'dinero' => $dinero_en_tickets_por_rango_de_edad,
            'concentrado' => $this->concentradoDineroEnTicketsPorRangoDeEdad()
        ];
    }

    private function concentradoDineroEnTicketsPorGenero()
    {
        $builder = $this->db->table('receipts');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $generos = ['M', 'F'];

        $clientes = [];
        foreach ($generos as $genero):
            $dinero_en_tickets_por_genero = $builder->select('MONTH(transactions.created_at) as mes, SUM(receipts.amount) AS total, info.gender')->groupBy('MONTH(transactions.created_at)')
                ->join('transactions', 'receipts.id = transactions.receipt_id')
                ->join('clients', 'clients.id = transactions.client_id')
                ->join('client_info as info', 'clients.id = info.client_id')
                ->where('info.gender', $genero)
                ->where('YEAR(date)', $this->year)
                ->where('receipts.enabled', 1)
                ->where('transactions.enabled', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_tickets_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function concentradoDineroEnRedencionesPorGenero()
    {
        $builder = $this->db->table('certificates');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $generos = ['M', 'F'];

        $clientes = [];
        foreach ($generos as $genero):
            $dinero_en_redenciones_por_genero = $builder->select('MONTH(redemptions.created_at) as mes, SUM(certificates.price * redemptions.amount) AS total, info.gender')->groupBy('MONTH(redemptions.created_at)')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->join('clients', 'clients.id = redemptions.client_id')
                ->join('client_info as info', 'clients.id = info.client_id')
                ->where('info.gender', $genero)
                ->where('redemptions.enabled', 1)
                ->where('YEAR(redemptions.created_at)', $this->year)
                //->orderBy('info.gender')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_redenciones_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function concentradoDineroEnTicketsPorNivel()
    {
        $builder = $this->db->table('receipts');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($niveles as $nivel):
            $dinero_en_tickets_por_nivel = $builder->select('MONTH(transactions.created_at) as mes, SUM(receipts.amount) AS total, level.name')->groupBy('MONTH(transactions.created_at)')
                ->join('transactions', 'receipts.id = transactions.receipt_id')
                ->join('cards', 'cards.id = transactions.card_id')
                ->join('card_levels as level', 'level.id = cards.card_level_id')
                ->where('YEAR(date)', $this->year)
                ->where('level.name', $nivel)
                ->where('receipts.enabled', 1)
                ->where('transactions.enabled', 1)
                //->orderBy('level.id')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_tickets_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function concentradoDineroEnRedencionesPorNivel()
    {
        $builder = $this->db->table('certificates');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($niveles as $nivel):
            $dinero_en_redenciones_por_nivel = $builder->select('MONTH(redemptions.created_at) as mes, SUM(certificates.price * redemptions.amount) AS total, level.name')->groupBy('MONTH(redemptions.created_at)')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->join('cards', 'cards.id = redemptions.card_id')
                ->join('card_levels as level', 'level.id = cards.card_level_id')
                ->where('level.name', $nivel)
                ->where('redemptions.enabled', 1)
                ->where('YEAR(redemptions.created_at)', $this->year)
                //->orderBy('level.id')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_redenciones_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function concentradoDineroEnTicketsPorRangoDeEdad()
    {
        $builder = $this->db->table('receipts as r');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $edades = ['0 AND 17', '18 AND 25', '26 AND 35', '36 AND 45', '46 AND 55', '56 AND 65', '66 AND 99'];

        $clientes = [];
        foreach ($edades as $edad):
            $dinero_en_tickets_por_rango_de_edad = $builder->select('
                MONTH(c.created_at) as mes, 
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "'. $edad.'",
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', r.amount, 0)) as total')->groupBy('MONTH(c.created_at)')
                ->join('transactions as t', 't.receipt_id = r.id')
                ->join('clients as c', 'c.id = t.client_id')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('YEAR(date)', $this->year)
                ->where('r.enabled', 1)
                ->where('t.enabled', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes):
                $encontrado = false;
                foreach ($dinero_en_tickets_por_rango_de_edad as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            endforeach;
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'edades' => ['De 0 a 17 años','De 18 a 25 años','De 26 a 35 años','De 36 a 45 años','De 46 a 55 años','De 56 a 65 años','De 66 a 90 años'],
            'backgrounds' => ['bg-primary', 'bg-danger', 'bg-secondary', 'bg-success', 'bg-warning', 'bg-dark','bg-secondary'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

}
