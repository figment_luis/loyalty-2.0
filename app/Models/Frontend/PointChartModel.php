<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class PointChartModel extends Model
{
    public function __construct($year)
    {
        helper('custom');
        $this->db = db_connect('default');
        $this->year = $year;
    }

    public function puntosDisponibles()
    {
        $builder = $this->db->table('points');
        $puntos_disponibles = $builder->selectSum('points_available')->where('YEAR(created_at)', $this->year)->get()->getRow()->points_available;
        return $puntos_disponibles;
    }

    public function puntosRedimidos()
    {
        $builder = $this->db->table('points');
        $puntos_redimidos = $builder->selectSum('points_redeemed')->where('YEAR(created_at)', $this->year)->get()->getRow()->points_redeemed;
        return $puntos_redimidos;
    }

    public function puntosExpirados()
    {
        $builder = $this->db->table('points');
        $puntos_expirados = $builder->selectSum('points_expired')->where('YEAR(created_at)', $this->year)->get()->getRow()->points_expired;
        return $puntos_expirados;
    }

    public function concentradoPuntosDisponibles()
    {
        $builder = $this->db->table('transactions');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $puntos = [];
        $puntos_disponibles = $builder->select('MONTH(transactions.created_at) as mes, SUM(transactions.points) AS total')->groupBy('MONTH(transactions.created_at)')
            ->where('transactions.is_canceled', 0)
            // TODO: Preguntar al backend por que no está deshabilitando las transacciones cuando estas son canceladas
            // ->where('transactions.enabled', 1)
            ->where('YEAR(transactions.created_at)', $this->year)
            ->get()->getResult();
        $data = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($puntos_disponibles as $item) {
                if ($item->mes == $index + 1) {
                    array_push($data, ['mes' => $mes, 'total' => $item->total]);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($data, ['mes' => $mes, 'total' => 0]);
            }
        }
        array_push($puntos, $data);
        $informacion = [
            'meses' => $meses,
            'periodo' => $puntos
        ];
        return $informacion;
    }

    public function concentradoPuntosRedimidos()
    {
        $builder = $this->db->table('tickets_redemptions_by_certificates as transaction');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $puntos = [];
        $puntos_redimidos = $builder->select('MONTH(transaction.created_at) as mes, SUM(transaction.points_redeemed) AS total')->groupBy('MONTH(transaction.created_at)')
            ->where('transaction.enabled', 1)
            ->where('YEAR(transaction.created_at)', $this->year)
            ->get()->getResult();
        $data = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($puntos_redimidos as $item) {
                if ($item->mes == $index + 1) {
                    array_push($data, ['mes' => $mes, 'total' => $item->total]);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($data, ['mes' => $mes, 'total' => 0]);
            }
        }
        array_push($puntos, $data);
        $informacion = [
            'meses' => $meses,
            'periodo' => $puntos
        ];
        return $informacion;
    }
}
