<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class PartnerReportModel extends Model
{
    public function __construct()
    {
        $this->db = db_connect('default');
    }

    public function sociosRegistrados($fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('clients');

        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, type.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, IF(info.gender = "M", "Masculino", "Femenino") as gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as estado, municipality.name as municipio')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('card_types as type', 'type.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('DATE(clients.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        return $registros;
    }

    public function sociosComprasPorTienda($tienda, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('clients');

        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, stores.name as store, SUM(receipts.amount) as amount, receipts.date')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('transactions', 'clients.id = transactions.client_id')
            ->join('receipts', 'receipts.id = transactions.receipt_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->where('receipts.store_id', $tienda)
            ->where('DATE(receipts.date) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->groupBy('clients.email')
            ->get()->getResult();

        return $registros;
    }

    public function sociosPorGenero($genero, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('clients');

        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('info.gender', $genero)
            ->where('DATE(clients.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        return $registros;
    }

    public function sociosPorNivel($nivel, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('clients');

        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('level.name', $nivel)
            ->where('DATE(clients.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        return $registros;
    }

    public function sociosPorRangoDeEdad($rango, $fechaInicio, $fechaFin)
    {
        list($edadMin, $edadMax) = explode('-', $rango);
        $builder = $this->db->table('clients');

        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, info.birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) BETWEEN ' . $edadMin . ' AND ' . $edadMax)
            ->where('DATE(clients.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        return $registros;
    }
}
