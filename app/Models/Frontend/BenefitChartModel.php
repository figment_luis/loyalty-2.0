<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class BenefitChartModel extends Model
{
    public function __construct($year)
    {
        helper('custom');
        $this->db = db_connect('default');
        $this->year = $year;
    }

    public function certificadosRedimidosPorMes()
    {
        $builder = $this->db->table('redemptions_by_certificates');
        $listado_certificados = $builder->select('SUM(amount) AS total, MONTH(created_at) AS month')->groupBy('month')
            ->where('enabled', 1)
            ->where('is_canceled', 0)
            ->where('YEAR(created_at)', $this->year)
            ->get()->getResult();

        // Preparar datos del periodo (año) seleccionado
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $certificados_redimidos_por_mes = [];
        $grafica_certificados_redimidos_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($listado_certificados as $registro) {
                if ($index + 1 == $registro->month) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($certificados_redimidos_por_mes, $object);
                    array_push($grafica_certificados_redimidos_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($certificados_redimidos_por_mes, $object);
                array_push($grafica_certificados_redimidos_por_mes['data'], 0);
            }
        }
        return [
            'data' => $grafica_certificados_redimidos_por_mes,
            'certificados' => $certificados_redimidos_por_mes,
        ];
    }

    public function beneficiosRedimidosPorMes()
    {
        $builder = $this->db->table('redemptions_by_benefits');
        $listado_certificados = $builder->select('SUM(amount) AS total, MONTH(created_at) AS month')->groupBy('month')
            ->where('enabled', 1)
            ->where('is_canceled', 0)
            ->where('YEAR(created_at)', $this->year)
            ->get()->getResult();

        // Preparar datos del periodo (año) seleccionado
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $certificados_redimidos_por_mes = [];
        $grafica_certificados_redimidos_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($listado_certificados as $registro) {
                if ($index + 1 == $registro->month) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($certificados_redimidos_por_mes, $object);
                    array_push($grafica_certificados_redimidos_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($certificados_redimidos_por_mes, $object);
                array_push($grafica_certificados_redimidos_por_mes['data'], 0);
            }
        }
        return [
            'data' => $grafica_certificados_redimidos_por_mes,
            'certificados' => $certificados_redimidos_por_mes,
        ];
    }

    public function certificados()
    {
        $builder = $this->db->table('certificates');
        $listado_certificados = $builder->select('id, title, subtitle, points, image, price, current_stock,, enabled, exchanges')
                                        ->get()->getResult();
        return $listado_certificados;
    }

    public function beneficios()
    {
        $builder = $this->db->table('benefits');
        $listado_beneficios = $builder->select('benefits.id, benefits.title, benefits.description, benefits.image, benefits.unlimited_stock, benefits.current_stock, benefits.enabled, SUM(redemptions.amount) as amount, redemptions.is_canceled')
            ->join('redemptions_by_benefits as redemptions', 'benefits.id = redemptions.benefit_id', 'left')
            ->where('redemptions.is_canceled', 0)
            ->orWhere('redemptions.is_canceled', null)
            ->groupBy('redemptions.benefit_id')
            ->get()->getResult();
        return $listado_beneficios;
    }
}
