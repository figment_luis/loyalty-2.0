<?php

namespace App\Models\Frontend;

use App\Models\Services\ClientModel;
use App\Models\Services\PointsModel;
use CodeIgniter\Model;

class TicketModel extends Model
{
    public function imprimirTicketRedenciones($client_id, $card_id, $points_redeemed, $date)
    {
        $clientModel = new ClientModel();
        $pointsModel = new PointsModel();

        $client = $clientModel->getClientInfoByID($client_id)[0];
        $points = $pointsModel->getPoints($client_id)[0];

        $this->db = db_connect('default');

        // Transacción: Obtener información completa de los certificados que figuran al emitir el ticket,
        // Establecer que registros se imprimieron, y
        // Generar un registro del ticket emitido
        $this->db->transBegin();

        $certificates = $this->db->table('redemptions_by_certificates AS r')
            ->select('r.amount, c.title, c.points, r.id')
            ->join('certificates as c', 'c.id = r.certificate_id')
            ->where('r.client_id', $client_id)
            ->where('r.card_id', $card_id)
            ->where('r.enabled', 1)
            ->where('r.created_at <=', $date)
            ->where('r.its_printed', 0)
            ->orderBy('r.id')
            ->get()->getResult();

        // Establecer estos registros a impresos
        $builder = $this->db->table('redemptions_by_certificates');
        $builder
            ->where('client_id', $client_id)
            ->where('card_id', $card_id)
            ->where('enabled', 1)
            ->where('created_at <=', $date)
            ->where('its_printed', 0)
            ->update(['its_printed' => 1, 'updated_at' => $date]);
        $builder->resetQuery();

        // Generar registro de ticket y relacionarlo con las operaciones de redencion
        $builder = $this->db->table('tickets_redemptions_by_certificates');
        $builder->insert([
            'user_id' => session()->get('user_id'),
            'client_id' => $client_id,
            'card_id' => $card_id,
            'points_redeemed' => $points_redeemed,
            'enabled' => 1,
            'created_at' => $date
        ]);
        $builder->resetQuery();

        $ticketID = $this->db->insertID();

        $dataPivot = [];
        foreach ($certificates as $certificate) {
            array_push($dataPivot, [
                'ticket_redemption_by_certificate_id' => $ticketID,
                'redemption_by_certificate_id' => $certificate->id
            ]);
        }

        $builder = $this->db->table('ticket_redemption_by_certificates_has_redemption_by_certificate');
        $builder->insertBatch($dataPivot);
        $builder->resetQuery();

        // Confirmar operaciones en base de datos
        if ($this->db->transStatus() !== FALSE) {
            $this->db->transCommit();
            $data = [
                'certificates' => $certificates,
                'date' => $date,
                'client' => $client['name'],
                'card_number' => $client['card_number'],
                'level' => $client['level'],
                'points' => number_format($points['points_available'], 0,'.', ','),
                'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
            ];
            return $data;
        } else {
            $this->db->transRollback();
            $response = [];
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible generar el ticket por concepto de redenciones en el sistema, favor de intentar mas tarde.";
            return $response;
        }
    }

    public function imprimirTicketAcreditaciones($client_id, $card_id, $date)
    {
        $clientModel = new ClientModel();
        $pointsModel = new PointsModel();

        $client = $clientModel->getClientInfoByID($client_id)[0];
        $points = $pointsModel->getPoints($client_id)[0];

        $this->db = db_connect('default');
        // Transacción: Obtener información completa de las transacciones generadas por acreditación de puntos que figuran al emitir el ticket (recibos de tienda),
        // Establecer que registros se imprimieron, y
        // Generar un registro del ticket emitido
        $this->db->transBegin();

        $transactions = $this->db->table('receipts AS r')
            ->select('s.name as store, r.amount, t.points, t.id')
            ->join('transactions as t', 'r.id = t.receipt_id')
            ->join('stores as s', 's.id = r.store_id')
            ->where('t.client_id', $client_id)
            ->where('t.card_id', $card_id)
            ->where('t.enabled', 1)
            ->where('t.created_at <=', $date)
            ->where('t.its_printed', 0)
            ->orderBy('t.id')
            ->get()->getResult();

        // Establecer estos registros a impresos
        $this->db->table('transactions')
            ->where('client_id', $client_id)
            ->where('card_id', $card_id)
            ->where('enabled', 1)
            ->where('created_at <=', $date)
            ->where('its_printed', 0)
            ->update(['its_printed' => 1, 'updated_at' => $date]);

        // FOR PARA CONOCER EL TOTAL DE PUNTOS ACREDITADOS EN ESTE TICKET
        $credited_points = 0;
        foreach ($transactions as $transaction) {
            $credited_points += $transaction->points;
        }

        // Generar registro de ticket que asocie una o varias transacciones
        $this->db->table('tickets_transactions')
            ->insert([
                'user_id' => session()->get('user_id'),
                'client_id' => $client_id,
                'card_id' => $card_id,
                'level' => $client['level'],
                'points_at_that_time' => $points['points_available'],
                'credited_points' => $credited_points,
                'enabled' => 1,
                'created_at' => $date
            ]);

        $ticketID = $this->db->insertID();

        $dataPivot = [];
        foreach ($transactions as $transaction) {
            array_push($dataPivot, [
                'ticket_transaction_id' => $ticketID,
                'transaction_id' => $transaction->id
            ]);
        }

        $this->db->table('ticket_transaction_has_transaction')->insertBatch($dataPivot);

        // Confirmar operaciones en base de datos
        if ($this->db->transStatus() !== FALSE) {
            $this->db->transCommit();
            $data = [
                'transactions' => $transactions,
                'date' => $date,
                'client' => $client['name'],
                'card_number' => $client['card_number'],
                'level' => $client['level'],
                'points' => number_format($points['points_available'],0,'.',','),
                'credited_points' => number_format($credited_points,0,'.',','),
                'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
            ];
            return $data;
        } else {
            $this->db->transRollback();
            $response = [];
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible generar el ticket por concepto de acreditación de puntos en el sistema, favor de intentar mas tarde.";
            return $response;
        }
    }

    public function imprimirTicketEntregaDeBeneficios($client_id, $card_id, $date)
    {
        $clientModel = new ClientModel();
        $pointsModel = new PointsModel();

        $client = $clientModel->getClientInfoByID($client_id)[0];
        $points = $pointsModel->getPoints($client_id)[0];

        $this->db = db_connect('default');
        // Transacción: Obtener información completa de los beneficios entregados que figuran al emitir el ticket,
        // Establecer que registros se imprimieron, y
        // Generar un registro del ticket emitido
        $this->db->transBegin();

        $benefits = $this->db->table('redemptions_by_benefits AS r')
            ->select('b.title, b.subtitle, b.description, r.amount, r.id')
            ->join('benefits as b', 'b.id = r.benefit_id')
            ->where('r.client_id', $client_id)
            ->where('r.card_id', $card_id)
            ->where('r.enabled', 1)
            ->where('r.created_at <=', $date)
            ->where('r.its_printed', 0)
            ->orderBy('r.id')
            ->get()->getResult();

        // Establecer estos registros a impresos
        $this->db->table('redemptions_by_benefits')
            ->where('client_id', $client_id)
            ->where('card_id', $card_id)
            ->where('enabled', 1)
            ->where('created_at <=', $date)
            ->where('its_printed', 0)
            ->update(['its_printed' => 1, 'updated_at' => $date]);

        // Generar registro de ticket que asocie una o varias entregas de beneficios
        $this->db->table('tickets_redemptions_by_benefits')
            ->insert([
                'user_id' => session()->get('user_id'),
                'client_id' => $client_id,
                'card_id' => $card_id,
                'level_at_that_time' => $client['level'],
                'enabled' => 1,
                'created_at' => $date
            ]);

        $ticketID = $this->db->insertID();

        $dataPivot = [];
        foreach ($benefits as $benefit) {
            array_push($dataPivot, [
                'ticket_redemption_by_benefit_id' => $ticketID,
                'redemption_by_benefit_id' => $benefit->id
            ]);
        }

        $this->db->table('ticket_redemption_by_benefits_has_redemption_by_benefit')->insertBatch($dataPivot);

        // Confirmar operaciones en base de datos
        if ($this->db->transStatus() !== FALSE) {
            $this->db->transCommit();
            $data = [
                'benefits' => $benefits,
                'date' => $date,
                'client' => $client['name'],
                'card_number' => $client['card_number'],
                'level' => $client['level'],
                'points' => number_format($points['points_available'],0,'.',','),
                'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
            ];
            return $data;
        } else {
            $this->db->transRollback();
            $response = [];
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible generar el ticket por concepto de entrega de beneficios en el sistema, favor de intentar mas tarde.";
            return $response;
        }
    }

    public function reimprimirTicketRedencion($ticket_id, $date)
    {
        $this->db = db_connect('default');

        $certificates = $this->db->table('tickets_redemptions_by_certificates AS t')
            ->select('r.amount, c.title, c.points, r.id, r.is_canceled')
            ->join('ticket_redemption_by_certificates_has_redemption_by_certificate as pivot', 't.id = pivot.ticket_redemption_by_certificate_id')
            ->join('redemptions_by_certificates as r', 'r.id = pivot.redemption_by_certificate_id')
            ->join('certificates as c', 'c.id = r.certificate_id')
            ->where('r.enabled', 1)
            //->where('r.is_canceled', 0)
            ->where('t.enabled', 1)
            ->where('t.id', $ticket_id)
            ->orderBy('r.id')
            ->get()->getResult();

        $data = [
            'certificates' => $certificates,
            'date' => $date,
            'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
        ];
        return $data;
    }

    public function reimprimirTicketAcreditacion($ticket_id, $date)
    {
        $this->db = db_connect('default');

        $transactions = $this->db->table('tickets_transactions AS ticket')
            ->select('s.name as store, r.amount, t.points, t.id, t.is_canceled')
            ->join('ticket_transaction_has_transaction as pivot', 'ticket.id = pivot.ticket_transaction_id')
            ->join('transactions as t', 't.id = pivot.transaction_id')
            ->join('receipts as r', 'r.id = t.receipt_id')
            ->join('stores as s', 's.id = r.store_id')
            ->where('t.enabled', 1)
            //->where('t.is_canceled', 0)
            ->where('r.enabled', 1)
            ->where('ticket.enabled', 1)
            ->where('ticket.id', $ticket_id)
            ->orderBy('r.id')
            ->get()->getResult();

        $data = [
            'transactions' => $transactions,
            'date' => $date,
            'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
        ];
        return $data;
    }

    public function reimprimirTicketBeneficio($ticket_id, $date)
    {
        $this->db = db_connect('default');

        $benefits = $this->db->table('tickets_redemptions_by_benefits AS ticket')
            ->select('b.title, b.subtitle, b.description, r.amount, r.id, r.is_canceled')

            ->join('ticket_redemption_by_benefits_has_redemption_by_benefit as pivot', 'ticket.id = pivot.ticket_redemption_by_benefit_id')
            ->join('redemptions_by_benefits as r', 'r.id = pivot.redemption_by_benefit_id')
            ->join('benefits as b', 'b.id = r.benefit_id')
            ->where('r.enabled', 1)
            //->where('r.is_canceled', 0)
            ->where('ticket.enabled', 1)
            ->where('ticket.id', $ticket_id)
            ->orderBy('r.id')
            ->get()->getResult();

        $data = [
            'benefits' => $benefits,
            'date' => $date,
            'concierge' => session()->get('first_name') . ' ' . session()->get('last_name')
        ];
        return $data;
    }
}
