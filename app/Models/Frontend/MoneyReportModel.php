<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class MoneyReportModel extends Model
{
    public function __construct()
    {
        $this->db = db_connect('default');
    }

    public function dineroTickets($fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('receipts');

        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, stores.name as store, receipts.amount, receipts.date, cards.number as card_number, level.name as level, type.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, receipts.date) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as level', 'level.id = card_level_id')
            ->join('card_types as type', 'type.id = card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->join('receipt_user_logs as logs', 'logs.receipt_id = receipts.id')
            ->join('users', 'users.id = logs.user_id')
            ->where('DATE(receipts.date) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->orderBy('receipts.date')
            ->get()->getResult();
        return $tickets;
    }

    public function dineroRedenciones($fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('certificates');

        $redemptions = $builder->select('certificates.title, certificates.subtitle, certificates.points, certificates.price, redemptions.amount, (redemptions.amount * certificates.price) as total, redemptions.created_at as date, clients.first_name, clients.last_name, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('DATE(redemptions.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $redemptions;
    }

    public function dineroTicketsPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('receipts');

        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('DATE(receipts.date) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->where('info.gender', $genderName)
            ->orderBy('receipts.date')
            ->get()->getResult();
        return $tickets;
    }

    public function dineroTicketsPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('receipts');

        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('DATE(receipts.date) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->where('levels.name', $levelName)
            ->orderBy('receipts.date')
            ->get()->getResult();
        return $tickets;
    }

    public function dineroTicketsPorRangoDeEdad($rangoEdad, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('receipts');
        list($edadMin, $edadMax) = explode(' a ', $rangoEdad);

        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) BETWEEN ' . $edadMin . ' AND ' . $edadMax)
            ->where('DATE(receipts.date) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->orderBy('receipts.date')
            ->get()->getResult();
        return $tickets;
    }

    public function dineroRedencionesPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('certificates');

        $redemptions = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, SUM(redemptions.amount * certificates.price) as amount, clients.email, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, info.gender, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as state, municipality.name as municipality, cards.number as card_number, levels.name as level, types.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('DATE(redemptions.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->where('info.gender', $genderName)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $redemptions;
    }

    public function dineroRedencionesPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $builder = $this->db->table('certificates');

        $redemptions = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, SUM(redemptions.amount * certificates.price) as amount, clients.email, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, info.gender, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as state, municipality.name as municipality, cards.number as card_number, levels.name as level, types.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('DATE(redemptions.created_at) BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->where('levels.name', $levelName)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $redemptions;
    }
}
