<?php

namespace App\Models\Frontend;

use CodeIgniter\Model;

class BenefitReportModel extends Model
{
    public function __construct()
    {
        helper('custom');
        $this->db = db_connect('default');
    }

    public function certificadosEntregados($id)
    {
        $builder = $this->db->table('certificates');
        $certificates = $builder->select('certificates.title, certificates.subtitle as brand, certificates.description, redemptions.amount, redemptions.created_at as date, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('redemption_by_certificate_user_logs as logs', 'redemptions.id = logs.redemption_by_certificate_id')
            ->join('users', 'users.id = logs.user_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('certificates.id', $id)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $certificates;
    }

	public function beneficiosEntregados($id)
    {
        $builder = $this->db->table('benefits');

        $benefits = $builder->select('benefits.title, benefits.subtitle as brand, benefits.description, redemptions.amount, redemptions.created_at as date, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('redemptions_by_benefits as redemptions', 'benefits.id = redemptions.benefit_id')
            ->join('redemption_by_benefit_user_logs as logs', 'redemptions.id = logs.redemption_by_benefit_id')
            ->join('users', 'users.id = logs.user_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('benefits.id', $id)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $benefits;
    }

    public function certificadosEntregadosPorPeriodo($year)
    {
        $builder = $this->db->table('certificates');
        $certificates = $builder->select('certificates.title, certificates.subtitle as brand, certificates.description, redemptions.amount, redemptions.created_at as date, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('redemption_by_certificate_user_logs as logs', 'redemptions.id = logs.redemption_by_certificate_id')
            ->join('users', 'users.id = logs.user_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('YEAR(redemptions.created_at)', $year)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $certificates;
    }

    public function beneficiosEntregadosPorPeriodo($year)
    {
        $builder = $this->db->table('benefits');

        $benefits = $builder->select('benefits.title, benefits.subtitle as brand, benefits.description, redemptions.amount, redemptions.created_at as date, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('redemptions_by_benefits as redemptions', 'benefits.id = redemptions.benefit_id')
            ->join('redemption_by_benefit_user_logs as logs', 'redemptions.id = logs.redemption_by_benefit_id')
            ->join('users', 'users.id = logs.user_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('YEAR(redemptions.created_at)', $year)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();
        return $benefits;
    }
}
