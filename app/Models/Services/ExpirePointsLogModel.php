<?php

namespace App\Models;

use CodeIgniter\Model;

class ExpirePointsLogModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'expire_points_log';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'client_id',
    'points_expired',
    'points_redeemed'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;



    public function getExpirePoints($client_id){
        $query= $this->query("SELECT * FROM expire_points_log WHERE client_id = '$client_id'");
        return $query->getResultArray();
    }

}
