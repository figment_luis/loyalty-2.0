<?php

namespace App\Models;

use CodeIgniter\Model;

class CertificatesLogStockModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'certificate_stock_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'increase',
    'decrease',
    'certificate_id',
    'user_id',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
