<?php

namespace App\Models;

use CodeIgniter\Model;

class BenefitsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'benefits';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'title',
    'subtitle',
    'description',
    'image',
    'unlimited_stock',
    'initial_stock',
    'current_stock',
    'enabled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function getBenefitsByPoints($level){
        $query= $this->query("SELECT b.*, bl.level_id FROM benefits as b LEFT JOIN benefit_levels as bl ON bl.benefit_id = b.id WHERE bl.level_id <= $level AND (b.current_stock > 0 OR b.unlimited_stock=1) AND enabled = 1");
        return $query->getResultArray();
    }

    public function getBenefitInfo($id){
        $query= $this->query("SELECT b.*, bl.level_id FROM benefits AS b LEFT JOIN benefit_levels AS bl ON b.id = bl.benefit_id WHERE b.id = $id ORDER BY bl.level_id");
        return $query->getResultArray();
    }
}