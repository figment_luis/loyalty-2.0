<?php

namespace App\Models;

use CodeIgniter\Model;

class CardsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'cards';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'number',
    'enabled',
    'card_type_id',
    'card_status_id',
    'card_level_id',
    'card_reason_id',
    'client_id',
    'deleted_at',
    'assigned_at'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getCardInfo($card){
        $query= $this->query("SELECT * FROM cards WHERE number = '$card'");
        return $query->getResultArray();
    }

    public function getCardInfoByClientID($id){
        $query= $this->query("SELECT ca.* FROM cards AS ca LEFT JOIN clients AS ci ON ci.id = ca.client_id WHERE ci.id='$id' AND ca.enabled = 1");
        return $query->getResultArray();
    }

    public function getCardAndLevel($id){
        $query= $this->query("SELECT c.id, c.number, cl.factor FROM cards AS c LEFT JOIN card_levels AS cl ON c.card_level_id=cl.id where c.id = '$id'");
        return $query->getResultArray();
    }

    public function getCardLevels(){
        $query= $this->query("SELECT id, name, points FROM card_levels WHERE enabled = 1 ORDER BY points ASC");
        return $query->getResultArray();
    }
}
