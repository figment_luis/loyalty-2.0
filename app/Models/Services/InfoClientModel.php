<?php

namespace App\Models;

use CodeIgniter\Model;

class InfoClientModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'client_info';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'email_alternative',
    'telephone',
    'mobile',
    'gender',
    'birthday',
    'street',
    'exterior',
    'interior',
    'codigo_postal',
    'estado',
    'municipio',
    'colonia',
    'client_id'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}
