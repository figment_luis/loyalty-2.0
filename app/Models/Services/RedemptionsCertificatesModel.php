<?php

namespace App\Models;

use CodeIgniter\Model;

class RedemptionsCertificatesModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'redemptions_by_certificates';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'amount',
    'enabled',
    'certificate_id',
    'card_id',
    'client_id',
    'is_canceled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function validate_last_shippings($id, $days){
        $query= $this->query("SELECT SUM(r.amount) as amount FROM receipts AS r LEFT JOIN transactions AS t ON t.receipt_id = r.id WHERE t.client_id = $id AND r.date >= ( CURDATE() - INTERVAL $days DAY ) AND t.is_canceled = 0");
        return $query->getResultArray();
    }

    public function getRedemptionsCertificatesByClient($client_id){
        $query= $this->query("SELECT r.created_at, b.title, c.number, r.amount from redemptions_by_certificates AS r LEFT JOIN certificates AS b ON b.id = r.certificate_id LEFT JOIN cards AS c ON c.id=r.card_id WHERE r.client_id = $client_id AND r.enabled = 1 AND r.is_canceled = 0");
        return $query->getResultArray();
    }

    public function getRedemptionById($id){
        $query= $this->query("SELECT * from redemptions_by_certificates WHERE id = $id AND enabled = 1 AND is_canceled = 0");
        return $query->getResultArray();
    }


}
