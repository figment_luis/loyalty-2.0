<?php

namespace App\Models;

use CodeIgniter\Model;

class RedemptionsCertificatesLogModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'redemption_by_certificate_user_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'redemption_by_certificate_id',
    'user_id',
    'returned_points',
    'description',
    'operation_id'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}
