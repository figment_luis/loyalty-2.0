<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'first_name',
    'last_name',
    'email',
    'password',
    'role_id',
    'enabled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function getLoguinInfo($email){
        $query= $this->query("SELECT * FROM users WHERE email = '$email'");
        return $query->getResultArray();
    }

    public function getRoleInfo($id){
        $query= $this->query("SELECT * FROM user_roles WHERE id = '$id'");
        return $query->getResultArray();
    }

}
