<?php

namespace App\Models;

use CodeIgniter\Model;

class RedemptionsBenefitsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'redemptions_by_benefits';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'amount',
    'enabled',
    'benefit_id',
    'card_id',
    'client_id',
    'card_level_id',
    'is_canceled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function validate_time_days($benefit_id, $client_id){
        $query= $this->query("SELECT SUM(amount) AS quantity FROM redemptions_by_benefits WHERE client_id = $client_id AND benefit_id = $benefit_id AND created_at >= ( CURDATE() - INTERVAL 0 DAY) AND is_canceled = 0");
        return $query->getResultArray();
    }

    public function validate_time_months($benefit_id, $client_id){
        $query= $this->query("SELECT SUM(amount) AS quantity FROM redemptions_by_benefits WHERE client_id = $client_id AND benefit_id = $benefit_id AND created_at >= ( CURDATE() - INTERVAL 1 MONTH ) AND is_canceled = 0");
        return $query->getResultArray();
    }

    public function validate_time_week($benefit_id, $client_id){
        $query= $this->query("SELECT SUM(amount) as quantity  FROM redemptions_by_benefits WHERE client_id = $client_id AND benefit_id = $benefit_id AND YEARWEEK(created_at, 1) = YEARWEEK(CURDATE(), 1) AND is_canceled = 0");
        return $query->getResultArray();
    }

    public function getRedemptionsBenefitsByClient($client_id){
        $query= $this->query("SELECT r.created_at, b.title, c.number, r.amount from redemptions_by_benefits AS r LEFT JOIN benefits AS b ON b.id = r.benefit_id LEFT JOIN cards AS c ON c.id=r.card_id WHERE r.client_id = $client_id AND r.enabled = 1 AND r.is_canceled = 0");
        return $query->getResultArray();
    }

    public function getRedemptionById($id){
        $query= $this->query("SELECT * from redemptions_by_benefits WHERE id = $id AND enabled = 1 AND is_canceled = 0");
        return $query->getResultArray();
    }


}
