<?php

namespace App\Models;

use CodeIgniter\Model;

class ReceiptLogModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'receipt_user_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'receipt_id',
    'user_id',
    'description',
    'operation_id'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}
