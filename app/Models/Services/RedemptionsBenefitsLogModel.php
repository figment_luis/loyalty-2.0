<?php

namespace App\Models;

use CodeIgniter\Model;

class RedemptionsBenefitsLogModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'redemption_by_benefit_user_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'redemption_by_benefit_id',
    'user_id',
    'description',
    'operation_id'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}
