<?php

namespace App\Models\Services;

use CodeIgniter\Model;

class PointsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'points';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'points_available',
    'points_redeemed',
    'client_id',
    'debt_points'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getPoints($client_id){
        $query= $this->query("SELECT * FROM points WHERE client_id = '$client_id'");
        return $query->getResultArray();
    }

    public function getLevelPoints($client_id, $months){
        $query= $this->query("SELECT SUM(t.points) AS points FROM receipts AS r LEFT JOIN transactions AS t ON t.receipt_id = r.id WHERE t.client_id = $client_id AND t.created_at >= ( CURDATE() - INTERVAL $months MONTH ) AND t.is_canceled = 0");
        return $query->getResultArray();
    }

    public function getAllRegisters(){
        $query= $this->query("SELECT * FROM points");
        return $query->getResultArray();
    }

    public function getReportPoints(){
        $query= $this->query("SELECT concat_ws(' ', c.first_name, c.last_name) AS Nombre, ca.number AS Tarjeta, cl.name as Nivel, p.points_available AS 'Puntos Disponibles', p.points_redeemed AS 'Puntos Redimidos' FROM points as p LEFT JOIN clients AS c ON c.id = p.client_id LEFT JOIN cards AS ca ON ca.client_id = c.id LEFT JOIN card_levels AS cl ON ca.card_level_id = cl.id");
        return $query->getResultArray();
    }

}
