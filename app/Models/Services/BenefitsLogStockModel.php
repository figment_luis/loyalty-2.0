<?php

namespace App\Models;

use CodeIgniter\Model;

class BenefitsLogStockModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'benefit_stock_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'increase',
    'decrease',
    'benefit_id',
    'user_id',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
