<?php

namespace App\Models;

use CodeIgniter\Model;

class AcessLogModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'access_logs';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'last_access',
    'ip_address',
    'user_agent',
    'device',
    'user_id'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'last_access';
    protected $updatedField  = 'last_access';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
