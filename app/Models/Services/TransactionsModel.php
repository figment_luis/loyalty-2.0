<?php

namespace App\Models;

use CodeIgniter\Model;

class TransactionsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'transactions';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'points',
    'redeemed',
    'total',
    'enabled',
    'card_id',
    'client_id',
    'receipt_id',
    'is_canceled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getTransactionByReceiptId($id){
        $query= $this->query("SELECT * FROM transactions WHERE receipt_id = $id");
        return $query->getResultArray();
    }
}
