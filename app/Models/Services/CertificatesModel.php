<?php

namespace App\Models;

use CodeIgniter\Model;

class CertificatesModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'certificates';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'title',
    'subtitle',
    'description',
    'points',
    'price',
    'image',
    'initial_stock',
    'current_stock',
    'exchanges',
    'enabled'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function getCertificatesByPoints($points){
        $query= $this->query("SELECT * FROM certificates where points <= '$points' AND enabled = 1 AND current_stock > 0");
        return $query->getResultArray();
    }

    public function getCertificateInfo($id){
        $query= $this->query("SELECT * FROM certificates where id=$id");
        return $query->getResultArray();
    }

}
