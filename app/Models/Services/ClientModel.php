<?php

namespace App\Models\Services;

use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'clients';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'first_name',
    'last_name',
    'email',
    'password',
    'newsletter',
    'first_year',
    'enabled',
    'available'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function checkEmail($email){
        $query= $this->query("SELECT * FROM clients WHERE email = '$email'");
        return $query->getResultArray();
    }

    public function searchClient($search, $column){
        switch ($column) {
            case 'type_number':
                $query= $this->query("SELECT c.id as client_id, CONCAT_WS(' ', c.first_name, c.last_name) AS nombre, c.email, ci.telephone, ca.number  FROM clients AS c LEFT JOIN cards AS ca ON ca.client_id = c.id LEFT JOIN client_info as ci ON ci.client_id = c.id WHERE ca.number = '$search' AND ca.enabled = 1 ");
                break;
            case 'name':
                $query= $this->query("SELECT c.id as client_id, CONCAT_WS(' ', c.first_name, c.last_name) AS nombre, c.email, ci.telephone, ca.number  FROM clients AS c LEFT JOIN cards AS ca ON ca.client_id = c.id LEFT JOIN client_info as ci ON ci.client_id = c.id WHERE CONCAT_WS(' ', c.first_name, c.last_name) like '%$search%'  AND ca.enabled = 1");
                break;
            case 'email':
                $query= $this->query("SELECT c.id as client_id, CONCAT_WS(' ', c.first_name, c.last_name) AS nombre, c.email, ci.telephone, ca.number  FROM clients AS c LEFT JOIN cards AS ca ON ca.client_id = c.id LEFT JOIN client_info as ci ON ci.client_id = c.id WHERE c.email like '%$search%'  AND ca.enabled = 1");
                break;
            case 'telephone':
                    $query= $this->query("SELECT c.id as client_id, CONCAT_WS(' ', c.first_name, c.last_name) AS nombre, c.email, ci.telephone, ca.number  FROM clients AS c LEFT JOIN cards AS ca ON ca.client_id = c.id LEFT JOIN client_info as ci ON ci.client_id = c.id WHERE (ci.telephone like '%$search%' OR ci.mobile like '%$search%') AND ca.enabled = 1");
                break;
            default:
                return false;
                break;
        }
        return $query->getResultArray();
    }

    public function getClientInfoByID($id){
        $query= $this->query("SELECT DISTINCT c.id as client_id, CONCAT_WS(' ', c.first_name, c.last_name) AS name, ca.id as card_id, ca.number AS card_number, ci.telephone AS telephone, c.email, c.first_year, l.name as level, l.factor as factor FROM clients AS c LEFT JOIN client_info AS ci ON c.id = ci.client_id LEFT JOIN cards AS ca ON c.id = ca.client_id LEFT JOIN card_levels AS l ON ca.card_level_id = l.id LEFT JOIN transactions AS t ON t.client_id = c.id WHERE c.id = '$id' AND ca.enabled = 1");
        return $query->getResultArray();
    }

    public function getContactClientInfoByID($id){
        $query= $this->query("SELECT * FROM client_info WHERE client_id = '$id'");
        return $query->getResultArray();
    }

    public function getStates(){
        $query= $this->query("SELECT * FROM states");
        return $query->getResultArray();
    }

    public function searchMunicipalities($state){
        $query= $this->query("SELECT * FROM municipality WHERE state_id = '$state' ORDER BY name ASC");
        return $query->getResultArray();
    }

    public function searchSuburbs($municipality){
        $query= $this->query("SELECT * FROM suburb WHERE municipality_id = '$municipality' ORDER BY name ASC");
        return $query->getResultArray();
    }


    /*Functions for client Points Update*/

    public function checkFirstYear($id){
        $query= $this->query("SELECT TIMESTAMPDIFF(YEAR, created_at, CURDATE()) AS years FROM clients WHERE id = $id");
        return $query->getResultArray();
    }
}
