<?php

namespace App\Models\Services;

use CodeIgniter\Model;

class PointsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'points_history';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'points_available',
    'points_redeemed',
    'client_id',
    'points_expired',
    'debt_points',
    'date'
    ];

    protected $useTimestamps = false;


    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function saveActualData(){

        $date = date('Y-m-d');

        $query= $this->query("INSERT INTO points_history (points_available, points_redeemed, points_expired, debt_points, client_id, date) SELECT points_available, points_redeemed, points_expired, debt_points, client_id, '$date' FROM points");

        return $query->getResultArray();
    }

    public function getAvailableDates(){

        $query= $this->query("SELECT DISTINCT date FROM points_history GROUP BY date");

        return $query->getResultArray();
    }

    public function getHistoricByDate($date){

        $query= $this->query("SELECT CONCAT_WS(' ', cl.first_name, cl.last_name) AS Nombre, c.number as Tarjeta, p.points_available, p.points_redeemed, p.points_expired, p.debt_points, p.date FROM points_history p LEFT JOIN clients cl ON cl.id = p.client_id LEFT JOIN cards c ON c.client_id = cl.id WHERE date = '$date' AND c.enabled = 1;");

        return $query->getResultArray();
    }

    public function getDataHistoricByDate($date){

        $query= $this->query("SELECT SUM(p.points_available) AS Disponibles, SUM(p.points_redeemed) AS Redimidos, SUM(p.points_expired) AS Expirados, SUM(p.debt_points) AS 'En deuda' FROM points_history p
        WHERE p.date = '$date';");

        return $query->getResultArray();
    }

}
