<?php

namespace App\Models;

use CodeIgniter\Model;

class ReceiptsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'receipts';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    'number',
    'amount',
    'encrypted',
    'enabled',
    'store_id',
    'date'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function searchReceiptByEncrypted($encrypted){
        $query= $this->query("SELECT * FROM receipts WHERE encrypted = '$encrypted'");
        return $query->getResultArray(); 
    }
}
