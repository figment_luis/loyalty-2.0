<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\PartnerReportModel;

class PartnerReportController extends BaseController
{
    public function sociosRegistrados($fechaInicio, $fechaFin)
    {
        $this->setHeader('reporte-socios');

        $partnerReportModel = new PartnerReportModel();
        $socios = $partnerReportModel->sociosRegistrados($fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono Fijo', 'Teléfono Móvil', 'Fecha de Nacimiento', 'Edad', 'Género', 'Estado', 'Municipio o Delegación', 'Número de Tarjeta', 'Tipo de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $socios as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->birthday, $line->age, $line->gender, $line->estado, $line->municipio, $line->card_number, $line->card_type, $line->level, $line->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosComprasPorTienda($tienda, $fechaInicio, $fechaFin)
    {
        $this->setHeader('reporte-socios-compras-por-tienda');

        $partnerReportModel = new PartnerReportModel();
        $socios = $partnerReportModel->sociosComprasPorTienda($tienda, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Tienda', 'Monto de Compra', 'Fecha de Compra']);

        foreach ( $socios as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->store, $line->amount, $line->date];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorGenero($genero, $fechaInicio, $fechaFin)
    {
        $this->setHeader('reporte-socios-por-genero');

        $partnerReportModel = new PartnerReportModel();
        $socios = $partnerReportModel->sociosPorGenero($genero, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $socios as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorNivel($nivel, $fechaInicio, $fechaFin)
    {
        $this->setHeader('reporte-socios-por-nivel');

        $partnerReportModel = new PartnerReportModel();
        $socios = $partnerReportModel->sociosPorNivel($nivel, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $socios as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorRangoDeEdad($rango, $fechaInicio, $fechaFin)
    {
        $this->setHeader('reporte-socios-por-rango-de-edad');

        $partnerReportModel = new PartnerReportModel();
        $socios = $partnerReportModel->sociosPorRangoDeEdad($rango, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Nacimiento', 'Edad', 'Fecha de Registro']);

        foreach ( $socios as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->birthday, $line->age, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    private function setHeader($fileName)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '-' . date('Y-m-d') . '.csv');
    }

    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }
}
