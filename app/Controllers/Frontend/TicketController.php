<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\TicketModel;

class TicketController extends BaseController
{
    public function imprimirTicketRedencion()
    {
        $client_id = $this->request->getPost('client_id');
        $card_id = $this->request->getPost('card_id');
        $points_redeemed = $this->request->getPost('points_redeemed');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->imprimirTicketRedenciones($client_id, $card_id, $points_redeemed, $date);

        return $this->response->setJSON($ticket);

    }

    public function imprimirTicketAcreditacion()
    {
        $client_id = $this->request->getPost('client_id');
        $card_id = $this->request->getPost('card_id');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->imprimirTicketAcreditaciones($client_id, $card_id, $date);

        return $this->response->setJSON($ticket);
    }

    public function imprimirTicketEntregaDeBeneficios()
    {
        $client_id = $this->request->getPost('client_id');
        $card_id = $this->request->getPost('card_id');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->imprimirTicketEntregaDeBeneficios($client_id, $card_id, $date);

        return $this->response->setJSON($ticket);
    }

    public function reimprimirTicketRedencion()
    {
        $ticket_id = $this->request->getPost('ticket_id');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->reimprimirTicketRedencion($ticket_id, $date);

        return $this->response->setJSON($ticket);
    }

    public function reimprimirTicketAcreditacion()
    {
        $ticket_id = $this->request->getPost('ticket_id');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->reimprimirTicketAcreditacion($ticket_id, $date);

        return $this->response->setJSON($ticket);
    }

    public function reimprimirTicketBeneficio()
    {
        $ticket_id = $this->request->getPost('ticket_id');
        $date = date('Y-m-d H:i:s');

        $ticketModel = new TicketModel();
        $ticket = $ticketModel->reimprimirTicketBeneficio($ticket_id, $date);

        return $this->response->setJSON($ticket);
    }
}
