<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;

class PageController extends \App\Controllers\Services\Rules
{
    private $session = null;

    public function __construct()
    {
        $this->session = \Config\Services::session();
        helper('custom');
    }

    public function index()
    {
        return view('pages/panel-de-control');
    }

    public function registrarUsuario()
    {
        $clientModel = model('App\Models\Services\ClientModel');
        $states = $clientModel->getStates();

        // TODO: Generar un servicio para recuperar los niveles de tarjeta disponibles en base de datos
        $this->db = db_connect('default');
        $builder = $this->db->table('card_levels');
        $cardLevels = $builder->where('enabled', 1)->get()->getResult();

        return view('pages/registrar-socio', compact('cardLevels', 'states'));
    }

	public function acreditarPuntos($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        // TODO: Generar un servicio para recuperar todas las tiendas habilitadas en el plan de lealtad
        $this->db = db_connect('default');
        $builder = $this->db->table('stores');
        $stores = $builder->where('enabled', 1)->get()->getResult();

        return view('pages/acreditar-puntos', compact('client', 'stores', 'points'));
    }

    public function catalogoDePremios($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $certificatesModel = model('App\Models\Services\CertificatesModel');
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        $certificates = $certificatesModel->getCertificatesByPoints($points['points_available']);

        // TODO: Listado de 2 premios aleatorios que el usuario puede canjear en caso que no tenga puntos suficientes
        $this->db = db_connect('default');
        $builder = $this->db->table('certificates');
        $certificates_shift = $builder->where('enabled', 1)
            ->where('current_stock >', 0)
            ->orderBy('points', 'DESC')
            ->limit(2)
            ->get()->getResult();


        return view('pages/catalogo-de-premios', compact('client', 'points', 'certificates', 'certificates_shift'));
    }

    public function redenciones($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $certificateModel = model('App\Models\Services\CertificatesModel');

        $client = $clientModel->getClientInfoByID($id)[0];
        $points = $pointsModel->getPoints($id)[0];

        $certificates = $certificateModel->getCertificatesByPoints($points['points_available']);

        // TODO: Listado de 2 premios aleatorios que el usuario puede canjear en caso que no tenga puntos suficientes
        $this->db = db_connect('default');
        $builder = $this->db->table('certificates');
        $certificates_shift = $builder->where('enabled', 1)
            ->where('current_stock >', 0)
            ->orderBy('points', 'DESC')
            ->limit(2)
            ->get()->getResult();
        //dd($certificates);

        return view('pages/redenciones', compact('client', 'certificates', 'certificates_shift', 'points'));
    }

    public function tarjetasBeneficios()
    {
        // TODO: Generar un servicio para recuperar el listado de premios que puede canjear este usuario
        $this->db = db_connect('default');
        $builder = $this->db->table('card_levels');
        $cards = $builder->where('enabled', 1)->get()->getResult();

        return view('pages/tarjetas-y-beneficios', compact('cards'));
    }

    public function listadoDePremios()
    {
        // TODO: Generar un servicio para recuperar el listado de premios que puede canjear este usuario
        $this->db = db_connect('default');
        $builder = $this->db->table('certificates');
        $certificates = $builder->where('enabled', 1)->where('current_stock >', 0)->get()->getResult();
        return view('pages/listado-de-premios', compact('certificates'));
    }

    public function listadoDeBeneficios()
    {
        // TODO: Generar un servicio para recuperar el listado de premios que puede canjear este usuario
        $this->db = db_connect('default');
        $builder = $this->db->table('benefits');
        $benefits = $builder->where('enabled', 1)
            ->groupStart()
            ->where('current_stock >', 0)
            ->orWhere('unlimited_stock', 1)
            ->groupEnd()
            ->orderBy('id', 'DESC')
            ->get()->getResultArray();

        $builder = $this->db->table('benefit_levels');
        $pivot = $builder->get()->getResult();

        $newBenefits = [];
        foreach ($benefits as $b) {
            $temporalLevels = [];
            foreach ($pivot as $p) {
                if ($b['id'] == $p->benefit_id) {
                    if ($p->level_id == 1) array_push($temporalLevels, 'Blue');
                    if ($p->level_id == 2) array_push($temporalLevels, 'Silver');
                    if ($p->level_id == 3) array_push($temporalLevels, 'Gold');
                }
            }
            $b['levels'] = implode(', ', $temporalLevels);
            array_push($newBenefits, $b);
        }

        //dd($newBenefits);


        return view('pages/listado-de-beneficios', compact('newBenefits'));
    }

    public function entregaDeBeneficios($id)
    {

        $this->updateClientLevel($id);

        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $benefitModel = model('App\Models\Services\BenefitsModel');
        $cardModel = model('App\Models\Services\CardsModel');
        $client = $clientModel->getClientInfoByID($id)[0];
        $points = $pointsModel->getPoints($id)[0];
        $levelPoints = $pointsModel->getLevelPoints($id, 18)[0];

        $cardInfo = $cardModel->getCardInfoByClientID($id)[0];
        $benefits= $benefitModel->getBenefitsByPoints($cardInfo['card_level_id']);
        $benefitsAll = $benefitModel
            ->where('enabled', 1)
            ->groupStart()
            ->where('current_stock >', 0)
            ->orWhere('unlimited_stock', 1)
            ->groupEnd()
            ->orderBy('RAND()')
            ->limit(2)
            ->get()->getResult();

        // TODO: Servicio para consultar los niveles de tarjeta y los puntos requeridos
        $this->db = db_connect('default');
        $builder = $this->db->table('card_levels');
        $builder->orderBy('points');
        $levels = $builder->get()->getResult();

        $builder = $this->db->table('card_levels');
        $builder->selectMax('points');
        $maxPoints = $builder->get()->getRow()->points;

        $porcentaje = $levelPoints['points'] * 100 / $maxPoints;
        $porcentaje = $porcentaje > 100 ? 100 : $porcentaje;

        // 14.29% representa 1000 puntos = 50% de progreso
        // un porcentaje superior representa 50% + calculo_regla_tres_al_50%
        if ($porcentaje > 0 && $porcentaje <= 14.29) {
            $porcentaje = 50 * $porcentaje / 14.29;
        } else if ($porcentaje > 14.29 && $porcentaje < 100) {
            $porcentaje = 50 + (50 * $porcentaje / 100);
        }

        // TODO: Colocar regla en caso de que el nivel sea asignado por privilegio y no por puntos

        $legend = '';
        $flagLevel = false;

        foreach ($levels as $level) {
            if (!$flagLevel) {
                if ($level->points > $levelPoints['points']) {
                    $legend = 'Te faltan <strong>' . ($level->points - $levelPoints['points']) . '</strong> puntos para ser <strong>Socio Nivel ' . $level->name . '</strong>';
                    $flagLevel = true;
                }
                if ($levelPoints['points'] >= $maxPoints ) {
                    //$legend = 'Disfruta de todos los beneficios por ser Socio ' . $level->name .'.';
                    $legend = '100%';
                    $flagLevel = true;
                }
            }
        }

        //dd($benefits);
        return view('pages/entrega-de-beneficios', compact('client', 'points', 'benefits', 'benefitsAll', 'cardInfo', 'levels', 'legend', 'porcentaje'));
    }
    public function estadoDeCuenta($id)
    {

        $this->updateClientLevel($id);


        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $benefitsModel = model('App\Models\Services\RedemptionsBenefitsModel');
        $certificatesModel =model('App\Models\Services\RedemptionsCertificatesModel');
        $expirePointsModel =model('App\Models\Services\ExpirePointsLogModel');
        

        
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];
        $benefits = $benefitsModel->getRedemptionsBenefitsByClient($id);
        $certificates = $certificatesModel->getRedemptionsCertificatesByClient($id);
        $levelPoints = $pointsModel->getLevelPoints($id, 18)[0];
        $expirePoints = $expirePointsModel->getExpirePoints($id);




        $contactInfo = $clientModel->getContactClientInfoByID($id)[0];

        $this->db = db_connect('default');

        // TODO: Servicio para consultar el historial de transacciones correspondientes a un socio en particular
        $builder = $this->db->table('transactions');
        $builder->select('transactions.points as points, transactions.redeemed redeemed, transactions.created_at as date, receipts.amount as amount, receipts.number as ticket, cards.number as card_number, stores.name as store');
        $builder->join('receipts', 'transactions.receipt_id = receipts.id');
        $builder->join('cards', 'transactions.card_id = cards.id');
        $builder->join('stores', 'receipts.store_id = stores.id');
        $builder->where('transactions.client_id', $client['client_id']);
        $builder->where('transactions.enabled', 1);
        $builder->where('transactions.is_canceled', 0);
        $builder->orderBy('transactions.id', 'desc');
        $transactions = $builder->get()->getResult();

        // TODO: Servicio para consultar el historial de tarjetas asignadas a un socio en particular
        $builder = $this->db->table('cards');
        $builder->select('cards.number as card_number, cards.assigned_at as date, card_levels.name as level, card_status.name as status, card_types.name as type, card_reasons.name as reason');
        $builder->join('card_levels', 'cards.card_level_id = card_levels.id');
        $builder->join('card_status', 'cards.card_status_id = card_status.id');
        $builder->join('card_types', 'cards.card_type_id = card_types.id');
        $builder->join('card_reasons', 'cards.card_reason_id = card_reasons.id');
        $builder->where('cards.client_id', $client['client_id']);
        $builder->orderBy('cards.id', 'desc');
        $cards = $builder->get()->getResult();

        // TODO: Servicio para consultar los niveles de tarjeta y los puntos requeridos
        $builder = $this->db->table('card_levels');
        $builder->orderBy('points');
        $levels = $builder->get()->getResult();

        $builder = $this->db->table('card_levels');
        $builder->selectMax('points');
        $maxPoints = $builder->get()->getRow()->points;

        $porcentaje = $levelPoints['points'] * 100 / $maxPoints;
        $porcentaje = $porcentaje > 100 ? 100 : $porcentaje;


        if ($porcentaje > 0 && $porcentaje <= 14.29) {
            $porcentaje = 50 * $porcentaje / 14.29;
        } else if ($porcentaje > 14.29 && $porcentaje < 100) {
            $porcentaje = 50 + (50 * $porcentaje / 100);
        }

        // TODO: Colocar regla en caso de que el nivel sea asignado por privilegio y no por puntos


        $legend = '';
        $flagLevel = false;


        foreach ($levels as $level) {
            if (!$flagLevel) {
                if ($level->points > $levelPoints['points']) {
                    $legend = 'Te faltan <strong>' . ($level->points - $levelPoints['points']) . '</strong> puntos para ser <strong>Socio Nivel ' . $level->name . '</strong>';
                    $flagLevel = true;
                }
                if ($levelPoints['points'] >= $maxPoints ) {
                    // $legend = 'Disfruta de todos los beneficios por ser Socio ' . $level->name .'.';
                    $legend = '100%';
                    $flagLevel = true;
                }
            }
        }

        return view('pages/estado-de-cuenta', compact('client', 'levels', 'porcentaje', 'legend', 'transactions', 'cards', 'points', 'benefits', 'certificates', 'expirePoints'));
    }

    public function cambiarTarjeta($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        // TODO: Servicio para consultar los tipos de razones para cambio de tarjeta
        $this->db = db_connect('default');
        $builder = $this->db->table('card_reasons');
        $reasons = $builder->where('enabled', 1)->where('name !=', 'Uso actual')->get()->getResult();

        return view('pages/cambiar-tarjeta', compact('client', 'reasons', 'points'));
    }

    public function editarSocio($id)
    {
        $this->updateClientLevel($id);
        // TODO: Servico para consultar la información general del socio
        $this->db = db_connect('default');
        $clientInfo = $this->db->table('clients as c')
                            ->select('c.id, c.first_name, c.last_name, c.email, c.newsletter, i.email_alternative, i.telephone, i.mobile, i.gender, i.birthday, i.street, i.exterior, i.interior, i.codigo_postal, i.estado, i.municipio, i.colonia ')
                            ->join('client_info as i', 'c.id = i.client_id')
                            ->where('c.id', $id)
                            ->get()->getRow();

        $clientModel = model('App\Models\Services\ClientModel');
        $states = $clientModel->getStates();
        $municipios = $clientModel->searchMunicipalities($clientInfo->estado);
        $colonias = $clientModel->searchSuburbs($clientInfo->municipio);


        $pointsModel = model('App\Models\Services\PointsModel');
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        return view('pages/editar-socio', compact('states', 'clientInfo', 'municipios', 'colonias', 'points', 'client'));
    }

    public function reimprimirTickets($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');

        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        // TODO: Servico para recuperar tickets por concepto de redenciones
        $this->db = db_connect('default');

        // Entrega de certificados
        $tickets_redemptions_certificates = $this->db->table('tickets_redemptions_by_certificates as t')
            ->select('t.id, t.created_at, t.points_redeemed, c.number as card_number, CONCAT_WS(" ", u.first_name, u.last_name) as concierge, CONCAT_WS(" ", s.first_name, s.last_name) as client')
            ->join('users as u', 'u.id = t.user_id')
            ->join('cards as c', 'c.id = t.card_id')
            ->join('clients as s', 's.id = t.client_id')
            ->where('t.client_id', $id)
            ->where('t.enabled', 1)
            ->orderBy('t.id', 'DESC')
            ->get()->getResult();

        // Entrega de beneficios
        $tickets_redemptions_benefits = $this->db->table('tickets_redemptions_by_benefits as t')
            ->select('t.id, t.created_at, c.number as card_number, CONCAT_WS(" ", u.first_name, u.last_name) as concierge , CONCAT_WS(" ", s.first_name, s.last_name) as client')
            ->join('users as u', 'u.id = t.user_id')
            ->join('cards as c', 'c.id = t.card_id')
            ->join('clients as s', 's.id = t.client_id')
            ->where('t.client_id', $id)
            ->where('t.enabled', 1)
            ->orderBy('t.id', 'DESC')
            ->get()->getResult();

        // Acreditación de puntos
        $tickets_redemptions_transactions = $this->db->table('tickets_transactions as t')
            ->select('t.id, t.created_at, t.level, t.credited_points, t.points_at_that_time, c.number as card_number, CONCAT_WS(" ", u.first_name, u.last_name) as concierge, CONCAT_WS(" ", s.first_name, s.last_name) as client')
            ->join('users as u', 'u.id = t.user_id')
            ->join('cards as c', 'c.id = t.card_id')
            ->join('clients as s', 's.id = t.client_id')
            ->where('t.client_id', $id)
            ->where('t.enabled', 1)
            ->orderBy('t.id', 'DESC')
            ->get()->getResult();
        //dd($tickets_redemptions_certificates);

        return view('pages/reimprimir-tickets', compact('client', 'points', 'tickets_redemptions_certificates', 'tickets_redemptions_benefits', 'tickets_redemptions_transactions'));
    }

    public function cancelaciones($id)
    {
        $this->updateClientLevel($id);
        $clientModel = model('App\Models\Services\ClientModel');
        $pointsModel = model('App\Models\Services\PointsModel');

        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        $this->db = db_connect('default');
        $receipts =  $this->db->table('receipts')
            ->select('receipts.id as receipt_id, receipts.date, receipts.amount, stores.name as store, transactions.points, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.id as client_id')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->where('transactions.client_id', $id)
            ->where('transactions.enabled', 1)
            ->where('transactions.is_canceled', 0)
            ->where('transactions.redeemed', 0)
            ->where('receipts.enabled', 1)
            ->orderBy('transactions.id', 'DESC')
            ->get()->getResult();

        $redemptions =  $this->db->table('redemptions_by_certificates as redemptions')
            ->select('redemptions.id as redemption_id, redemptions.created_at as date, redemptions.amount, certificates.title as certificate, (certificates.points * redemptions.amount) as points, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.id as client_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('certificates', 'certificates.id = redemptions.certificate_id')
            ->where('redemptions.client_id', $id)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.id', 'DESC')
            ->get()->getResult();

        $benefits =  $this->db->table('redemptions_by_benefits as redemptions')
            ->select('redemptions.id as redemption_id, redemptions.created_at as date, redemptions.amount, benefits.title as benefit, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.id as client_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('benefits', 'benefits.id = redemptions.benefit_id')
            ->where('redemptions.client_id', $id)
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.id', 'DESC')
            ->get()->getResult();

        $tickets_redemptions_transactions = [];
        return view('pages/cancelaciones', compact('points', 'client', 'receipts',  'redemptions', 'benefits'));
    }

    public function registrarConcierge()
    {
        $clientModel = model('App\Models\Services\ClientModel');
        $states = $clientModel->getStates();

        return view('pages/registrar-concierge', compact('states'));
    }
}
