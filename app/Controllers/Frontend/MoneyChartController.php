<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\MoneyChartModel;

class MoneyChartController extends BaseController
{
	public function index($year)
	{
        $moneyChartModel = new MoneyChartModel($year);

        $dinero_total_en_tickets = $moneyChartModel->dineroIngresadoEnTickets();
        $dinero_total_en_redenciones = $moneyChartModel->dineroEntregadoEnRedenciones();

        $dinero_tickets_genero = $moneyChartModel->dineroEnTicketsPorGenero();
        $dinero_redenciones_genero = $moneyChartModel->dineroEnRedencionesPorGenero();
        $dinero_tickets_nivel = $moneyChartModel->dineroEnTicketsPorNivel();
        $dinero_redenciones_nivel = $moneyChartModel->dineroEnRedencionesPorNivel();
        $dinero_tickets_mes = $moneyChartModel->dineroEnTicketsPorMes();
        $dinero_redenciones_mes = $moneyChartModel->dineroEnRedencionesPorMes();
        $dinero_tickets_edad = $moneyChartModel->dineroEnTicketsPorRangoDeEdad();

        $porcentaje_dinero_tickets_genero = json_encode($dinero_tickets_genero['porcentajes']);
        $porcentaje_dinero_redenciones_genero = json_encode($dinero_redenciones_genero['porcentajes']);
        $porcentaje_dinero_tickets_nivel = json_encode($dinero_tickets_nivel['porcentajes']);
        $data_dinero_tickets_mes = json_encode($dinero_tickets_mes['data']);
        $data_dinero_redenciones_mes = json_encode($dinero_redenciones_mes['data']);
        $porcentaje_dinero_redenciones_nivel = json_encode($dinero_redenciones_nivel['porcentajes']);
        $porcentaje_dinero_tickets_edad = json_encode($dinero_tickets_edad['porcentajes']);

        $labels_dinero_tickets_genero = json_encode($dinero_tickets_genero['labels']);
        $labels_dinero_tickets_nivel = json_encode($dinero_tickets_nivel['labels']);
        $labels_dinero_redenciones_genero = json_encode($dinero_redenciones_genero['labels']);
        $labels_dinero_redenciones_nivel = json_encode($dinero_redenciones_nivel['labels']);


        return view('charts/money', compact('dinero_total_en_tickets', 'dinero_total_en_redenciones', 'dinero_tickets_genero', 'dinero_redenciones_genero', 'dinero_tickets_nivel', 'dinero_redenciones_nivel', 'dinero_tickets_mes', 'dinero_redenciones_mes', 'dinero_tickets_edad', 'porcentaje_dinero_tickets_genero', 'porcentaje_dinero_redenciones_genero', 'porcentaje_dinero_tickets_nivel', 'data_dinero_tickets_mes', 'data_dinero_redenciones_mes', 'porcentaje_dinero_redenciones_nivel', 'porcentaje_dinero_tickets_edad', 'labels_dinero_tickets_genero', 'labels_dinero_tickets_nivel', 'labels_dinero_redenciones_genero', 'labels_dinero_redenciones_nivel', 'year'));
	}
}
