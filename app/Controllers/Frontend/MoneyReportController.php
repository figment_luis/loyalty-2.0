<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\MoneyReportModel;

class MoneyReportController extends BaseController
{
    public function dineroTickets($fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroTickets($fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['TIENDA', 'MONTO DE COMPRA', 'FECHA DE COMPRA', 'NOMBRE DEL CLIENTE', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD AL MOMENTO DE LA COMPRA', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO', 'CONCIERGE']);

        foreach ($dinero as $ticket) {
            $val = [mb_strtoupper($ticket->store), $ticket->amount, $ticket->date, mb_strtoupper($ticket->client), mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register, mb_strtoupper($ticket->concierge)];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroRedenciones($fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroRedenciones($fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Certificado', 'Tienda partipante', 'Puntos', 'Precio', 'Unidades canjeadas', 'Monto total', 'Fecha de redención', 'Nombre(s)', 'Apellidos', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro']);

        foreach ($dinero as $redemption) {
            $val = [$redemption->title, $redemption->subtitle, $redemption->points, $redemption->price, $redemption->amount, $redemption->total, $redemption->date, $redemption->first_name, $redemption->last_name, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroTicketsPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-por-genero');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroTicketsPorGenero($genderName, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($dinero as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroTicketsPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-por-nivel');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroTicketsPorNivel($levelName, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($dinero as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroTicketsPorRangoDeEdad($rangoEdad, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-por-rango-de-edad');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroTicketsPorRangoDeEdad($rangoEdad, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($dinero as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroRedencionesPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones-por-genero');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroRedencionesPorGenero($genderName, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($dinero as $redemption) {
            $val = [mb_strtoupper($redemption->client), $redemption->amount, mb_strtolower($redemption->email), $redemption->birthday, $redemption->gender, $redemption->age, mb_strtoupper($redemption->state), mb_strtoupper($redemption->municipality), $redemption->card_number, mb_strtoupper($redemption->card_type), mb_strtoupper($redemption->level), $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function dineroRedencionesPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones-por-nivel');

        $moneyReportModel = new MoneyReportModel();
        $dinero = $moneyReportModel->dineroRedencionesPorNivel($levelName, $fechaInicio, $fechaFin);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($dinero as $redemption) {
            $val = [mb_strtoupper($redemption->client), $redemption->amount, mb_strtolower($redemption->email), $redemption->birthday, $redemption->gender, $redemption->age, mb_strtoupper($redemption->state), mb_strtoupper($redemption->municipality), $redemption->card_number, mb_strtoupper($redemption->card_type), mb_strtoupper($redemption->level), $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    private function setHeader($fileName)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '-' . date('Y-m-d') . '.csv');
    }

    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }
}
