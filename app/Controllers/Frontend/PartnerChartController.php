<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\PartnerChartModel;

class PartnerChartController extends BaseController
{
	public function index($year)
	{
        $partnerChartModel = new PartnerChartModel($year);

        $socios_totales = $partnerChartModel->clientesTotales();
        $tiendas = $partnerChartModel->listarTiendas();

        $socios_genero = $partnerChartModel->sociosRegistradosPorGenero();
        $socios_nivel = $partnerChartModel->sociosRegistradosPorNivel();
        $socios_edad = $partnerChartModel->sociosRegistradosPorRangoDeEdad();
        $socios_region = $partnerChartModel->sociosRegistradosPorRegion();
        $socios_mes = $partnerChartModel->sociosRegistradosPorMes();

        $porcentaje_socios_genero = json_encode($socios_genero['porcentajes']);
        $porcentaje_socios_nivel = json_encode($socios_nivel['porcentajes']);
        $porcentaje_socios_edad = json_encode($socios_edad['porcentajes']);
        $data_socios_region = json_encode($socios_region['data']);
        $data_socios_mes = json_encode($socios_mes['data']);

        $labels_socios_genero = json_encode($socios_genero['labels']);
        $labels_socios_nivel = json_encode($socios_nivel['labels']);
        $labels_socios_edad = json_encode($socios_edad['labels']);

        return view('charts/partner', compact('socios_totales', 'tiendas', 'socios_genero', 'porcentaje_socios_genero', 'socios_nivel', 'porcentaje_socios_nivel', 'socios_edad', 'porcentaje_socios_edad', 'socios_region', 'data_socios_region', 'socios_mes', 'data_socios_mes', 'labels_socios_genero', 'labels_socios_nivel', 'labels_socios_edad', 'year'));
	}
}
