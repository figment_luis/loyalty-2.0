<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Services\PointsModel;

class PointReportController extends BaseController
{
	public function puntos()
	{
        $this->setHeader('reporte-puntos');

        $pointsModel = new PointsModel();
        $points = $pointsModel->getReportPoints();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Nombre del cliente', 'Tarjeta', 'Nivel', 'Puntos disponibles', 'Puntos redimidos']);

        foreach ($points as $point) {
            $val = [$point['Nombre'], $point['Tarjeta'], $point['Nivel'], $point['Puntos Disponibles'], $point['Puntos Redimidos']];
            fputcsv($fp, $val);
        }
        fclose($fp);
	}

    private function setHeader($fileName)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '-' . date('Y-m-d') . '.csv');
    }

    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }
}
