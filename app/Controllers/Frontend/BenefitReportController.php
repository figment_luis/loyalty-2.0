<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\BenefitReportModel;

class BenefitReportController extends BaseController
{
    public function certificadosEntregados($id)
    {
        $this->setHeader('certificados-entregados');

        $benefitReportModel = new BenefitReportModel();
        $certificates = $benefitReportModel->certificadosEntregados($id);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Certificado', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($certificates as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function beneficiosEntregados($id)
    {
        $this->setHeader('beneficios-entregados');

        $benefitReportModel = new BenefitReportModel();
        $benefits = $benefitReportModel->beneficiosEntregados($id);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Beneficio', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($benefits as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function certificadosEntregadosPorPeriodo($year)
    {
        $this->setHeader('certificados-entregados-periodo-' . $year);

        $benefitReportModel = new BenefitReportModel();
        $certificates = $benefitReportModel->certificadosEntregadosPorPeriodo($year);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Certificado', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($certificates as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function beneficiosEntregadosPorPeriodo($year)
    {
        $this->setHeader('beneficios-entregados-periodo-' . $year);

        $benefitReportModel = new BenefitReportModel();
        $benefits = $benefitReportModel->beneficiosEntregadosPorPeriodo($year);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Beneficio', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($benefits as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

	/*public function beneficiosEntregados()
	{
        $this->setHeader('beneficios-entregados');

        $benefitReportModel = new BenefitReportModel();
        $benefits = $benefitReportModel->beneficiosEntregados();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Beneficio', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($benefits as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
	}*/

    private function setHeader($fileName)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '-' . date('Y-m-d') . '.csv');
    }

    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }
}
