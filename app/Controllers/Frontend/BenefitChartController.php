<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\BenefitChartModel;

class BenefitChartController extends BaseController
{
	public function index($year)
	{
	    $benefitChartModel = new BenefitChartModel($year);

	    $certificados_redimidos_por_mes = $benefitChartModel->certificadosRedimidosPorMes();
        $beneficios_redimidos_por_mes = $benefitChartModel->beneficiosRedimidosPorMes();
	    $certificados = $benefitChartModel->certificados();
	    $beneficios = $benefitChartModel->beneficios();

	    $data_certificados_redimidos_por_mes = json_encode($certificados_redimidos_por_mes['data']);
        $data_beneficios_redimidos_por_mes = json_encode($beneficios_redimidos_por_mes['data']);

	    //dd($beneficios_redimidos_por_mes);

		return view('charts/benefit', compact('year', 'certificados', 'beneficios', 'certificados_redimidos_por_mes', 'data_certificados_redimidos_por_mes', 'beneficios_redimidos_por_mes', 'data_beneficios_redimidos_por_mes'));
	}
}
