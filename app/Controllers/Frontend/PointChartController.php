<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Frontend\PointChartModel;

class PointChartController extends BaseController
{
	public function index($year)
	{
	    $pointChartModel = new PointChartModel($year);

	    $puntos_disponibles = $pointChartModel->puntosDisponibles();
	    $puntos_redimidos = $pointChartModel->puntosRedimidos();
	    $puntos_expirados = $pointChartModel->puntosExpirados();

	    $concentrado_puntos_disponibles = $pointChartModel->concentradoPuntosDisponibles();
        $concentrado_puntos_redimidos = $pointChartModel->concentradoPuntosRedimidos();

		return view('charts/point', compact('year', 'puntos_disponibles', 'puntos_redimidos', 'puntos_expirados', 'concentrado_puntos_disponibles', 'concentrado_puntos_redimidos'));
	}
}
