<?php
namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Services\PointsModel;
use App\Models\Services\PointssModel;

class ReportController extends BaseController
{

    private $db = null;

    public function __construct()
    {
        helper('custom');
        $this->db = db_connect('default');
    }

    public function socios()
    {
        $this->db = db_connect('default');
        $builder = $this->db->table('stores');
        $tiendas = $builder->where('enabled', 1)->orderBy('name')->get()->getResult();

        $builder = $this->db->table('clients as c');
        $clientes_totales = $builder->where('enabled', 1)->where('available', 1)->countAll();
        $clientes_por_genero = $builder->select('i.gender, COUNT(*) as total')->groupBy('i.gender')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->orderBy('i.gender')
            ->get()->getResult();
        $generos = [];
        foreach ($clientes_por_genero as $g) {
            array_push($generos, round(($g->total * 100 / $clientes_totales)));
        }

        $generos_table = $this->sociosPorGeneroTable();

        $clientes_por_nivel = $builder->select('l.name, COUNT(*) as total')->groupBy('l.name')
            ->join('cards', 'c.id = cards.client_id')
            ->join('card_levels as l', 'l.id = cards.card_level_id')
            ->join('card_status as s', 's.id = cards.card_status_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('cards.enabled', 1)
            ->where('s.name', 'Habilitada')
            ->get()->getResult();

        $niveles = [];
        foreach ($clientes_por_nivel as $g) {
            array_push($niveles, round(($g->total * 100 / $clientes_totales)));
        }

        $nivel_table = $this->sociosPorNivelTable();

        $clientes_por_edad = $builder->select('
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 18 AND 29, 1, 0)) as "18-29",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 30 AND 39, 1, 0)) as "30-39",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 40 AND 49, 1, 0)) as "40-49",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 50 AND 59, 1, 0)) as "50-59",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 60 AND 69, 1, 0)) as "60-69",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 70 AND 79, 1, 0)) as "70-79",
        ')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();

        $edades = [];
        foreach ($clientes_por_edad[0] as $clave => $valor) {
            array_push($edades, round(($valor * 100 / $clientes_totales)));
        }

        $rango_edad_table = $this->sociosPorRangoEdadTable();

        $clientes_por_regiones = $builder->select('states.name, COUNT(*) as total')->groupBy('states.name')
            ->join('client_info as i', 'c.id = i.client_id')
            ->join('states', 'states.id = i.estado')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();
        $data_regiones = [];
        $regiones_name = [];
        $regiones_total = [];
        foreach ($clientes_por_regiones as $region) {
            array_push($regiones_name, $region->name);
            array_push($regiones_total, $region->total);
        }
        $data_regiones['data'] = $regiones_total;
        $data_regiones['labels'] = $regiones_name;

        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $clientes_por_periodo = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('mes')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('YEAR(c.created_at)', date('Y'))
            ->get()->getResult();

        $periodos = [];
        $chart_periodo = [];
        $periodo_data = [];
        $periodo_labels = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($clientes_por_periodo as $periodo) {
                if ($periodo->mes == $index + 1) {
                    array_push($periodos, ['mes' => $mes, 'total' => $periodo->total]);
                    array_push($periodo_data, $periodo->total);
                    array_push($periodo_labels, $mes);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($periodos, ['mes' => $mes, 'total' => 0]);
                array_push($periodo_data, 0);
                array_push($periodo_labels, $mes);
            }
        }
        $chart_periodo['data'] = $periodo_data;
        $chart_periodo['labels'] = $periodo_labels;

        //dd($clientes_totales, $clientes_por_genero, $clientes_por_nivel, $clientes_por_edad, $clientes_por_regiones, $clientes_por_periodo, $periodos);
        return view('reports/socios', compact('clientes_totales', 'clientes_por_genero', 'clientes_por_nivel', 'clientes_por_edad', 'clientes_por_regiones', 'periodos', 'clientes_por_periodo', 'generos', 'niveles', 'edades', 'data_regiones', 'chart_periodo', 'tiendas', 'nivel_table', 'generos_table', 'rango_edad_table'));
    }

    public function sociosRegistrados($fechaInicio, $fechaFin)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-' . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, type.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, IF(info.gender = "M", "Masculino", "Femenino") as gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as estado, municipality.name as municipio')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('card_types as type', 'type.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('clients.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono Fijo', 'Teléfono Móvil', 'Fecha de Nacimiento', 'Edad', 'Género', 'Estado', 'Municipio o Delegación', 'Número de Tarjeta', 'Tipo de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->birthday, $line->age, $line->gender, $line->estado, $line->municipio, $line->card_number, $line->card_type, $line->level, $line->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorGenero($genero, $fechaInicio, $fechaFin)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-genero-' .$genero. '-'  . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('info.gender', $genero)
            ->where('clients.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorNivel($nivel, $fechaInicio, $fechaFin)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-nivel-' .$nivel. '-'  . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('level.name', $nivel)
            ->where('clients.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorRangoEdad($rango, $fechaInicio, $fechaFin)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-rango-edad-' .$rango. '-'  . date('Y-m-d') . '.csv');

        list($edadMin, $edadMax) = explode('-', $rango);
        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, info.birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, clients.created_at')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) BETWEEN ' . $edadMin . ' AND ' . $edadMax)
            ->where('clients.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        //dd($registros);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Nacimiento', 'Edad', 'Fecha de Registro']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->birthday, $line->age, $line->created_at];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosComprasPorTienda($tienda, $fechaInicio, $fechaFin)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-compras-tienda-' .$tienda. '-'  . date('Y-m-d') . '.csv');


        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, stores.name as store, SUM(receipts.amount) as amount, receipts.date')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('transactions', 'clients.id = transactions.client_id')
            ->join('receipts', 'receipts.id = transactions.receipt_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->where('receipts.store_id', $tienda)
            ->where('receipts.date BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->groupBy('clients.email')
            ->get()->getResult();

        //dd($registros);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Tienda', 'Monto de Compra', 'Fecha de Compra']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->store, $line->amount, $line->date];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }


    /**************************************************************
     *
     *  Reporte - Dineros
     *
     *
     *************************************************************/

    public function reporteGeneralDineroTickets($fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets');

        $builder = $this->db->table('receipts');
        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, stores.name as store, receipts.amount, receipts.date, cards.number as card_number, level.name as level, type.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, receipts.date) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as level', 'level.id = card_level_id')
            ->join('card_types as type', 'type.id = card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->join('receipt_user_logs as logs', 'logs.receipt_id = receipts.id')
            ->join('users', 'users.id = logs.user_id')
            ->where('receipts.date BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->orderBy('receipts.date')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['TIENDA', 'MONTO DE COMPRA', 'FECHA DE COMPRA', 'NOMBRE DEL CLIENTE', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD AL MOMENTO DE LA COMPRA', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO', 'CONCIERGE']);

        foreach ($tickets as $ticket) {
            $val = [mb_strtoupper($ticket->store), $ticket->amount, $ticket->date, mb_strtoupper($ticket->client), mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register, mb_strtoupper($ticket->concierge)];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteDineroTicketsPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-genero-' . $genderName);

        $builder = $this->db->table('receipts');
        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('receipts.date BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->where('info.gender', $genderName)
            ->orderBy('receipts.date')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($tickets as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteDineroTicketsPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-nivel-' . $levelName);

        $builder = $this->db->table('receipts');
        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('receipts.date BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->where('levels.name', $levelName)
            ->orderBy('receipts.date')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($tickets as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteDineroTicketsPorRangoDeEdad($rangoEdad, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-tickets-rango-edad-' . $rangoEdad);

        list($edadMin, $edadMax) = explode(' a ', $rangoEdad);
        $builder = $this->db->table('receipts');
        $tickets = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, SUM(receipts.amount) as amount, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) BETWEEN ' . $edadMin . ' AND ' . $edadMax)
            ->where('receipts.date BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('transactions.enabled', 1)
            ->where('receipts.enabled', 1)
            ->orderBy('receipts.date')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($tickets as $ticket) {
            $val = [mb_strtoupper($ticket->client), $ticket->amount, mb_strtolower($ticket->email), $ticket->birthday, $ticket->gender, $ticket->age, mb_strtoupper($ticket->state), mb_strtoupper($ticket->municipality), $ticket->card_number, mb_strtoupper($ticket->card_type), mb_strtoupper($ticket->level), $ticket->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteGeneralDineroRedenciones($fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones');

        $builder = $this->db->table('certificates');
        $redemptions = $builder->select('certificates.title, certificates.subtitle, certificates.points, certificates.price, redemptions.amount, (redemptions.amount * certificates.price) as total, redemptions.created_at as date, clients.first_name, clients.last_name, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('redemptions.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Certificado', 'Tienda partipante', 'Puntos', 'Precio', 'Unidades canjeadas', 'Monto total', 'Fecha de redención', 'Nombre(s)', 'Apellidos', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro']);

        foreach ($redemptions as $redemption) {
            $val = [$redemption->title, $redemption->subtitle, $redemption->points, $redemption->price, $redemption->amount, $redemption->total, $redemption->date, $redemption->first_name, $redemption->last_name, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteDineroRedencionesPorGenero($genderName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones-genero-' . $genderName);

        $builder = $this->db->table('certificates');
        $redemptions = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, SUM(redemptions.amount * certificates.price) as amount, clients.email, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, info.gender, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as state, municipality.name as municipality, cards.number as card_number, levels.name as level, types.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('redemptions.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->where('info.gender', $genderName)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($redemptions as $redemption) {
            $val = [mb_strtoupper($redemption->client), $redemption->amount, mb_strtolower($redemption->email), $redemption->birthday, $redemption->gender, $redemption->age, mb_strtoupper($redemption->state), mb_strtoupper($redemption->municipality), $redemption->card_number, mb_strtoupper($redemption->card_type), mb_strtoupper($redemption->level), $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteDineroRedencionesPorNivel($levelName, $fechaInicio, $fechaFin)
    {
        $this->setHeader('dinero-redenciones-nivel-' . $levelName);

        $builder = $this->db->table('certificates');
        $redemptions = $builder->select('CONCAT_WS(" ", clients.first_name, clients.last_name) as client, SUM(redemptions.amount * certificates.price) as amount, clients.email, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, info.gender, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as state, municipality.name as municipality, cards.number as card_number, levels.name as level, types.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register')->groupBy('clients.email')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('redemptions.created_at BETWEEN "' . $fechaInicio . '" AND "' . $fechaFin .'"')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->where('levels.name', $levelName)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['NOMBRE DEL CLIENTE', 'MONTO TOTAL', 'EMAIL', 'FECHA DE NACIMIENTO', 'GÉNERO', 'EDAD', 'ESTADO', 'MUNICIPIO O DELEGACIÓN', 'NÚMERO DE TARJETA', 'TIPO DE TARJETA', 'NIVEL', 'FECHA DE REGISTRO']);

        foreach ($redemptions as $redemption) {
            $val = [mb_strtoupper($redemption->client), $redemption->amount, mb_strtolower($redemption->email), $redemption->birthday, $redemption->gender, $redemption->age, mb_strtoupper($redemption->state), mb_strtoupper($redemption->municipality), $redemption->card_number, mb_strtoupper($redemption->card_type), mb_strtoupper($redemption->level), $redemption->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteGeneralBeneficiosEntregados()
    {
        $this->setHeader('beneficios-entregados');

        $builder = $this->db->table('benefits');
        $redemptions = $builder->select('benefits.title, benefits.subtitle as brand, benefits.description, redemptions.amount, redemptions.created_at as date, CONCAT_WS(" ", clients.first_name, clients.last_name) as client, clients.email, cards.number as card_number, levels.name as level, types.name as card_type, info.gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, states.name as state, municipality.name as municipality, TIMESTAMPDIFF(YEAR, info.birthday, redemptions.created_at) as age, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, CONCAT_WS(" ", users.first_name, users.last_name) as concierge')
            ->join('redemptions_by_benefits as redemptions', 'benefits.id = redemptions.benefit_id')
            ->join('redemption_by_benefit_user_logs as logs', 'redemptions.id = logs.redemption_by_benefit_id')
            ->join('users', 'users.id = logs.user_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as levels', 'levels.id = cards.card_level_id')
            ->join('card_types as types', 'types.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('redemptions.enabled', 1)
            ->where('redemptions.is_canceled', 0)
            ->orderBy('redemptions.created_at')
            ->get()->getResult();

        //dd($redemptions);

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Beneficio', 'Tienda partipante', 'Descripción', 'Unidades canjeadas', 'Fecha de redención', 'Nombre del socio', 'Email', 'Fecha de nacimiento', 'Género', 'Edad al momento de la redención', 'Estado', 'Municipio o Delegación', 'Número de tarjeta', 'Tipo de tarjeta', 'Nivel', 'Fecha de registro', 'Concierge']);

        foreach ($redemptions as $redemption) {
            $val = [$redemption->title, $redemption->brand, $redemption->description, $redemption->amount, $redemption->date, $redemption->client, $redemption->email, $redemption->birthday, $redemption->gender, $redemption->age, $redemption->state, $redemption->municipality, $redemption->card_number, $redemption->card_type, $redemption->level, $redemption->register, $redemption->concierge];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function reporteGeneralPuntos()
    {
        $pointsModel = new PointsModel();
        $points = $pointsModel->getReportPoints();

        $this->setHeader('reporte-puntos');

        $fp = fopen('php://output', 'wb');
        fputs( $fp, "\xEF\xBB\xBF" );

        $this->setCabecerasArchivo($fp, ['Nombre del cliente', 'Tarjeta', 'Nivel', 'Puntos disponibles', 'Puntos redimidos', 'Puntos expirados']);

        foreach ($points as $point) {
            $val = [$point['Nombre'], $point['Tarjeta'], $point['Nivel'], $point['Puntos Disponibles'], $point['Puntos Redimidos'], $point['Puntos Expirados']];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    private function setHeader($fileName)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '-' . date('Y-m-d') . '.csv');
    }
    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }


    private function sociosPorNivelTable()
    {
        $builder = $this->db->table('clients as c');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $periodo = [];
        foreach ($niveles as $nivel):
            $clientes_por_nivel = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('MONTH(c.created_at)')
                ->join('cards', 'c.id = cards.client_id')
                ->join('card_levels as l', 'l.id = cards.card_level_id')
                ->join('card_status as s', 's.id = cards.card_status_id')
                ->where('l.name', $nivel)
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->where('cards.enabled', 1)
                ->where('s.name', 'Habilitada')
                ->get()->getResult();

            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($clientes_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($periodo, $data);
        endforeach;
        $niveles = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $periodo
        ];
        return $niveles;
    }

    private function sociosPorGeneroTable()
    {
        $builder = $this->db->table('clients as c');

        $generos = ['M', 'F'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($generos as $genero):
            $clientes_por_genero = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('MONTH(c.created_at)')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('i.gender', $genero)
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->orderBy('i.gender')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($clientes_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function sociosPorRangoEdadTable()
    {
        $builder = $this->db->table('clients as c');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $edades = ['18 AND 29','30 AND 39','40 AND 49','50 AND 59','60 AND 69','70 AND 79'];

        $clientes = [];
        foreach ($edades as $index => $edad):
            $clientes_por_edad = $builder->select('
                MONTH(c.created_at) as mes,
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "'. $edad.'",
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "total",
            ')->groupBy('MONTH(c.created_at)')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('c.enabled', 1)
                ->where('c.available', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes):
                $encontrado = false;
                foreach ($clientes_por_edad as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            endforeach;
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'edades' => ['De 18 a 29 años','De 30 a 39 años','De 40 a 49 años','De 50 a 59 años','De 60 a 69 años','De 70 a 79 años'],
            'backgrounds' => ['bg-primary', 'bg-danger', 'bg-secondary', 'bg-success', 'bg-warning', 'bg-dark'],
            'periodo' => $clientes
        ];
        return $informacion;
    }
}