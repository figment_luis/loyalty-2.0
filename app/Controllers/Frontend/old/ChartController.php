<?php
namespace App\Controllers\Frontend;

use App\Controllers\BaseController;

class ChartController extends BaseController
{

    private $db = null;

    public function __construct()
    {
        helper('custom');
        $this->db = db_connect('default');
    }

    public function socios()
    {
        $this->db = db_connect('default');
        $builder = $this->db->table('stores');
        $tiendas = $builder->where('enabled', 1)->orderBy('name')->get()->getResult();

        $builder = $this->db->table('clients as c');
        $clientes_totales = $builder->where('enabled', 1)->where('available', 1)->countAll();
        $clientes_por_genero = $builder->select('i.gender, COUNT(*) as total')->groupBy('i.gender')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->orderBy('i.gender')
            ->get()->getResult();
        $generos = [];
        foreach ($clientes_por_genero as $g) {
            array_push($generos, round(($g->total * 100 / $clientes_totales)));
        }

        $clientes_por_nivel = $builder->select('l.name, COUNT(*) as total')->groupBy('l.name')
            ->join('cards', 'c.id = cards.client_id')
            ->join('card_levels as l', 'l.id = cards.card_level_id')
            ->join('card_status as s', 's.id = cards.card_status_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('cards.enabled', 1)
            ->where('s.name', 'Habilitada')
            ->get()->getResult();

        $niveles = [];
        foreach ($clientes_por_nivel as $g) {
            array_push($niveles, round(($g->total * 100 / $clientes_totales)));
        }

        $clientes_por_edad = $builder->select('
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 18 AND 29, 1, 0)) as "18-29",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 30 AND 39, 1, 0)) as "30-39",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 40 AND 49, 1, 0)) as "40-49",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 50 AND 59, 1, 0)) as "50-59",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 60 AND 69, 1, 0)) as "60-69",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 70 AND 79, 1, 0)) as "70-79",
        ')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();

        $edades = [];
        foreach ($clientes_por_edad[0] as $clave => $valor) {
            array_push($edades, round(($valor * 100 / $clientes_totales)));
        }

        $clientes_por_regiones = $builder->select('states.name, COUNT(*) as total')->groupBy('states.name')
            ->join('client_info as i', 'c.id = i.client_id')
            ->join('states', 'states.id = i.estado')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();
        $data_regiones = [];
        $regiones_name = [];
        $regiones_total = [];
        foreach ($clientes_por_regiones as $region) {
            array_push($regiones_name, $region->name);
            array_push($regiones_total, $region->total);
        }
        $data_regiones['data'] = $regiones_total;
        $data_regiones['labels'] = $regiones_name;

        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $clientes_por_periodo = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('mes')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('YEAR(c.created_at)', date('Y'))
            ->get()->getResult();

        $periodos = [];
        $chart_periodo = [];
        $periodo_data = [];
        $periodo_labels = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($clientes_por_periodo as $periodo) {
                if ($periodo->mes == $index + 1) {
                    array_push($periodos, ['mes' => $mes, 'total' => $periodo->total]);
                    array_push($periodo_data, $periodo->total);
                    array_push($periodo_labels, $mes);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($periodos, ['mes' => $mes, 'total' => 0]);
                array_push($periodo_data, 0);
                array_push($periodo_labels, $mes);
            }
        }
        $chart_periodo['data'] = $periodo_data;
        $chart_periodo['labels'] = $periodo_labels;

        //dd($clientes_totales, $clientes_por_genero, $clientes_por_nivel, $clientes_por_edad, $clientes_por_regiones, $clientes_por_periodo, $periodos);
        return view('reports/socios', compact('clientes_totales', 'clientes_por_genero', 'clientes_por_nivel', 'clientes_por_edad', 'clientes_por_regiones', 'periodos', 'clientes_por_periodo', 'generos', 'niveles', 'edades', 'data_regiones', 'chart_periodo', 'tiendas'));
    }

    public function sociosRegistrados()
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-' . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, type.name as card_type, DATE_FORMAT(clients.created_at, "%Y-%m-%d") as register, IF(info.gender = "M", "Masculino", "Femenino") as gender, DATE_FORMAT(info.birthday, "%Y-%m-%d") as birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age, states.name as estado, municipality.name as municipio')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('card_types as type', 'type.id = cards.card_type_id')
            ->join('states', 'states.id = info.estado')
            ->join('municipality', 'municipality.id = info.municipio')
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono Fijo', 'Teléfono Móvil', 'Fecha de Nacimiento', 'Edad', 'Género', 'Estado', 'Municipio o Delegación', 'Número de Tarjeta', 'Tipo de Tarjeta', 'Nivel', 'Fecha de Registro']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->birthday, $line->age, $line->gender, $line->estado, $line->municipio, $line->card_number, $line->card_type, $line->level, $line->register];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorGenero($genero)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-genero-' .$genero. '-'  . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('info.gender', $genero)
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorNivel($nivel)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-nivel-' .$nivel. '-'  . date('Y-m-d') . '.csv');

        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('level.name', $nivel)
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosPorRangoEdad($rango)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-por-rango-edad-' .$rango. '-'  . date('Y-m-d') . '.csv');

        list($edadMin, $edadMax) = explode('-', $rango);
        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, info.birthday, TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) as age')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->where('TIMESTAMPDIFF(YEAR, info.birthday, CURDATE()) BETWEEN ' . $edadMin . ' AND ' . $edadMax)
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->get()->getResult();

        //dd($registros);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Fecha de Nacimiento', 'Edad']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->birthday, $line->age];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function sociosComprasPorTienda($tienda)
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="reporte-socios-compras-tienda-' .$tienda. '-'  . date('Y-m-d') . '.csv');


        $this->db = db_connect('default');
        $builder = $this->db->table('clients');
        $registros = $builder->select('clients.first_name, clients.last_name, clients.email, info.telephone, info.mobile, cards.number as card_number, level.name as level, stores.name as store, SUM(receipts.amount) as amount')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->join('cards', 'clients.id = cards.client_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->join('card_status as status', 'status.id = cards.card_status_id')
            ->join('transactions', 'clients.id = transactions.client_id')
            ->join('receipts', 'receipts.id = transactions.receipt_id')
            ->join('stores', 'stores.id = receipts.store_id')
            ->where('receipts.store_id', $tienda)
            ->where('clients.enabled', 1)
            ->where('clients.available', 1)
            ->where('cards.enabled', 1)
            ->where('status.name', 'Habilitada')
            ->orderBy('level.id')
            ->groupBy('clients.email')
            ->get()->getResult();

        //dd($registros);

        $fp = fopen('php://output', 'wb');
        //This line is important:
        fputs( $fp, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

        $this->setCabecerasArchivo($fp, ['Nombre(s)', 'Apellidos', 'Email', 'Teléfono', 'Celular', 'Número de Tarjeta', 'Nivel', 'Tienda', 'Monto de Compra']);

        foreach ( $registros as $line ) {
            $val = [$line->first_name, $line->last_name, $line->email, $line->telephone, $line->mobile, $line->card_number, $line->level, $line->store, $line->amount];
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    private function setCabecerasArchivo(&$fp, $headers)
    {
        fputcsv($fp, $headers);
    }

    public function dineros()
    {
        $builder = $this->db->table('receipts');
        $dinero_en_tickets = $builder->selectSum('amount')->where('enabled', 1)->get()->getRow()->amount;

        $builder = $this->db->table('receipts');
        // TODO: Colocar el año de forma dinámica
        $dinero_en_tickets_anio_actual = $builder->selectSum('amount')->where('YEAR(date)', date('Y'))->where('enabled', 1)->get()->getRow()->amount;


        $builder = $this->db->table('certificates');
        $dinero_en_redenciones = $builder->select('SUM(certificates.price * redemptions.amount) AS total')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->where('redemptions.enabled', 1)
            ->get()->getRow()->total;

        $builder = $this->db->table('certificates');
        $dinero_en_redenciones_por_genero = $builder->select('SUM(certificates.price * redemptions.amount) AS total, info.gender')->groupBy('info.gender')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('clients', 'clients.id = redemptions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->where('redemptions.enabled', 1)
            ->orderBy('info.gender')
            ->get()->getResult();
        $grafica_dinero_redenciones_genero = [];
        foreach ($dinero_en_redenciones_por_genero as $genero) {
            array_push($grafica_dinero_redenciones_genero, (float) bcdiv(($genero->total * 100 / $dinero_en_redenciones),1,2));
        }

        $redenciones_genero_table = $this->dineroRedencionesPorGeneroTable();

        $builder = $this->db->table('receipts');
        $dinero_en_tickets_por_genero = $builder->select('SUM(receipts.amount) AS total, info.gender')->groupBy('info.gender')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('clients', 'clients.id = transactions.client_id')
            ->join('client_info as info', 'clients.id = info.client_id')
            ->where('receipts.enabled', 1)
            ->where('transactions.enabled', 1)
            ->orderBy('info.gender')
            ->get()->getResult();
        $grafica_dinero_tickets_genero = [];
        foreach ($dinero_en_tickets_por_genero as $genero) {
            array_push($grafica_dinero_tickets_genero, (float) bcdiv(($genero->total * 100 / $dinero_en_tickets),1,2));
        }

        $tickets_genero_table = $this->dineroTicketsPorGeneroTable();

        $builder = $this->db->table('receipts');
        $dinero_en_tickets_por_nivel = $builder->select('SUM(receipts.amount) AS total, level.name')->groupBy('level.name')
            ->join('transactions', 'receipts.id = transactions.receipt_id')
            ->join('cards', 'cards.id = transactions.card_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->where('receipts.enabled', 1)
            ->where('transactions.enabled', 1)
            ->orderBy('level.id')
            ->get()->getResult();
        $grafica_dinero_en_tickets_por_nivel = [];
        foreach ($dinero_en_tickets_por_nivel as $nivel) {
            array_push($grafica_dinero_en_tickets_por_nivel, (float) bcdiv(($nivel->total * 100 / $dinero_en_tickets), 1,2));
        }

        $tickets_nivel_table = $this->dineroTicketsPorNivelTable();

        $builder = $this->db->table('certificates');
        $dinero_en_redenciones_por_nivel = $builder->select('SUM(certificates.price * redemptions.amount) AS total, level.name')->groupBy('level.name')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->join('cards', 'cards.id = redemptions.card_id')
            ->join('card_levels as level', 'level.id = cards.card_level_id')
            ->where('redemptions.enabled', 1)
            ->orderBy('level.id')
            ->get()->getResult();
        $grafica_dinero_en_redenciones_por_nivel = [];
        foreach ($dinero_en_redenciones_por_nivel as $nivel) {
            array_push($grafica_dinero_en_redenciones_por_nivel, (float) bcdiv(($nivel->total * 100 / $dinero_en_redenciones), 1, 2));
        }

        $redenciones_nivel_table = $this->dineroRedencionesPorNivelTable();

        $builder = $this->db->table('receipts');
        $dinero_en_tickets_por_mes_db = $builder->select('SUM(amount) AS total, MONTH(date) AS month_numeric')->groupBy('month_numeric')
            ->where('enabled', 1)
            ->where('YEAR(date)', date('Y'))
            ->get()->getResult();
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        // Agregar meses sin reporte de tickets
        $dinero_en_tickets_por_mes = [];
        $grafica_dinero_en_tickets_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($dinero_en_tickets_por_mes_db as $registro) {
                if ($index + 1 == $registro->month_numeric) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($dinero_en_tickets_por_mes, $object);
                    array_push($grafica_dinero_en_tickets_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($dinero_en_tickets_por_mes, $object);
                array_push($grafica_dinero_en_tickets_por_mes['data'], 0);
            }
        }

        $builder = $this->db->table('certificates');
        $dinero_en_redenciones_por_mes_db = $builder->select('SUM(certificates.price * redemptions.amount) AS total, MONTH(redemptions.created_at) AS month_numeric')->groupBy('month_numeric')
            ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
            ->where('redemptions.enabled', 1)
            ->where('YEAR(redemptions.created_at)', date('Y'))
            ->get()->getResult();
        // Agregar meses sin reporte de tickets
        $dinero_en_redenciones_por_mes = [];
        $grafica_dinero_en_redenciones_por_mes = ['data' => [], 'labels' => $meses];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            $object = new \stdClass();
            $object->month = $mes;
            foreach ($dinero_en_redenciones_por_mes_db as $registro) {
                if ($index + 1 == $registro->month_numeric) {
                    $encontrado = true;
                    $object->total = $registro->total;
                    array_push($dinero_en_redenciones_por_mes, $object);
                    array_push($grafica_dinero_en_redenciones_por_mes['data'], $registro->total);
                    break;
                }
            }
            if (!$encontrado) {
                $object->total = 0;
                array_push($dinero_en_redenciones_por_mes, $object);
                array_push($grafica_dinero_en_redenciones_por_mes['data'], 0);
            }
        }

        $builder = $this->db->table('receipts as r');
        $dinero_en_tickets_por_rango_de_edad = $builder->select('SUM(r.amount) as total, 
CASE 
	WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 0 AND 17) THEN \'0 a 17\'
	WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 18 AND 25) THEN \'18 a 25\'
	WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 26 AND 35) THEN \'26 a 35\'
    WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 36 AND 45) THEN \'36 a 45\'
    WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 46 AND 55) THEN \'46 a 55\'
    WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 56 AND 65) THEN \'56 a 65\'
    WHEN (TIMESTAMPDIFF(YEAR, i.birthday, r.date) BETWEEN 66 AND 90) THEN \'66 a 90\'
END as rango')->groupBy('rango')
            ->join('transactions as t', 't.receipt_id = r.id')
            ->join('clients as c', 'c.id = t.client_id')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('YEAR(date)', date('Y'))
            ->where('r.enabled', 1)
            ->where('t.enabled', 1)
            ->get()->getResult();
        $grafica_dinero_en_tickets_por_rango_de_edad = ['data' => [], 'labels' => []];;
        foreach ($dinero_en_tickets_por_rango_de_edad as $edad) {
            array_push($grafica_dinero_en_tickets_por_rango_de_edad['data'], (float) bcdiv(($edad->total * 100 / $dinero_en_tickets_anio_actual), 1, 2));
            array_push($grafica_dinero_en_tickets_por_rango_de_edad['labels'], $edad->rango);
        }

        $tickets_rango_edad_table = $this->dineroTicketsPorRangoEdadTable();

        //dd($dinero_en_tickets_por_rango_de_edad_db);
        //dd($dinero_en_redenciones_por_mes);
        /*$grafica_dinero_en_redenciones_por_nivel = [];
        foreach ($dinero_en_redenciones_por_nivel as $nivel) {
            array_push($grafica_dinero_en_redenciones_por_nivel, round(($nivel->total * 100 / $dinero_en_redenciones)));
        }*/
        //dd($dinero_en_tickets_por_mes);
        /*$grafica_dinero_en_tickets_por_nivel = [];
        foreach ($dinero_en_tickets_por_nivel as $nivel) {
            array_push($grafica_dinero_en_tickets_por_nivel, round(($nivel->total * 100 / $dinero_en_tickets)));
        }*/

        /*$clientes_por_nivel = $builder->select('l.name, COUNT(*) as total')->groupBy('l.name')
            ->join('cards', 'c.id = cards.client_id')
            ->join('card_levels as l', 'l.id = cards.card_level_id')
            ->join('card_status as s', 's.id = cards.card_status_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('cards.enabled', 1)
            ->where('s.name', 'Habilitada')
            ->get()->getResult();

        $niveles = [];
        foreach ($clientes_por_nivel as $g) {
            array_push($niveles, round(($g->total * 100 / $clientes_totales)));
        }*/

        /*$clientes_por_edad = $builder->select('
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 18 AND 29, 1, 0)) as "18-29",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 30 AND 39, 1, 0)) as "30-39",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 40 AND 49, 1, 0)) as "40-49",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 50 AND 59, 1, 0)) as "50-59",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 60 AND 69, 1, 0)) as "60-69",
            SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN 70 AND 79, 1, 0)) as "70-79",
        ')
            ->join('client_info as i', 'c.id = i.client_id')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();

        $edades = [];
        foreach ($clientes_por_edad[0] as $clave => $valor) {
            array_push($edades, round(($valor * 100 / $clientes_totales)));
        }*/

        /*$clientes_por_regiones = $builder->select('states.name, COUNT(*) as total')->groupBy('states.name')
            ->join('client_info as i', 'c.id = i.client_id')
            ->join('states', 'states.id = i.estado')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->get()->getResult();
        $data_regiones = [];
        $regiones_name = [];
        $regiones_total = [];
        foreach ($clientes_por_regiones as $region) {
            array_push($regiones_name, $region->name);
            array_push($regiones_total, $region->total);
        }
        $data_regiones['data'] = $regiones_total;
        $data_regiones['labels'] = $regiones_name;*/

        /*$meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $clientes_por_periodo = $builder->select('MONTH(c.created_at) as mes, COUNT(*) as total')->groupBy('mes')
            ->where('c.enabled', 1)
            ->where('c.available', 1)
            ->where('YEAR(c.created_at)', date('Y'))
            ->get()->getResult();

        $periodos = [];
        $chart_periodo = [];
        $periodo_data = [];
        $periodo_labels = [];
        foreach ($meses as $index => $mes) {
            $encontrado = false;
            foreach ($clientes_por_periodo as $periodo) {
                if ($periodo->mes == $index + 1) {
                    array_push($periodos, ['mes' => $mes, 'total' => $periodo->total]);
                    array_push($periodo_data, $periodo->total);
                    array_push($periodo_labels, $mes);
                    $encontrado = true;
                    break;
                }
            }
            if (!$encontrado) {
                array_push($periodos, ['mes' => $mes, 'total' => 0]);
                array_push($periodo_data, 0);
                array_push($periodo_labels, $mes);
            }
        }
        $chart_periodo['data'] = $periodo_data;
        $chart_periodo['labels'] = $periodo_labels;*/

        //dd($clientes_totales, $clientes_por_genero, $clientes_por_nivel, $clientes_por_edad, $clientes_por_regiones, $clientes_por_periodo, $periodos);
        //dd($tickets_genero_table);
        return view('charts/money', compact('dinero_en_tickets', 'dinero_en_redenciones', 'dinero_en_redenciones_por_genero', 'grafica_dinero_redenciones_genero', 'dinero_en_tickets_por_genero', 'grafica_dinero_tickets_genero', 'dinero_en_tickets_por_nivel', 'grafica_dinero_en_tickets_por_nivel', 'dinero_en_redenciones_por_nivel', 'grafica_dinero_en_redenciones_por_nivel', 'dinero_en_tickets_por_mes', 'grafica_dinero_en_tickets_por_mes', 'dinero_en_redenciones_por_mes', 'grafica_dinero_en_redenciones_por_mes', 'dinero_en_tickets_por_rango_de_edad', 'grafica_dinero_en_tickets_por_rango_de_edad', 'redenciones_genero_table', 'tickets_genero_table', 'tickets_nivel_table', 'redenciones_nivel_table', 'tickets_rango_edad_table'));
    }

    private function dineroRedencionesPorGeneroTable()
    {
        $builder = $this->db->table('certificates');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $generos = ['M', 'F'];

        $clientes = [];
        foreach ($generos as $genero):
            $dinero_en_redenciones_por_genero = $builder->select('MONTH(redemptions.created_at) as mes, SUM(certificates.price * redemptions.amount) AS total, info.gender')->groupBy('MONTH(redemptions.created_at)')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->join('clients', 'clients.id = redemptions.client_id')
                ->join('client_info as info', 'clients.id = info.client_id')
                ->where('info.gender', $genero)
                ->where('redemptions.enabled', 1)
                //->orderBy('info.gender')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_redenciones_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function dineroTicketsPorGeneroTable()
    {
        $builder = $this->db->table('receipts');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $generos = ['M', 'F'];

        $clientes = [];
        foreach ($generos as $genero):
            $dinero_en_tickets_por_genero = $builder->select('MONTH(transactions.created_at) as mes, SUM(receipts.amount) AS total, info.gender')->groupBy('MONTH(transactions.created_at)')
                ->join('transactions', 'receipts.id = transactions.receipt_id')
                ->join('clients', 'clients.id = transactions.client_id')
                ->join('client_info as info', 'clients.id = info.client_id')
                ->where('info.gender', $genero)
                ->where('receipts.enabled', 1)
                ->where('transactions.enabled', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_tickets_por_genero as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'generos' => ['Hombres', 'Mujeres'],
            'backgrounds' => ['bg-primary', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function dineroTicketsPorNivelTable()
    {
        $builder = $this->db->table('receipts');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($niveles as $nivel):
            $dinero_en_tickets_por_nivel = $builder->select('MONTH(transactions.created_at) as mes, SUM(receipts.amount) AS total, level.name')->groupBy('MONTH(transactions.created_at)')
                ->join('transactions', 'receipts.id = transactions.receipt_id')
                ->join('cards', 'cards.id = transactions.card_id')
                ->join('card_levels as level', 'level.id = cards.card_level_id')
                ->where('level.name', $nivel)
                ->where('receipts.enabled', 1)
                ->where('transactions.enabled', 1)
                //->orderBy('level.id')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_tickets_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function dineroRedencionesPorNivelTable()
    {
        $builder = $this->db->table('certificates');
        $niveles = ['Blue', 'Gold', 'Silver'];
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $clientes = [];
        foreach ($niveles as $nivel):
            $dinero_en_redenciones_por_nivel = $builder->select('MONTH(redemptions.created_at) as mes, SUM(certificates.price * redemptions.amount) AS total, level.name')->groupBy('MONTH(redemptions.created_at)')
                ->join('redemptions_by_certificates as redemptions', 'certificates.id = redemptions.certificate_id')
                ->join('cards', 'cards.id = redemptions.card_id')
                ->join('card_levels as level', 'level.id = cards.card_level_id')
                ->where('level.name', $nivel)
                ->where('redemptions.enabled', 1)
                //->orderBy('level.id')
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes) {
                $encontrado = false;
                foreach ($dinero_en_redenciones_por_nivel as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            }
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'niveles' => $niveles,
            'backgrounds' => ['bg-primary', 'bg-success', 'bg-danger'],
            'periodo' => $clientes
        ];
        return $informacion;
    }

    private function dineroTicketsPorRangoEdadTable()
    {
        $builder = $this->db->table('receipts as r');
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $edades = ['0 AND 17', '18 AND 25', '26 AND 35', '36 AND 45', '46 AND 55', '56 AND 65', '66 AND 99'];
        $clientes = [];
        foreach ($edades as $edad):
            $dinero_en_tickets_por_rango_de_edad = $builder->select('
                MONTH(c.created_at) as mes, 
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', 1, 0)) as "'. $edad.'",
                SUM(IF(TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) BETWEEN '. $edad.', r.amount, 0)) as total')->groupBy('MONTH(c.created_at)')
                ->join('transactions as t', 't.receipt_id = r.id')
                ->join('clients as c', 'c.id = t.client_id')
                ->join('client_info as i', 'c.id = i.client_id')
                ->where('YEAR(date)', date('Y'))
                ->where('r.enabled', 1)
                ->where('t.enabled', 1)
                ->get()->getResult();
            $data = [];
            foreach ($meses as $index => $mes):
                $encontrado = false;
                foreach ($dinero_en_tickets_por_rango_de_edad as $item) {
                    if ($item->mes == $index + 1) {
                        array_push($data, ['mes' => $mes, 'total' => $item->total]);
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado) {
                    array_push($data, ['mes' => $mes, 'total' => 0]);
                }
            endforeach;
            array_push($clientes, $data);
        endforeach;
        $informacion = [
            'meses' => $meses,
            'edades' => ['De 0 a 17 años','De 18 a 25 años','De 26 a 35 años','De 36 a 45 años','De 46 a 55 años','De 56 a 65 años','De 66 a 90 años'],
            'backgrounds' => ['bg-primary', 'bg-danger', 'bg-secondary', 'bg-success', 'bg-warning', 'bg-dark','bg-secondary'],
            'periodo' => $clientes
        ];
       return $informacion;

    }

}