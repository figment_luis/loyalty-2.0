<?php namespace App\Controllers\Frontend;

use App\Controllers\BaseController;

class ServiceController extends BaseController
{

    //private $session = null;

    /*public function __construct()
    {
        $this->session = \Config\Services::session();
        helper('custom');
    }*/

    public function registrarCasosEspeciales()
    {
        // TODO: Generar servicio para registrar casos especiales correspondientes a un socio
        try {
            $rules = [
                'title' => [
                    'label' => 'Titulo del caso especial',
                    'rules' => 'trim|required|min_length[10]|max_length[60]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción del caso especial',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'priority' => [
                    'label' => 'Prioridad del caso especial',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
            ];
            $input = $this->validate($rules);
            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['errors'] = $this->validator->getErrors();

                return json_encode($response);
            }

            $title          = $this->request->getPost('title');
            $description    = $this->request->getPost('description');
            $priority       = $this->request->getPost('priority');
            $client_id        = $this->request->getPost('client_id');
            $card_id        = $this->request->getPost('card_id');

            $data = [
                'title' => $title,
                'description' => $description,
                'issue_priority_id' => $priority,
                'issue_status_id' => 1, // Pendiente
                'card_id' => $card_id,
                'client_id' => $client_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db     = db_connect('default');
            $builder      = $this->db->table('issues');
            $issueCreated = $builder->insert($data);

            if ($issueCreated) {
                // TODO: Registrar Log sobre esta operación en particular en el sistema
                $issueID = $this->db->insertID();
                $operation = $this->db->query("SELECT * FROM operations WHERE name = 'Alta Casos Especiales'")->getRow();
                $dataLog = [
                    'issue_id' => $issueID,
                    'user_id'  => session()->get('user_id'),
                    'operation_id' => $operation->id
                ];
                $this->db->table('issue_user_logs')->insert($dataLog);

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El caso especial se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el caso especial en el sistema, favor de intentar mas tarde.";
            }
            return json_encode($response);
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Loyalty, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            // "Error interno en el sistema, favor de contactar al área de IT."
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }

    }

    public function cambiarTarjeta()
    {
        // TODO: Generar servicio para registrar cambios de tarjeta
        try {
            $rules = [
                'reason' => [
                    'label' => 'Razón del cambio',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'card_number' => [
                    'label' => 'Nuevo número de tarjeta',
                    'rules' => 'trim|required|min_length[15]|max_length[15]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];
            $input = $this->validate($rules);
            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['errors'] = $this->validator->getErrors();

                return json_encode($response);
            }

            $reason          = $this->request->getPost('reason');
            $card_new_number = $this->request->getPost('card_number');
            $client_id       = $this->request->getPost('client_id');
            $card_old_id     = $this->request->getPost('card_id');
            $card_type       = $this->request->getPost('card_type');

            $this->db     = db_connect('default');

            // Obtener id del tipo: para la nueva tarjeta
            $card_type_id = $this->db->table('card_types')->where('name', $card_type)->get()->getRow()->id;
            // Obtener id del nivel actual: para la tarjeta
            $card_old = $this->db->table('cards')->where('id', $card_old_id)->get()->getRow();

            $usoActualID = $this->db->table('card_reasons')->where('name', 'Uso actual')->get()->getRow()->id;

            // Para registro de nueva tarjeta digital
            $dataDigital = [
                'number' => $card_new_number,
                'assigned_at' => date('Y-m-d H:i:s'),
                'enabled' => 1,
                'card_type_id' => $card_type_id, // Digital / Física
                'card_status_id' => 1, // Habilitada
                'card_level_id' => $card_old->card_level_id, // Con base a la tarjeta anterior
                'card_reason_id' => $usoActualID,
                'client_id' => $client_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            // Para actualizar registro de nueva tarjeta fisica (Esta ya existe en base de datos
            $dataFisica = [
                'assigned_at' => date('Y-m-d H:i:s'),
                'enabled' => 1,
                'card_status_id' => 1, // Habilitada
                'card_reason_id' => $usoActualID,
                'client_id' => $client_id,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            // Transacción: Deshabilitar tarjeta actual y habilitar la nueva tarjeta asignada
            $this->db->transBegin();

            // Deshabilitar tarjeta actual
            $this->db->table('cards')->where('id', $card_old_id)->update([
                'enabled' => 0,
                'card_reason_id' => $reason,
                'card_status_id' => 3, // Deshabilitada
            ]);

            // Habilitar la nueva tarjeta con base a su tipo
            if ($card_type == 'Digital') {
                // Registrar la nueva tarjeta con sus respectiva activación y asociación con cliente
                $this->db->table('cards')->insert($dataDigital);
            } else {
                // Actualizar el registro de la tarjeta con su respectiva activación y asociación con cliente
                $this->db->table('cards')->where('number', $card_new_number)->update($dataFisica);
            }

            // Confirmar operaciones en base de datos
            if ($this->db->transStatus() !== FALSE) {
                $this->db->transCommit();

                // TODO: Registrar Log sobre esta operación en particular en el sistema
                $dataLog = [
                    'current_card_number' => $card_old->number,
                    'new_card_number'  => $card_new_number,
                    'card_reason_id' => $reason,
                    'client_id' => $client_id,
                    'user_id' => session()->get('user_id'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $this->db->table('card_change_logs')->insert($dataLog);

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El cambio de tarjeta se registró satisfactoriamente en el sistema.";
            } else {
                $this->db->transRollback();
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el cambio de tarjeta en el sistema, favor de intentar mas tarde.";
            }
            return json_encode($response);
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Loyalty, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            // "Error interno en el sistema, favor de contactar al área de IT."
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function editarSocio()
    {
        // TODO: Generar servicio para actualizar usuario
        try {
            //Array errors
            $data['errors'] = [];
            // TODO: Terminar la validación de estos campos
            $rules = [
                'first_name' => [
                    'label' => 'Nombre',
                    'rules' => 'trim|required|min_length[2]|max_length[30]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'last_name' => [
                    'label' => 'Apellidos',
                    'rules' => 'trim|required|min_length[2]|max_length[30]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'gender' => [
                    'label' => 'Género',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                    ]
                ],
            ];
            $input = $this->validate($rules);
            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['errors'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // TODO: Verificar que el email no este presente para otro usuario
            $this->db = db_connect('default');

            $email = $this->request->getPost('email');
            $client_id  = $this->request->getPost('client_id');

            $emailExist = $this->db->table('clients')->where('email', $email)->where('id !=', $client_id)->get()->getRow();
            if ($emailExist) {
                $response["status"]     = "invalid";
                $response["code"]       = 400;
                $response["message"]    = "Estimado usuario, el correo electrónico ya se encuentra en uso por otro usuario.";
                return json_encode($response);
            }


            $client_id  = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING); 
            $first_name = filter_var($this->request->getPost('first_name'), FILTER_SANITIZE_STRING);
            $last_name  = filter_var($this->request->getPost('last_name'), FILTER_SANITIZE_STRING);
            $email      = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
            $newsletter = filter_var($this->request->getPost('newsletter'), FILTER_SANITIZE_NUMBER_INT);
            $dataClient = [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'newsletter' => $newsletter ?: 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            if ($this->request->getPost('password')) {
                $dataClient['password'] = password_hash(filter_var($this->request->getPost('password'), FILTER_SANITIZE_STRING), PASSWORD_BCRYPT);
            }
            $email_alternative  = filter_var($this->request->getPost('email_alternative'), FILTER_SANITIZE_EMAIL);
            $telephone          = filter_var($this->request->getPost('telephone'), FILTER_SANITIZE_NUMBER_INT);
            $mobile             = filter_var($this->request->getPost('mobile'), FILTER_SANITIZE_NUMBER_INT);
            $gender             = filter_var($this->request->getPost('gender'), FILTER_SANITIZE_STRING);
            $birthday           = filter_var($this->request->getPost('birthday'), FILTER_SANITIZE_STRING);
            $street             = filter_var($this->request->getPost('street'), FILTER_SANITIZE_STRING);
            $exterior           = filter_var($this->request->getPost('exterior'), FILTER_SANITIZE_STRING);
            $interior           = filter_var($this->request->getPost('interior'), FILTER_SANITIZE_STRING);
            $codigo_postal      = filter_var($this->request->getPost('codigo_postal'), FILTER_SANITIZE_NUMBER_INT);
            $estado             = filter_var($this->request->getPost('estado'), FILTER_SANITIZE_NUMBER_INT);
            $municipio          = filter_var($this->request->getPost('municipio'), FILTER_SANITIZE_NUMBER_INT);
            $colonia            = filter_var($this->request->getPost('colonia'), FILTER_SANITIZE_NUMBER_INT);


            if(!$first_name){ $data['errors']['first_name'] = 'El nombre debe ser obligatorio';}
            if(!$last_name){ $data['errors']['last_name'] = 'El apellido debe ser obligatorio';}
            if(!$email){ $data['errors']['email'] = 'El email debe ser obligatorio';}
            if(!$mobile){ $data['errors']['mobile'] = 'El telefono movil debe ser obligatorio';}
            if(!$gender){ $data['errors']['gender'] = 'Debe seleccionar un genero';}
            if(!$birthday){ $data['errors']['birthday'] = 'El cumpleaños debe ser obligatorio';}
            if(!$codigo_postal){ $data['errors']['codigo_postal'] = 'El codigo postal es un campo obligatorio';}
            if(!$estado){ $data['errors']['estado'] = 'El estado es un campo obligatorio';}
            if(!$municipio){ $data['errors']['municipio'] = 'El Municipio es un campo obligatorio';}
            if(!$colonia){ $data['errors']['colonia'] = 'La colonia es un campo obligatorio';}


            if($data['errors']){
                $data['message'] = 'Error de validación de datos';
                $data['status'] = 'invalid';
                $data['code'] = 406;
                return json_encode($data);
                
            }else {
                $dataInfoClient= [
                    'email_alternative' => $email_alternative,
                    'telephone' => $telephone,
                    'mobile' => $mobile,
                    'gender' => $gender,
                    'birthday' => $birthday,
                    'street' => $street,
                    'exterior' => $exterior,
                    'interior' => $interior,
                    'codigo_postal' => $codigo_postal,
                    'estado' => $estado,
                    'municipio' => $municipio,
                    'colonia' => $colonia,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
    
                $this->db = db_connect('default');
    
                // Transacción: Actualizar datos del cliente e Información asociada al cliente
                $this->db->transBegin();
    
                $this->db->table('clients')->where('id', $client_id)->update($dataClient);
                $this->db->table('client_info')->where('client_id', $client_id)->update($dataInfoClient);
    
                // Confirmar operaciones en base de datos
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
    
                    // TODO: Registrar Log sobre esta operación en particular en el sistema
                    $dataLog = [
                        'client_id' => $client_id,
                        'user_id' => session()->get('user_id'),
                        'operation_id' => $this->db->table('operations')->where('name', 'Editar Cliente')->get()->getRow()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $this->db->table('users_clients_logs')->insert($dataLog);
    
                    $response["status"]     = "success";
                    $response["code"]       = 200;
                    $response["message"]    = "El cliente fue actualizado satisfactoriamente.";
                } else {
                    $this->db->transRollback();
                    $response["status"]     = "error";
                    $response["code"]       = 400;
                    $response["message"]    = "No fue posible actualizar el cliente en el sistema, favor de intentar mas tarde.";
                }
                return json_encode($response);

            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Loyalty, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            // "Error interno en el sistema, favor de contactar al área de IT."
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function consultarPuntos($id) {
        $pointsModel = model('App\Models\Services\PointsModel');
        $clientModel = model('App\Models\Services\ClientModel');
        $points = $pointsModel->getPoints($id)[0];
        $client = $clientModel->getClientInfoByID($id)[0];

        $data = [
            'points' => $points,
            'client' => $client
        ];

        return $this->response->setJSON($data);
    }

}
