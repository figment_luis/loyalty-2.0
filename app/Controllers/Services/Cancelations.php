<?php

namespace App\Controllers\Services;
use App\Controllers\Services\Rules;

class Cancelations extends \App\Controllers\Services\Rules{

    public function cancelReceipt()
	{		
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $receiptLogModel = model('App\Models\Services\ReceiptLogModel');
            $pointsModel = model('App\Models\Services\PointsModel');
            $receiptModel= model('App\Models\Services\ReceiptsModel');
            $transactionModel = model('App\Models\Services\TransactionsModel');


            //Extract Form Data
            $receipt_id = filter_var($this->request->getPost('receipt_id'), FILTER_SANITIZE_STRING);
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);
            $description = filter_var($this->request->getPost('desctiption'), FILTER_SANITIZE_STRING);

            try {

                $receiptDetail = $transactionModel->getTransactionByReceiptId($receipt_id)[0];
                $clientPoints = $pointsModel->getPoints($client_id)[0];

                if($receiptDetail){

                    if($clientPoints['points_available'] >= $receiptDetail['points']){ //Si el cliente cuenta con los puntos aun

                        $pointsTableData = [
                            'points_available' => $clientPoints['points_available'] - $receiptDetail['points']
                        ];
                        $pointsModel->update($clientPoints['id'], $pointsTableData);

                    } else { //Si el cliente ya hizo uso de dichos puntos
                        $pointsTableData = [
                            'points_available' => '0',
                            'debt_points' => $receiptDetail['points'] - $clientPoints['points_available']
                        ];
                        $pointsModel->update($clientPoints['id'], $pointsTableData);
                    }

                    $transactionTableData = [
                        'is_canceled' => '1'
                    ];
                    $transactionModel->update($receiptDetail['id'], $transactionTableData);

                    $user_id = session()->get('user_id');

                    $receiptLogTableData = [
                        'receipt_id' => $receiptDetail['id'],
                        'user_id' => $user_id,
                        'operation_id' => 5,
                        'description' => $description
                    ];
                    $receiptLogModel->insert($receiptLogTableData);


                    $data['status'] = $user_id;
                    $data['message'] = 'La acreditación ha sido cancelada exitosamente';
                    $data['code'] = 200;


                } else{
                    $data['status'] = false;
                    $data['message'] = 'El recibo indicado no se encuentra registrado';
                    $data['code'] = 404;
                }
            } catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
            

        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}


    public function cancelRedemption()
	{		
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $redemptionModel = model('App\Models\Services\RedemptionsCertificatesModel');
            $redemptionLogModel = model('App\Models\Services\RedemptionsCertificatesLogModel');
            $pointsModel = model('App\Models\Services\PointsModel');
            $certificateModel = model('App\Models\Services\CertificatesModel');
            $certificateLogModel = model('App\Models\Services\CertificatesLogStockModel');
            


            //Extract Form Data
            $redemption_id = filter_var($this->request->getPost('redemption_id'), FILTER_SANITIZE_STRING);
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);
            $description = filter_var($this->request->getPost('desctiption'), FILTER_SANITIZE_STRING);

            try {

                $redemption_info = $redemptionModel->getRedemptionById($redemption_id)[0];
                
                if($redemption_info){

                    $certificate_info = $certificateModel->getCertificateInfo($redemption_info['certificate_id'])[0];
                    $points = $pointsModel->getPoints($client_id)[0];

                    //Realizamos un Calculo de los puntos 

                    $returnedPoints = ($certificate_info['points'] * $redemption_info['amount']);

                    $pointsTableData = [
                        'points_available' => $points['points_available'] + $returnedPoints,
                        'points_redeemed' => $points['points_redeemed'] - $returnedPoints
                    ];
                    $pointsModel->update($points['id'], $pointsTableData);

                    //Establecemos como cancelada la redencion
                    $redemptionTableData = [
                        'is_canceled' => '1'
                    ];
                    $redemptionModel->update($redemption_id, $redemptionTableData);


                    //Creamos un log para tener registro de la cancelacion
                    $user_id = session()->get('user_id');

                    $redemptionLogTableData = [
                        'redemption_by_certificate_id' => $redemption_id,
                        'user_id' => $user_id,
                        'operation_id' => 6,
                        'description' => $description,
                        'returned_points' => $returnedPoints
                    ];
                    $redemptionLogModel->insert($redemptionLogTableData);

                    //Devolver Stock en caso de ser ilimitado

                    $certificateTableData = [
                        'current_stock' => $certificate_info['current_stock'] + $redemption_info['amount'],
                        'exchanges' => $certificate_info['exchanges'] - $redemption_info['amount']
                    ];
                    $certificateModel->update($certificate_info['id'], $certificateTableData);


                    $certificateLogTableData = [
                        'increase' => $redemption_info['amount'],
                        'decrease' => 0,
                        'certificate_id' => $certificate_info['id'],
                        'user_id' => $user_id,
                    ];
                    $certificateLogModel->insert($certificateLogTableData);
                
                    $data['status'] = true;
                    $data['message'] = 'La redención ha sido cancelada exitosamente';
                    $data['code'] = 200;

                } else{
                    $data['status'] = false;
                    $data['message'] = 'El recibo indicado no se encuentra registrado';
                    $data['code'] = 404;
                }
            } catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}


    public function cancelBenefit()
	{		
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $redemptionModel = model('App\Models\Services\RedemptionsBenefitsModel');
            $redemptionLogModel = model('App\Models\Services\RedemptionsBenefitsLogModel');
            $benefitModel = model('App\Models\Services\BenefitsModel');
            $benefitLogModel = model('App\Models\Services\BenefitsLogStockModel');
            


            //Extract Form Data
            $redemption_id = filter_var($this->request->getPost('redemption_id'), FILTER_SANITIZE_STRING);
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);
            $description = filter_var($this->request->getPost('desctiption'), FILTER_SANITIZE_STRING);

            $user_id = session()->get('user_id');

            try {

                $redemption_info = $redemptionModel->getRedemptionById($redemption_id)[0];

                if($redemption_info){

                    $benefit_info = $benefitModel->getBenefitInfo($redemption_info['benefit_id'])[0];

                    //Establecemos la redencion como cancelada
                    $redemptionTableData = [
                        'is_canceled' => '1'
                    ];
                    $redemptionModel->update($redemption_id, $redemptionTableData);

                    //Actualizamos el stock del beneficio en caso de ser necesario

                    if($benefit_info['unlimited_stock'] == 0){
                        $benefitTableData = [
                            'current_stock' => $benefit_info['current_stock'] + $redemption_info['amount']
                        ];
                        $benefitModel->update($benefit_info['id'], $benefitTableData);

                        //Creamos un registro del intremento de stock

                        $benefitLogTableData = [
                            'increase' => $redemption_info['amount'],
                            'decrease' => 0,
                            'benefit_id' => $benefit_info['id'],
                            'user_id' => $user_id,
                        ];
                        $benefitLogModel->insert($benefitLogTableData);
                    }

                    //Creamos un registro de la cancelacion
                    

                    $redemptionLogTableData = [
                        'redemption_by_benefit_id' => $redemption_id,
                        'user_id' => $user_id,
                        'operation_id' => 7,
                        'description' => $description
                    ];
                    $redemptionLogModel->insert($redemptionLogTableData);

                    
                    $data['status'] = true;
                    $data['message'] = 'La redención ha sido cancelada exitosamente';
                    $data['code'] = 200;

                } else{
                    $data['status'] = false;
                    $data['message'] = 'El recibo indicado no se encuentra registrado';
                    $data['code'] = 404;
                }
            } catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}

}