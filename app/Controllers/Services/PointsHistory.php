<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class PointsHistory extends BaseController
{
    public function saveData(){

        //Load Models
        $pointsHistoryModel = model('App\Models\Services\PointsHistoryModel');


        try{ 
            $save = $pointsHistoryModel->saveActualData();
            
            $data['status'] = true;
            $data['message'] = 'Puntos guardados exitosamente';
            $data['code'] = 200;

            }catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function getDates(){

        //Load Models
        $pointsHistoryModel = model('App\Models\Services\PointsHistoryModel');


        try{ 
            $dates = $pointsHistoryModel->getAvailableDates();
            
            $data['status'] = true;
            $data['message'] = 'Fechas obtenidas exitosamente';
            $data['code'] = 200;
            $data['dates'] = $dates;

            }catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function getHistoric(){

        //Load Models
        $pointsHistoryModel = model('App\Models\Services\PointsHistoryModel');

        if ($this->request->isAJAX()){
            try{ 

                $startDate = filter_var($this->request->getPost('start_date'), FILTER_SANITIZE_STRING);
                $endDate = filter_var($this->request->getPost('end_date'), FILTER_SANITIZE_STRING);

                if( $startDate AND $endDate ){  //Chekc if the front-end send both dates

                    $startTable = $pointsHistoryModel->getHistoricByDate($startDate);
                    $startData = $pointsHistoryModel->getDataHistoricByDate($startDate);

                    $endTable = $pointsHistoryModel->getHistoricByDate($endDate);
                    $endData = $pointsHistoryModel->getDataHistoricByDate($endDate);


                    $data['inicio_data'] = $startData;
                    $data['fin_data'] = $endData;

                    $data['inicio_table'] = $startTable;
                    $data['fin_table'] = $endTable;

                    $data['status'] = true;
                    $data['message'] = 'Reporte generado exitosamente';
                    $data['code'] = 200;
                } else {
                    
                    $data['status'] = false;
                    $data['message'] = 'Debe ingresar ambas fechas';
                    $data['code'] = 400;
                }
           
                }catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }

        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }

        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }




}