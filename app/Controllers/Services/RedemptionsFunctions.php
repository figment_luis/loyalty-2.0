<?php

namespace App\Controllers\Services;
use App\Controllers\Services\Rules;

class RedemptionsFunctions extends \App\Controllers\Services\Rules{

    public function redemptionRegister(){
        if ($this->request->isAJAX()){
            

            //Array errors
            $data['errors'] = [];

            //Load Models
            $redemptionsModel =  model('App\Models\Services\RedemptionsCertificatesModel');
            $redemptionsLogsModel =  model('App\Models\Services\RedemptionsCertificatesLogModel');
            $transactionModel = model('App\Models\Services\TransactionsModel');
            $cardModel = model('App\Models\Services\CardsModel');
            $clientModel = model('App\Models\Services\ClientModel');
            $pointsModel = model('App\Models\Services\PointsModel');
            $certificatesModel = model('App\Models\Services\CertificatesModel');
            $certificatesLogModel = model('App\Models\Services\CertificatesLogStockModel');
            

            //Extract Redemption Table Data
            $certificate_id = filter_var($this->request->getPost('certificate_id'), FILTER_SANITIZE_STRING);
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);
            $amount = filter_var($this->request->getPost('amount'), FILTER_SANITIZE_NUMBER_INT);

            //Validation Rules
            if(!$certificate_id){ $data['errors']['certificate_id'] = 'El numero de certificado debe ser obligatorio';}
            if(!$client_id){ $data['errors']['client_id'] = 'El id del cliente debe ser obligatorio';}
            if(!$amount){ $data['errors']['amount'] = 'El monto debe ser obligatorio';}


            $user_id = session()->get('user_id');

            if($data['errors'] == []){

                $certificateInfo =$certificatesModel->getCertificateInfo($certificate_id)[0];
                $cardInfo = $cardModel->getCardInfoByClientID($client_id)[0];
                $clientInfo = $clientModel->getClientInfoByID($client_id)[0];
                $pointsInfo = $pointsModel->getPoints($client_id)[0];

                $points = (int)$certificateInfo['points'] * $amount;
                $points_reduciton = $pointsInfo['points_available'] - $points;
                $points_redeemed = $pointsInfo['points_redeemed'] + $points;


                if($points <= $pointsInfo['points_available']){
                    if($amount <= $certificateInfo['current_stock']){
                        try {
                            $user_id = session()->get('user_id');
    
                            //Set Redeption Certificates Table Data
                            $redeptionTableData = [
                                'amount' => $amount,
                                'enabled' => 1,
                                'certificate_id' => $certificate_id,
                                'card_id' => $cardInfo['id'],
                                'client_id' => $client_id
                            ];
            
                            $redemptionsModel->insert($redeptionTableData);
    
                            //Set Redeption Certificates Logs Table Data
                            $redeptionLogTableData = [
                                'redemption_by_certificate_id' => $redemptionsModel->getInsertID(),
                                'user_id' => $user_id,
                                'returned_points' => 0,
                                'description' => '',
                                'operation_id' => 2
                            ];
            
                            $redemptionsLogsModel->insert($redeptionLogTableData);
    

                            //Set Certificates Table Data
                            $certificatesTableData = [
                                'current_stock' => $certificateInfo['current_stock'] - $amount,
                                'exchanges' => $certificateInfo['exchanges'] + $amount
                            ];   
                           
                            $certificatesModel->update($certificate_id, $certificatesTableData);
                            

                            //Set Certificates Log Stock Table Data
                            $certificatesLogTableData = [
                                'decrease' => $amount,
                                'certificate_id' => $certificate_id,
                                'user_id' => $user_id
                            ];

                            $certificatesLogModel->insert($certificatesLogTableData);

                            //Set Certificates Log Stock Table Data
                            $PointsModelTableData = [
                                'decrease' => $amount,
                                'certificate_id' => $certificate_id,
                                'user_id' => $user_id
                            ];

                            $pointsModel->where('client_id', $client_id)->set(['points_available' => $points_reduciton])->set(['points_redeemed' => $points_redeemed])->update();
    
    
                            $data['status'] = true;
                            $data['message'] = 'Redencion registrada exitosamente';
                            $data['code'] = 200;
                
                        } catch (\Exception $e){
                            $data['status'] = false;
                            $data['message'] = $e->getMessage();
                            $data['code'] = 500;
                        }
                    } else {
                        $data['status'] = false;
                        $data['message'] = 'La cantidad de certificados ingresada supera el stock actual';
                        $data['code'] = 400;
                    }
                    
                } else {
                    $data['status'] = false;
                    $data['message'] = 'El cliente no cuenta con los puntos suficiones para realizar la redencion';
                    $data['code'] = 400;
                }                
            } else {
                $data['status'] = false;
                $data['message'] = 'Error de validación de datos';
                $data['code'] = 406;
            }
        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }

        header("Content-type: application/json; charset=utf-8");
	    die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }



    public function redemptionBenefit(){
        if ($this->request->isAJAX()){
            

            //Array errors
            $data['errors'] = [];

            //Load Models
            $redemptionsModel =  model('App\Models\Services\RedemptionsBenefitsModel');
            $redemptionsLogsModel =  model('App\Models\Services\RedemptionsBenefitsLogModel');
            $cardModel = model('App\Models\Services\CardsModel');
            $clientModel = model('App\Models\Services\ClientModel');
            $pointsModel = model('App\Models\Services\PointsModel');
            $benefitsModel = model('App\Models\Services\BenefitsModel');
            $benefitsLogModel = model('App\Models\Services\BenefitsLogStockModel');

            //Extract Redemption Table Data
            $benefit_id = filter_var($this->request->getPost('benefit_id'), FILTER_SANITIZE_STRING);
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);
            $amount = filter_var($this->request->getPost('amount'), FILTER_SANITIZE_NUMBER_INT);

            //Validation Rules
            if(!$benefit_id){ $data['errors']['certificate_id'] = 'El numero de certificado debe ser obligatorio';}
            if(!$client_id){ $data['errors']['client_id'] = 'El id del cliente debe ser obligatorio';}
            if(!$amount){ $data['errors']['amount'] = 'El monto debe ser obligatorio';}


            $user_id = session()->get('user_id');

            if($data['errors'] == []){

                $benefitInfo = $benefitsModel->getBenefitInfo($benefit_id)[0];
                $cardInfo = $cardModel->getCardInfoByClientID($client_id)[0];
                $clientInfo = $clientModel->getClientInfoByID($client_id)[0];
                $pointsInfo = $pointsModel->getPoints($client_id)[0]; 

                if( (int) $cardInfo['card_level_id'] >= (int)$benefitInfo['level_id']){
                    if($amount <= $benefitInfo['current_stock'] OR $benefitInfo['unlimited_stock'] == 1 ) {
                        try {


                            //Test Rules Code
                            $rules = $this->validate_time_rules($benefit_id, $client_id, (int)$benefitInfo['rule_days'], (int)$benefitInfo['rule_months'], $amount, (int)$benefitInfo['rule_week'], $benefitInfo['rule_shop']);
                             
                            if($rules['status'] == false){
                            $data['status'] = false;
                            $data['message'] = $rules['message'];
                            $data['code'] = 412;

                                header("Content-type: application/json; charset=utf-8");
                                die(json_encode($data, JSON_UNESCAPED_UNICODE));
                            }


                            


                            $user_id = session()->get('user_id');
                            $user_id = 1;

                            //Set Redeption Benefits Table Data
                            $redeptionTableData = [
                                'amount' => $amount,
                                'enabled' => 1,
                                'benefit_id' => $benefit_id,
                                'card_id' => $cardInfo['id'],
                                'client_id' => $client_id,
                                'card_level_id' => $cardInfo['card_level_id']
                            ];
            
                            $redemptionsModel->insert($redeptionTableData);

                            //Set Redeption Benefits Logs Table Data
                            $redeptionLogTableData = [
                                'redemption_by_benefit_id' => $redemptionsModel->getInsertID(),
                                'user_id' => $user_id,
                                'returned_points' => 0,
                                'description' => '',
                                'operation_id' => 8
                            ];

                            $redemptionsLogsModel->insert($redeptionLogTableData);
            
                            //Set Benefits Table Data
                            if($benefitInfo['unlimited_stock'] != 1){
                                $benefitsTableData = [
                                    'current_stock' => $benefitInfo['current_stock'] - $amount
                                ];
                                $benefitsModel->update($benefitInfo['id'], $benefitsTableData);
                            }


                            //Set Benefits Log Stock Table Data
                            $benefitsLogTableData = [
                                'decrease' => $amount,
                                'benefit_id' => $benefit_id,
                                'user_id' => $user_id
                            ];

                            $benefitsLogModel->insert($benefitsLogTableData);

                            $data['status'] = true;
                            $data['message'] = 'Redencion registrada exitosamente';
                            $data['code'] = 200;
                
                        } catch (\Exception $e){
                            $data['status'] = false;
                            $data['message'] = $e->getMessage();
                            $data['code'] = 500;
                        }
                    } else {
                        $data['status'] = false;
                        $data['message'] = 'La cantidad de beneficios ingresada supera el stock actual';
                        $data['code'] = 400;
                    }    
                }else {
                    $data['status'] = false;
                    $data['message'] = 'El usuario no cuenta con el nivel requerido para canjear este beneficio';
                    $data['code'] = 400;
                }
                              
            } else {
                $data['status'] = false;
                $data['message'] = 'Error de validación de datos';
                $data['code'] = 406;
            }
        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }

        header("Content-type: application/json; charset=utf-8");
	    die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    

}