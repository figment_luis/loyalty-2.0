<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class Rules extends BaseController{

    //This function check if a benefit have the quantity rule by days months or weeks
    public function validate_time_rules($benefit_id, $client_id, $num_day, $num_month, $amount, $week, $rule_shop){

        //Variables
        $days = 15; //Num of days to count the last receipts
        $rule_amount = 150; //Amount that client needs to pass the validation

        //load the Model
        $redemptionsModel =  model('App\Models\Services\RedemptionsBenefitsModel');
        $redemptionsCertificatesModel =  model('App\Models\Services\RedemptionsCertificatesModel');


        $rule_days=$redemptionsModel->validate_time_days($benefit_id, $client_id)[0];
        $rule_months=$redemptionsModel->validate_time_months($benefit_id, $client_id)[0];
        $rule_week=$redemptionsModel->validate_time_week($benefit_id, $client_id)[0];

        $quantity=$redemptionsCertificatesModel->validate_last_shippings($client_id, $days)[0];


        if ($rule_shop AND $quantity['amount'] <= $rule_amount){
            $data['status'] = false;
            $data['message'] = 'El cliente no puede canjear debido a que no ha acumulado un total de '. $amount . ' pesos en compras durante los últimos '. $days . ' días';
        } else if($amount + $rule_days['quantity'] > $num_day AND $num_day != 0){
            $data['status'] = false;
            $data['message'] = 'El cliente no puede canjear este beneficio, debido a que ya ha llegado a el limite el dia de hoy. <br> <p> Máximo por día: <b>'.$num_day.'</b></p>';
        }else if($amount + $rule_months['quantity'] > $num_month AND $num_month != 0){
            $data['status'] = false;
            $data['message'] = 'El cliente no puede canjear este beneficio, debido a que ya ha llegado a el limite del mes. <br> <p> Máximo por mes: <b>'.$num_month.'</b></p>';
        }else if($amount + $rule_week['quantity'] > $week AND $week != 0) {
            $data['status'] = false;
            $data['message'] = 'El cliente no puede canjear este beneficio, debido a que ya lo ha canjeado esta semana. <br> <p> Máximo por semana: <b>'.$week.'</b></p>';
        } else {
            $data['status'] = true;
        }
        
        return $data;
    }

    public function updateClientLevel($client_id){
        $months = 18;   //Cantidad de meses para el calculo de puntos para el nivel del cliente
        $newLevel = 1; //Initializate Level on the basic level

        $clientModel =  model('App\Models\Services\ClientModel');
        $pointsModel =  model('App\Models\Services\PointsModel');
        $cardsModel  =  model('App\Models\Services\CardsModel');

        $clientInfo = $pointsModel->getPoints($client_id)[0];
        $levels = $cardsModel->getCardLevels();
        $cardInfo = $cardsModel->getCardInfoByClientID($client_id)[0];
        $points = $pointsModel->getLevelPoints($client_id, $months)[0];

        /*First check if the client*/

        $firstYear = $clientModel->checkFirstYear($client_id)[0];

        /*In this section we calculate the level of the client according to the client points*/
        foreach($levels as $level){
            if ((int)$points['points'] >= $level['points']){
                $newLevel = $level['id'];
            }
        }

        if(($firstYear['years'] > 0) OR ($firstYear['years'] == 0 AND $newLevel >= $cardInfo['card_level_id'])){
            if ($newLevel != $cardInfo['card_level_id']){
                $newTableData = [
                    'card_level_id' => $newLevel
                ];
                $cardsModel->update($cardInfo['id'], $newTableData);
            }
        }

        if($firstYear['years'] > 0){
            $updateTable = [
                'first_year' => 0
            ];
            $clientModel->update($client_id, $updateTable);
        }
    }


    public function expirePoints(){
        $today = date('Y-m-d');
        $today=date('Y-m-d', strtotime($today));

        $endDate = date('Y-m-d', strtotime("10/06/2021"));  //Formato = Mes/Dia/Año


        $pointsModel =  model('App\Models\Services\PointsModel');
        $expirePointsModel = model('App\Models\Services\ExpirePointsLogModel');

        if($today === $endDate){

            $registros = $pointsModel->getAllRegisters();

            foreach( $registros as $registro ){
                $tableLogData = [
                    'client_id' => $registro['client_id'],
                    'points_expired' => $registro['points_available'],
                    'points_redeemed' => $registro['points_redeemed']
                ];

                $expirePointsModel->insert($tableLogData);

                $pointsTableData = [
                    'points_available' => 0,
                    'points_redeemed' => 0
                ];

                $pointsModel->update($registro['id'], $pointsTableData);
            }
            $data['code'] = '200';
        } else {
            $data['code'] = '400';
        }

        header("Content-type: application/json; charset=utf-8");
        die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}
