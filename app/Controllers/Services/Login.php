<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class Login extends BaseController
{
    private $session = null;

    public function __construct()
    {
        $this->session = \Config\Services::session();
    }


	public function loginValidation()
	{
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $loginModel = model('App\Models\Services\LoginModel');
            $accessLogModel = model('App\Models\Services\AccessLogModel'); 

            //Extract Form Data
            $email = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
            $password = filter_var($this->request->getPost('password'), FILTER_SANITIZE_STRING);

            if(!$email){
                $data['errors']['email'] = 'El email debe ser obligatorio';
            }

            if(!$password){
                $data['errors']['password'] = 'La contraseña es obligatoria';
            }

            if($data['errors']){
                $data['message'] = 'Error de validación de datos';
                $data['status'] = false;
                $data['code'] = 406;


            } else {    //If input has no errors
                try {

                    //Get the Login info
                    $loginInfo =  $loginModel->getLoguinInfo($email);
                    $roleInfo = $loginModel->getRoleInfo($loginInfo[0]['user_role_id'])[0];

                    if(password_verify($password, $loginInfo[0]['password'])){
                        
                        //Prepare all data session
                        $dataSesion=[
                            'user_id' => $loginInfo[0]['id'],
                            'role' => $loginInfo[0]['user_role_id'],
                            'display_role' => $roleInfo['display_name'],
                            'first_name' => $loginInfo[0]['first_name'],
                            'last_name' => $loginInfo[0]['last_name'],
                            'avatar' => $loginInfo[0]['avatar'],
                            'email' => $loginInfo[0]['email'],
                            'login' => true
                        ];

                        //Set the data session
                        $this->session->set($dataSesion);

                        //Init User Agent
                        $agent = $this->request->getUserAgent();

                        $accessLogData = [
                            'user_agent' => $agent->getAgentString(),
                            'ip_address' => $this->request->getIPAddress(),
                            'device' => $agent->getPlatform(),
                            'user_id' =>$loginInfo[0]['id']
                        ];

                        $accessLogModel->insert($accessLogData);


                        //Set the response
                        $data['status'] = true;
                        $data['code'] = 200;
                        $data['message'] = 'Se ha logueado Correctamente';
                        $data['name'] = $loginInfo[0]['first_name'];
                    }
                    else {
                        $data['status'] = false;
                        $data['code'] = 400;
                        $data['message'] = 'Credenciales de accesos incorrectas';
                    }

                } catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            }

        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}

	public function login()
    {
        if (session()->get('login')) {
            return redirect()->to('/socios/panel-de-control');
        }

        return view('auth/login');
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect('/');
    }
}