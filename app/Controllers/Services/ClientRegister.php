<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class ClientRegister extends BaseController{

    public function generateNewCard(){
        $strength = 15;
        $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($permitted_chars);


        $cardModel = model('App\Models\Services\CardsModel');
        

        do{
            $random_string = '';
            for($i = 0; $i < $strength; $i++) {
                $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                $random_string .= $random_character;
            }

            $random_string[3] = '-';
            $random_string[7] = '-';
            $random_string[11] = '-';

            $search = $cardModel->getCardInfo($random_string);

        } while ($search);

        $data['card_number'] = $random_string;

        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function clientRegister(){
        if ($this->request->isAJAX()){
             //Array errors
             $data['errors'] = [];
            
             //Load Models
             $clientModel = model('App\Models\Services\ClientModel');
             $infoClientModel = model('App\Models\Services\InfoClientModel');
             $cardModel = model('App\Models\Services\CardsModel');
             $pointsModel = model('App\Models\Services\PointsModel');


            //Extract Form Data

            //Client Table Data
            $first_name = filter_var($this->request->getPost('first_name'), FILTER_SANITIZE_STRING);
            $last_name = filter_var($this->request->getPost('last_name'), FILTER_SANITIZE_STRING);
            $email = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
            $password = filter_var($this->request->getPost('password'), FILTER_SANITIZE_STRING);
            $newsletter = filter_var($this->request->getPost('newsletter'), FILTER_SANITIZE_NUMBER_INT);

            //Info Client Table Data
            $email_alternative = filter_var($this->request->getPost('email_alternative'), FILTER_SANITIZE_STRING);
            $telephone = filter_var($this->request->getPost('telephone'), FILTER_SANITIZE_STRING);
            $mobile = filter_var($this->request->getPost('mobile'), FILTER_SANITIZE_STRING);
            $gender = filter_var($this->request->getPost('gender'), FILTER_SANITIZE_STRING);
            $birthday = filter_var($this->request->getPost('birthday'), FILTER_SANITIZE_STRING);
            $street = filter_var($this->request->getPost('street'), FILTER_SANITIZE_STRING);
            $exterior = filter_var($this->request->getPost('exterior'), FILTER_SANITIZE_STRING);
            $interior = filter_var($this->request->getPost('interior'), FILTER_SANITIZE_STRING);
            $codigo_postal = filter_var($this->request->getPost('codigo_postal'), FILTER_SANITIZE_STRING);
            $estado = filter_var($this->request->getPost('estado'), FILTER_SANITIZE_STRING);
            $municipio = filter_var($this->request->getPost('municipio'), FILTER_SANITIZE_STRING);
            $colonia = filter_var($this->request->getPost('colonia'), FILTER_SANITIZE_STRING);

            //Card Table Data

            $card_number = filter_var($this->request->getPost('card_number'), FILTER_SANITIZE_STRING);
            $card_level_id = filter_var($this->request->getPost('card_level_id'), FILTER_SANITIZE_NUMBER_INT);


            //Validation rules

            if(!$first_name){ $data['errors']['first_name'] = 'El nombre debe ser obligatorio';}
            if(!$last_name){ $data['errors']['last_name'] = 'El apellido debe ser obligatorio';}
            if(!$email){ $data['errors']['email'] = 'El email debe ser obligatorio';}
            if($clientModel->checkEmail($email)){ $data['errors']['email'] = 'Este correo ya se encuentra asignado a otra cuenta'; }
            if(!$password){ $data['errors']['password'] = 'La contraseña debe ser obligatorio';}
            if(!$mobile){ $data['errors']['mobile'] = 'El telefono movil debe ser obligatorio';}
            if(!$gender){ $data['errors']['gender'] = 'Debe seleccionar un genero';}
            if(!$birthday){ $data['errors']['birthday'] = 'El cumpleaños debe ser obligatorio';}
            if(!$codigo_postal){ $data['errors']['codigo_postal'] = 'El codigo postal es un campo obligatorio';}
            if(!$estado){ $data['errors']['estado'] = 'El estado es un campo obligatorio';}
            if(!$municipio){ $data['errors']['municipio'] = 'El Municipio es un campo obligatorio';}
            if(!$colonia){ $data['errors']['colonia'] = 'La colonia es un campo obligatorio';}
            if(!$card_number){ $data['errors']['card_number'] = 'El numero de tarjeta es un campo obligatorio';}
            if(!$card_level_id){
                $cardInfo = $cardModel->getCardInfo($card_number);
                if(!$cardInfo){
                    $data['errors']['card_number'] = 'La tarjeta ingresada no se encuentra dada de alta, favor de verificar el numero';
                }
                if($cardInfo[0]['enabled'] == 1 AND $cardInfo[0]['client_id'] != NULL){
                    $data['errors']['card_number'] = 'La tarjeta ingresada ya se encuentra asignada a un usuario';
                }
            }
            

            if($data['errors']){
                $data['message'] = 'Error de validación de datos';
                $data['status'] = false;
                $data['code'] = 406;
            }else {

                try{

                //Encrypt the user password
                //Set data for new password in user Table
                $options = array(
                    'cost' => 12
                );
                $password_hashed = password_hash($password, PASSWORD_BCRYPT, $options);

                //Set client data to insert in the DB
                $clientDataTable = [
                    'first_name' => $first_name, 
                    'last_name' => $last_name,
                    'email' => $email,  
                    'password' => $password_hashed, 
                    'newsletter' => $newsletter, 
                    'enabled' => 1, 
                    'available' => 1, 
                ];
    
                $clientModel->insert($clientDataTable);
                $newUser = $clientModel->checkEmail($email);

                //Set client info data to insert in DB

                $infoClientDataTable = [
                    'email_alternative' => $email_alternative, 
                    'telephone' => $telephone,
                    'mobile' => $mobile,  
                    'gender' => $gender, 
                    'birthday' => $birthday, 
                    'street' => $street, 
                    'exterior' => $exterior,
                    'interior' => $interior, 
                    'codigo_postal' => $codigo_postal, 
                    'estado' => $estado, 
                    'municipio' => $municipio, 
                    'colonia' => $colonia, 
                    'client_id' => $newUser[0]['id'], 
                ];

                $infoClientModel->insert($infoClientDataTable);



                
                $today = date('Y-m-d H:i:s');


                // First Check the type of card
                if(!$card_level_id) {    //Physical Card

                    $cardDataTable = [
                        'enabled' => 1,
                        'assigned_at' => $today,
                        'card_type_id' => 1,
                        'card_status_id' => 1,
                        'client_id' => $newUser[0]['id'],
                        'card_reason_id' => 1
                    ];

                    $cardModel->update($cardInfo[0]['id'], $cardDataTable);

                    
                } else {    //Digital Card
                    $cardDataTable = [
                        'number' => $card_number,
                        'assigned_at' => $today,
                        'enabled' => 1,
                        'card_type_id' => 2,  
                        'card_status_id' => 1,
                        'card_level_id' => $card_level_id,
                        'client_id' => $newUser[0]['id'],
                        'card_reason_id' => 1
                    ];

                    $cardModel->insert($cardDataTable);

                }

                $pointsDataTable = [
                    'points_available' => 0,
                    'points_redeemed' => 0,
                    'client_id' => $newUser[0]['id']
                ];

                $pointsModel->insert($pointsDataTable);

                // Registrar Log sobre esta operación en particular en el sistema
                $this->db = db_connect('default');
                $dataLog = [
                    'client_id' => $newUser[0]['id'],
                    'user_id' => session()->get('user_id'),
                    'operation_id' => $this->db->table('operations')->where('name', 'Registrar Cliente')->get()->getRow()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                $this->db->table('users_clients_logs')->insert($dataLog);

                $data['status'] = $cardDataTable;
                $data['message'] = 'Registro de cliente exitoso';
                $data['code'] = 200;

                }catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            }
        }else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}