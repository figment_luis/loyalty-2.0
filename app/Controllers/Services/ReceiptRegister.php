<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class ReceiptRegister extends BaseController
{
    public function receiptRegister()
	{		
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $receiptsModel = model('App\Models\Services\ReceiptsModel');
            $receiptsLogsModel = model('App\Models\Services\ReceiptLogModel');
            $transactionModel = model('App\Models\Services\TransactionsModel');
            $cardModel = model('App\Models\Services\CardsModel');
            $pointsModel = model('App\Models\Services\PointsModel');


            //Extract Receipt Form Data
            $number = filter_var($this->request->getPost('number'), FILTER_SANITIZE_STRING);
            $amount = filter_var($this->request->getPost('amount'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $date = filter_var($this->request->getPost('date'), FILTER_SANITIZE_STRING);
            $store_id = filter_var($this->request->getPost('store_id'), FILTER_SANITIZE_STRING);

            //Extract Log Receipt Form Data
            $description = filter_var($this->request->getPost('description'), FILTER_SANITIZE_STRING);

            //Extract Log Receipt Form Data
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_NUMBER_INT);
            $card_id = filter_var($this->request->getPost('card_id'), FILTER_SANITIZE_NUMBER_INT);

            //Validation Rules
            if(!$number){ $data['errors']['number'] = 'El numero de tarjeta debe ser obligatorio';}
            if(!$amount){ $data['errors']['amount'] = 'El monto debe ser obligatorio';}
            if(!$date){ $data['errors']['date'] = 'La fecha debe ser obligatoria';}
            if(!$store_id){ $data['errors']['store_id'] = 'La tienda debe ser obligatorio';}
            if(!$client_id){ $data['errors']['client_id'] = 'El id del cliente debe ser obligatorio';}
            if(!$card_id){ $data['errors']['card_id'] = 'La tarjeta del cliente debe ser obligatorio';}


            //Variable encrypted = NUMBER + ID_TIENDA + MONTO + FECHA_HORA

            $formatDate = date_create($date);
            $encrypted = $number . '|' . $amount . '|' . date_format($formatDate, 'Y-m-d H:i') . '|' . $store_id;
            $encrypted = sha1($encrypted);

            if($receiptsModel->searchReceiptByEncrypted($encrypted)){
                $data['status'] = false;
                $data['message'] = 'No se ha ingresado el ticket, debido a que ya se había registrado previamente.';
                $data['code'] = 400;
                
                header("Content-type: application/json; charset=utf-8");
		        die(json_encode($data, JSON_UNESCAPED_UNICODE));
            }

            if($data['errors'] == []){
                try {
                    
                    $user_id = session()->get('user_id');

                    //Set Receipt Table Data
                    $receiptTableData = [
                        'number' => $number,
                        'amount' => $amount,
                        'date' => $date,
                        'store_id' => $store_id,
                        'enabled' => 1,
                        'encrypted' => $encrypted 
                    ];
    
                    $receiptsModel->insert($receiptTableData);

                    $receiptInfo = $receiptsModel->searchReceiptByEncrypted($encrypted);

                    //Set Receipt-User-Log Table Data
                    $receiptLogTableData = [
                        'receipt_id' => $receiptInfo[0]['id'],
                        'user_id' => $user_id,
                        'operation_id' => 2,
                    ];
                    $receiptsLogsModel->insert($receiptLogTableData);


                    //Calculate points

                    $cardInfo=$cardModel->getCardAndLevel($card_id);
                    $points = (floor(($amount/50))) * $cardInfo[0]['factor'];

                    //Set Transaction Table Data
                    $transactionTableData = [
                        'points' => $points,
                        'redeemed' => 0,
                        'total' => $points,
                        'card_id' => $card_id,
                        'client_id' => $client_id,
                        'receipt_id' => $receiptInfo[0]['id'],
                    ];
                    $transactionModel->insert($transactionTableData);

                    $pointsInfo = $pointsModel->getPoints($client_id);

                    $debt_points = $pointsInfo[0]['debt_points'];
                    $points_available = $pointsInfo[0]['points_available'] + $points;  //Puntos actuales mas los de la acreditacion

                    if($debt_points > 0){
                        if($debt_points > $points_available){
                            $pointsDataTable = [
                                'points_available' => 0,
                                'debt_points' => $debt_points - $points_available
                            ];
                        } else {
                            $pointsDataTable = [
                                'points_available' => $points_available - $debt_points,
                                'debt_points' => 0
                            ];
                        }
                    } else {
                        $pointsDataTable = [
                            'points_available' => $points_available,
                        ];
                    }
                    
    
                    $pointsModel->update($pointsInfo[0]['id'], $pointsDataTable);

                    $data['status'] = $points;
                    $data['message'] = 'Ticket registrado exitosamente';
                    $data['code'] = 200;
        
                } catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            } else {
                $data['status'] = false;
                $data['message'] = 'Error de validación de datos';
                $data['code'] = 406;
            }    
        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}
}