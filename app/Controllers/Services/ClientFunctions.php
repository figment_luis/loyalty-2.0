<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class ClientFunctions extends BaseController
{
	public function searchClient()
	{		
        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $clientModel = model('App\Models\Services\ClientModel');

            //Extract Form Data
            $query = filter_var($this->request->getPost('query'), FILTER_SANITIZE_STRING);
            $type = filter_var($this->request->getPost('type'), FILTER_SANITIZE_STRING);

            try {

                if($type == 'type_number' OR $type == 'email'){
                    $data['results'] = $clientModel->searchClient($query, $type);
                } else {
                    $data['results'] = $clientModel->searchClient($query, $type);
                }

                if($data['results']){
                    $data['status'] = true;
                    $data['message'] = 'Coincidencias encontradas';
                    $data['code'] = 200;
                } else {
                    $data['status'] = false;
                    $data['message'] = 'No se han encontrado resultados con la informacion proporcionada';
                    $data['code'] = 404;
                }

                


            } catch (\Exception $e){
                $data['status'] = false;
                $data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
            

        } else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}

    public function checkCardInfo(){
        if ($this->request->isAJAX()){

            //Load Models
            $cardModel = model('App\Models\Services\CardsModel');

            //Extract Form Data
            $card_number = filter_var($this->request->getPost('number'), FILTER_SANITIZE_STRING);
            $card_type = filter_var($this->request->getPost('card_type'), FILTER_SANITIZE_NUMBER_INT);


            try {
                $cardInfo = $cardModel->getCardInfo($card_number);

                if($card_type == 1 AND !$cardInfo){
                    $data['status'] = false;
                    $data['code'] = 404;
                    $data['message'] = 'La tarjeta ingresada no se encuentra dada de alta, favor de verificar el numero';

                    header("Content-type: application/json; charset=utf-8");
		            die(json_encode($data, JSON_UNESCAPED_UNICODE));
                }

                if(!$cardInfo){
                    $data['status'] = false;
                    $data['code'] = 404;
                    $data['message'] = 'La tarjeta ingresada no se encuentra dada de alta, favor de verificar el numero';
                } else if($cardInfo[0]['enabled'] == 1 ){
                    $data['status'] = false;
                    $data['code'] = 406;
                    $data['message']['card_number'] = 'La tarjeta ingresada ya se encuentra asignada a un usuario';
                } else {
                    $data['status'] = true;
                    $data['code'] = 200;
                    $data['message']['card_number'] = 'La tarjeta ingresada se encuentra disponible';
                }
            } catch (\Exception $e){
                $data['status'] = false;
                //$data['message'] = $e->getMessage();
                $data['code'] = 500;
            } 
        }

        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    } 
    
    public function getMunicipalitiesByState(){
        if ($this->request->isAJAX()){

            //Load Models
            $clientModel = model('App\Models\Services\ClientModel');

            //Extract Form Data
            $state = filter_var($this->request->getPost('state'), FILTER_SANITIZE_STRING);


            try {
                $states = $clientModel->searchMunicipalities($state);

                if(!$states){
                    $data['status'] = false;
                    $data['code'] = 404;
                    $data['message'] = 'El Estado ingresado no tiene asignado ningun municipio';
                } else {
                    $data['states'] = $states;
                    $data['status'] = true;
                    $data['code'] = 200;
                    $data['message'] = 'Solicitud exitosa';
                }
            } catch (\Exception $e){
                $data['status'] = false;
                //$data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        }

        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function getSuburbsByMunicipalities(){
        if ($this->request->isAJAX()){

            //Load Models
            $clientModel = model('App\Models\Services\ClientModel');

            //Extract Form Data
            $municipality = filter_var($this->request->getPost('municipality'), FILTER_SANITIZE_STRING);


            try {
                $states = $clientModel->searchSuburbs($municipality);

                if(!$states){
                    $data['status'] = false;
                    $data['code'] = 404;
                    $data['message'] = 'El Estado ingresado no tiene asignado ningun municipio';
                } else {
                    $data['states'] = $states;
                    $data['status'] = true;
                    $data['code'] = 200;
                    $data['message'] = 'Solicitud exitosa';
                }
            } catch (\Exception $e){
                $data['status'] = false;
                //$data['message'] = $e->getMessage();
                $data['code'] = 500;
            }
        }

        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function getCertificates(){

        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];

            //Load Models
            $clientModel = model('App\Models\Services\ClientModel');
            $pointsModel = model('App\Models\Services\PointsModel');
            $certificateModel = model('App\Models\Services\CertificatesModel');

            //Extract Form Data
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);

            if(!$client_id){ $data['errors']['client_id'] = 'El id es un campo obligatorio';}

            if($data['errors']){
                $data['message'] = 'Error de validación de datos';
                $data['status'] = false;
                $data['code'] = 406;
            }else {
                try {
                    $clientPoints = $pointsModel->getPoints($client_id)[0];

                    $certificates = $certificateModel->getCertificatesByPoints($clientPoints['points_available']);

    
                    if(!$clientPoints){
                        $data['status'] = false;
                        $data['code'] = 404;
                        $data['message'] = 'Usuario no encontrado';
                    }
                    else {
                        $data['certificates'] = $certificates;
                        $data['status'] = true;
                        $data['code'] = 200;
                        $data['message'] = 'Solicitud Exitosa';
                    }
                } catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            }
            header("Content-type: application/json; charset=utf-8");
		    die(json_encode($data, JSON_UNESCAPED_UNICODE));
        } 
    }

    public function getBenefits(){

        if ($this->request->isAJAX()){

            //Array errors
            $data['errors'] = [];

            //Load Models
            $clientModel = model('App\Models\Services\ClientModel');
            $cardModel = model('App\Models\Services\CardsModel');
            $benefitModel = model('App\Models\Services\BenefitsModel');

            //Extract Form Data
            $client_id = filter_var($this->request->getPost('client_id'), FILTER_SANITIZE_STRING);

            if(!$client_id){ $data['errors']['client_id'] = 'El id es un campo obligatorio';}

            if($data['errors']){
                $data['message'] = 'Error de validación de datos';
                $data['status'] = false;
                $data['code'] = 406;
            }else {
                try {
                    $cardInfo = $cardModel->getCardInfoByClientID($client_id)[0];
                   

                    $benefits= $benefitModel->getBenefitsByPoints($cardInfo['card_level_id']);

                    if(!$benefits){
                        $data['status'] = false;
                        $data['code'] = 400;
                        $data['message'] = 'No se han encontrado beneficios';
                    }
                    else {
                        $data['certificates'] = $benefits;
                        $data['status'] = true;
                        $data['code'] = 200;
                        $data['message'] = 'Solicitud Exitosa';
                    }

                   
                } catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            }
            header("Content-type: application/json; charset=utf-8");
		    die(json_encode($data, JSON_UNESCAPED_UNICODE));
        } 
    }
}