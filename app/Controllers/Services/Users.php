<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class Users extends BaseController
{
    public function userRegister(){

        if ($this->request->isAJAX()){

         //Array errors
         $data['errors'] = [];

        //Load Models
        $userModel = model('App\Models\Services\UserModel');

         //Extract Form Data

        //Client Table Data
        $first_name = filter_var($this->request->getPost('first_name'), FILTER_SANITIZE_STRING);
        $last_name = filter_var($this->request->getPost('last_name'), FILTER_SANITIZE_STRING);
        $email = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
        $password = filter_var($this->request->getPost('password'), FILTER_SANITIZE_STRING);
        $confirm_Password = filter_var($this->request->getPost('confirm_password'), FILTER_SANITIZE_STRING);


        //Validation rules
        if(!$first_name){ $data['errors']['first_name'] = 'El nombre debe ser obligatorio';}
        if(!$last_name){ $data['errors']['last_name'] = 'El apellido debe ser obligatorio';}
        if(!$email){ $data['errors']['email'] = 'El email debe ser obligatorio';}
        if($userModel->checkEmail($email)){ $data['errors']['email'] = 'Este correo ya se encuentra asignado a otra cuenta'; }
        if($password != $confirm_Password){ $data['errors']['password'] = 'Las contraseñas ingresadas no coinciden'; }
        if(!$password){ $data['errors']['password'] = 'La contraseña debe ser obligatorio';}


        if($data['errors']){
            $data['message'] = 'Error de validación de datos';
            $data['status'] = false;
            $data['code'] = 406;
        }else{

            try{

                //Encrypt the user password
                //Set data for new password in user Table
                $options = array(
                    'cost' => 12
                );
                $password_hashed = password_hash($password, PASSWORD_BCRYPT, $options);

                //Set user data to insert in the DB
                $userDataTable = [
                    'first_name' => $first_name, 
                    'last_name' => $last_name,
                    'email' => $email,  
                    'password' => $password_hashed, 
                    'enabled' => 1, 
                    'user_role_id' => 2,
                    'avatar' => 'assets/images/avatars/'.mt_rand(1, 10).'.jpg'
                ];
    
                $userModel->insert($userDataTable);

                // Registrar Log sobre esta operación en particular en el sistema
                $this->db = db_connect('default');
                $dataLog = [
                    'user_id' => $this->db->insertID(),
                    'admin_id' => session()->get('user_id'),
                    'operation_id' => $this->db->table('operations')->where('name', 'Registrar Concierge')->get()->getRow()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                $this->db->table('admin_users_logs')->insert($dataLog);
                
                $data['status'] = true;
                $data['message'] = 'Registro de cliente exitoso';
                $data['code'] = 200;

                }catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
        }


        }else {
            $data['status'] = false;
            $data['message'] = 'Ha ocurrido un error inesperado, favor de contactar con el administrador del sistema';
            $data['code'] = 404;
        }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}