<?php
if (!function_exists('active_link')) {
    function active_link(string $relativePath, bool $exact)
    {
        if ($exact)
            return (current_url() === base_url($relativePath)) ? 'active' : '';

        return url_is($relativePath) ? 'active' : '';
    }
}