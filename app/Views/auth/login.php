<?= $this->extend('layout/auth-layout') ?>

<?= $this->section('content') ?>
<div id="auth" class="login">

    <div class="row h-100">
        <div class="col-xl-5 col-12 d-flex justify-content-center align-items-center">
            <div id="auth-left" class="login-left">
                <div class="auth-logo d-xl-none text-center">
                    <a href="index.html"><img src="<?= base_url('assets/images/logotipo-ticket.svg') ?>" alt="Logotipo Loyalty 2.0" class="img-fluid"></a>
                </div>
                <h1 class="auth-title">Iniciar Sesión</h1>
                <p class="auth-subtitle mb-5">Ingrese sus credenciales de acceso.</p>
                <form action="<?= base_url('login-validation') ?>" method="post" id="formLogin">
                    <div class="form-group position-relative has-icon-left mb-4">
                        <input type="text" name="email" class="form-control form-control-xl"
                               data-required="El correo electrónico es un dato requerido"
                               id="email" placeholder="Correo electrónico" required>
                        <div class="form-control-icon">
                            <i class="bi bi-person"></i>
                        </div>
                    </div>
                    <div class="form-group position-relative has-icon-left mb-4">
                        <input type="password" name="password" class="form-control form-control-xl"
                               data-required="La contraseña es un dato requerido"
                               id="password" placeholder="Contraseña" required>
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg shadow-lg mt-5 position-relative" name="btnLogin">
                        <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader img-fluid" id="loader">
                        Entrar
                    </button>
                </form>
                <p id="login-messages" class="login-messages"></p>
            </div>
        </div>
        <div class="col-xl-7 d-none d-xl-block">
            <div id="auth-right" class="d-flex justify-content-center align-items-center login-right">
                <img src="<?= base_url('assets/images/logotipo.svg') ?>" alt="Logotipo Loyalty 2.0" class="logotipo img-fluid">
            </div>
        </div>
    </div>

</div>
<?= $this->endSection() ?>
