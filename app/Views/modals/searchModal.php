<div class="modal fade" id="searchModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-lg"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                        aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <form action="#" method="post" id="form">
                <div class="modal-body">
                    <div id="modalBodyForm">
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="type_number" name="type" id="rdoCardNumber" checked required>
                                <label class="form-check-label" for="rdoCardNumber">Tarjeta</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="name" name="type" id="rdoName" required>
                                <label class="form-check-label" for="rdoName">Nombre</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="email" name="type" id="rdoEmail" required>
                                <label class="form-check-label" for="rdoEmail">Email</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="telephone" name="type" id="rdoTelephone" required>
                                <label class="form-check-label" for="rdoTelephone">Teléfono</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="query" class="form-control form-control-lg" id="query" placeholder="Ingrese el término de búsqueda" required>
                        </div>
                    </div>
                    <div id="modalBodyResults">
                        <table class="table  modal-table" id="modalTableResults">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Teléfono</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="modalFooterForm" class="w-100">
                        <div class="d-flex justify-content-end align-items-center w-100">
                            <div id="search-messages" class="me-auto search-messages"></div>
                            <button type="button" class="btn btn-light-secondary"
                                    data-bs-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancelar</span>
                            </button>
                            <button type="submit" class="btn btn-primary ms-1" id="btnSearch">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Buscar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>