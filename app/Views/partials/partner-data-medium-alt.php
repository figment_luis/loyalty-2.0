<!--
<h3 class="mt-4">Puntos acumulados</h3>
<p class="text-subtitle">Información de puntos acumulados al <?= date('d-m-Y') ?>.</p>
<hr>
<div class="row">
    <div class="col-4">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-coins me-3"></i>
            <p class="m-0" id="info_points_available"><?= esc($points['points_available']) ?> Puntos disponibles</p>
        </div>
    </div>
    <div class="col-4">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-file-invoice-dollar me-4"></i>
            <p class="m-0" id="info_points_redeemed"><?= esc($points['points_redeemed']) ?> Puntos redimidos</p>
        </div>
    </div>
    <div class="col-4">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-clipboard-list me-4"></i>
            <p class="m-0" id="info_points_total"><?= esc($points['points_available']) + esc($points['points_redeemed']) ?> Puntos totales</p>
        </div>
    </div>
</div>
-->
<h3 class="mt-4">
    <?= $porcentaje == 100 ? 'El socio puede disfrutar de todos los beneficios' : 'Puntos faltantes para subir al siguiente nivel' ?>
</h3>
<div class="level-indicator"
     style="background-image: linear-gradient(90deg, var(--color-verde) <?= $porcentaje ?>%, var(--color-textos-4) <?= $porcentaje ?>%, var(--color-textos-4) 100%);">
    <?= $legend ?>
</div>
<div class="level-card d-flex justify-content-between">
    <?php foreach ($levels as $level):?>
        <div class="text-center">
            <p class="m-1"><strong>Nivel <?= esc($level->name) ?></strong></p>
            <p class="level-points"><i class="fas fa-coins me-1"></i> <?= number_format(esc($level->points), 0, '.', ',') ?> Puntos necesarios</p>
        </div>

    <?php endforeach; ?>
</div>