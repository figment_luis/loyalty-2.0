<h3>Datos del socio</h3>
<p class="text-subtitle">Información personal del socio seleccionado.</p>
<hr>
<div class="row">
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fa fa-user me-3"></i>
            <p class="m-0" id="info_client"><?= esc($client['name']) ?></p>
        </div>
    </div>
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-envelope me-3"></i>
            <p class="m-0" id="info_email"><?= esc($client['email']) ?></p>
        </div>
    </div>
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="far fa-credit-card me-3"></i>
            <p class="m-0" id="info_card"><?= esc($client['card_number']) ?></p>
        </div>
    </div>
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-layer-group me-3"></i>
            <p class="m-0" id="info_level">Nivel <?= esc($client['level']) ?> <?= esc($client['first_year']) ? '<span class="level-assign"> - Válido durante el primer año</span>' : ''?></p>
        </div>
    </div>
</div>
<h3 class="mt-4">Puntos acumulados</h3>
<p class="text-subtitle">Información de puntos acumulados al <?= date('d-m-Y') ?>.</p>
<hr>
<div class="row">
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-coins me-3"></i>
            <p class="m-0" id="info_points_available"><?= number_format(esc($points['points_available']), 0, '.', ',') ?> Puntos disponibles</p>
        </div>
    </div>
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-file-invoice-dollar me-4"></i>
            <p class="m-0" id="info_points_redeemed"><?= number_format(esc($points['points_redeemed']), 0, '.', ',') ?> Puntos redimidos</p>
        </div>
    </div>
    <div class="col-12">
        <div class="d-flex align-items-center form-data-user mb-2">
            <i class="fas fa-clipboard-list me-4"></i>
            <p class="m-0" id="info_points_total"><?= number_format(esc($points['points_available']) + esc($points['points_redeemed']), 0, '.', ',') ?> Puntos totales</p>
        </div>
    </div>
</div>