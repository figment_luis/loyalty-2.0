<div id="sidebar" class="active sidebar">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="<?= base_url('/') ?>"><img src="<?= base_url('assets/images/logotipo.svg') ?>" class="logotipo img-fluid" alt="Logotipo"></a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item  has-sub <?= active_link('socios/*', false) ?>">
                    <a href="#" class="sidebar-link">
                        <i class="fas fa-user-tie"></i>
                        <span>Socios</span>
                    </a>
                    <ul class="submenu ps-1 <?= active_link('socios/*', false) ?>">
                        <li class="submenu-item  <?= active_link('socios/panel-de-control', true) ?>">
                            <a href="<?= base_url('socios/panel-de-control') ?>">
                                <i class="fas fa-tachometer-alt me-1"></i>
                                <span>Panel de control</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('socios/acreditar-puntos/*', false) ?>">
                            <a href="#"
                               data-bs-title="Acreditar Puntos"
                               data-bs-url="client-search"
                               data-bs-redirect="socios/acreditar-puntos"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-tag me-1"></i>
                                <span>Acreditar puntos</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('socios/redenciones/*', false) ?>">
                            <a href="#"
                               data-bs-title="Redenciones"
                               data-bs-url="client-search"
                               data-bs-redirect="socios/redenciones"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-gift me-1"></i>
                                <span>Redenciones</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('socios/entrega-de-beneficios/*', false) ?>">
                            <a href="#"
                               data-bs-title="Entrega de Beneficios"
                               data-bs-url="client-search"
                               data-bs-redirect="socios/entrega-de-beneficios"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-trophy me-1"></i>
                                <span>Entregar beneficios</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('socios/estado-de-cuenta/*', false) ?>">
                            <a href="#"
                               data-bs-title="Consultar Estado de Cuenta"
                               data-bs-url="client-search"
                               data-bs-redirect="socios/estado-de-cuenta"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-user-cog"></i>
                                <span>Estado de cuenta</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('socios/catalogo-de-premios/*', false) ?>">
                            <a href="#"
                               data-bs-title="Catálogo de Certificados"
                               data-bs-url="client-search"
                               data-bs-redirect="socios/catalogo-de-premios"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-th-list me-1"></i>
                                <span>Catálogo de Certificados</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item  has-sub <?= active_link('clientes/*', false) ?>">
                    <a href="#" class="sidebar-link">
                        <i class="fas fa-users"></i>
                        <span>Nuevos Socios</span>
                    </a>
                    <ul class="submenu ps-1 <?= active_link('clientes/*', false) ?>">
                        <li class="submenu-item <?= active_link('clientes/registrar-usuario', true) ?>">
                            <a href="<?= base_url('clientes/registrar-usuario') ?>">
                                <i class="far fa-edit"></i>
                                <span>Registrar socio</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('clientes/tarjetas-y-beneficios', true) ?>">
                            <a href="<?= base_url('clientes/tarjetas-y-beneficios') ?>">
                                <i class="far fa-credit-card"></i>
                                <span>Información de tarjetas</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('clientes/listado-de-premios', true) ?>">
                            <a href="<?= base_url('clientes/listado-de-premios') ?>">
                                <i class="fas fa-th-list"></i>
                                <span>Listado de certificados</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('clientes/listado-de-beneficios', true) ?>">
                            <a href="<?= base_url('clientes/listado-de-beneficios') ?>">
                                <i class="fas fa-th-list"></i>
                                <span>Listado de beneficios</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if(session()->get('role') == 1 || session()->get('role') == 3): ?>
                    <li class="sidebar-item  has-sub <?= active_link('graficas/*', false) ?>">
                    <a href="#" class="sidebar-link">
                        <i class="fas fa-chart-line"></i>
                        <span>Reportes</span>
                    </a>
                    <ul class="submenu ps-1 <?= active_link('graficas/*', false) ?>">
                        <li class="submenu-item  <?= active_link('graficas/socios/*', false) ?>">
                            <a href="<?= base_url('graficas/socios/' . date('Y')) ?>">
                                <i class="fas fa-user-tag"></i>
                                <span>Socios</span>
                            </a>
                        </li>
                        <li class="submenu-item  <?= active_link('graficas/puntos/*', false) ?>">
                            <a href="<?= base_url('graficas/puntos/' . date('Y')) ?>">
                                <i class="fas fa-coins me-1"></i>
                                <span>Puntos</span>
                            </a>
                        </li>
                        <li class="submenu-item  <?= active_link('graficas/dinero/*', false) ?>">
                            <a href="<?= base_url('graficas/dinero/' . date('Y')) ?>">
                                <i class="fas fa-hand-holding-usd me-1"></i>
                                <span>Dinero</span>
                            </a>
                        </li>
                        <li class="submenu-item  <?= active_link('graficas/beneficios/*', false) ?>">
                            <a href="<?= base_url('graficas/beneficios/' . date('Y')) ?>">
                                <i class="fas fa-gifts me-1"></i>
                                <span>Beneficios</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
                <li class="sidebar-item  has-sub <?= active_link('operaciones/*', false) ?>">
                    <a href="#" class="sidebar-link">
                        <i class="fas fa-cogs"></i>
                        <span>Operaciones</span>
                    </a>
                    <ul class="submenu ps-1 <?= active_link('operaciones/*', false) ?>">
                        <li class="submenu-item <?= active_link('operaciones/cambiar-tarjeta/*', false) ?>">
                            <a href="#"
                               data-bs-title="Cambiar Tarjeta"
                               data-bs-url="client-search"
                               data-bs-redirect="operaciones/cambiar-tarjeta"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-sync-alt"></i>
                                <span>Cambiar tarjeta</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('operaciones/editar-socio/*', false) ?>">
                            <a href="#"
                               data-bs-title="Actualizar datos del Socio"
                               data-bs-url="client-search"
                               data-bs-redirect="operaciones/editar-socio"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-pencil-alt"></i>
                                <span>Actualizar socio</span>
                            </a>
                        </li>
                        <li class="submenu-item <?= active_link('operaciones/reimprimir-tickets/*', false) ?>">
                            <a href="#"
                               data-bs-title="Reimprimir Tickets"
                               data-bs-url="client-search"
                               data-bs-redirect="operaciones/reimprimir-tickets"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-print"></i>
                                <span>Reimprimir tickets</span>
                            </a>
                        </li>
                        <?php if(session()->get('role') == 1 || session()->get('role') == 3): ?>
                            <li class="submenu-item <?= active_link('operaciones/cancelaciones/*', false) ?>">
                            <a href="#"
                               data-bs-title="Cancelaciones"
                               data-bs-url="client-search"
                               data-bs-redirect="operaciones/cancelaciones"
                               data-bs-toggle="modal" data-bs-target="#searchModal">
                                <i class="fas fa-minus-circle"></i>
                                <span>Cancelaciones</span>
                            </a>
                        </li>
                            <li class="submenu-item <?= active_link('operaciones/registrar-concierge', false) ?>">
                                <a href="<?= base_url('operaciones/registrar-concierge') ?>">
                                    <i class="far fa-edit"></i>
                                    <span>Registrar concierge</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>