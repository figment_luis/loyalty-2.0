<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Reporteador de beneficios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-sm-6 col-lg-8">
                <h3>Reporte de Redenciones y entrega de Beneficios</h3>
                <p class="text-subtitle">Información detallada de los certificados y beneficios registrados en el plan de lealtad.</p>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="form-group">
                    <label for="selectorPeriodo">Cambiar periodo</label>
                    <select name="periodo" id="selectorPeriodo" class="form-select form-select-sm mb-2 mt-3">
                        <?php for($i = date('Y'); $i >= 2021; $i--): ?>
                        <option value="<?= $i ?>" <?= $i == $year ? 'selected': ''?>>Año <?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-6">
                <div class="card-info">
                    <div class="row">
                        <div class="chart-report">
                            <h5>Número de certificados redimidos en el año <?= $year ?></h5>
                            <div class="row">
                                <div class="col-12">
                                    <div id="graficaCertificadosRedimidosPorMes"></div>
                                </div>
                                <div class="col-12">
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="#" data-url="<?= base_url('reportes/certificados/periodo/'.$year) ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Reporte de redenciones año <?= $year ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card-info">
                    <div class="row">
                        <div class="chart-report">
                            <h5>Número de beneficios entregados en el año <?= $year ?></h5>
                            <div class="row">
                                <div class="col-12">
                                    <div id="graficaBeneficiosRedimidosPorMes"></div>
                                </div>
                                <div class="col-12">
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="#" data-url="<?= base_url('reportes/beneficios/periodo/'.$year) ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Reporte de beneficios entregados año <?= $year ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Listado de certificados registrados y redimidos</h5>
                    <table class="table table-striped" id="certificados-table">
                        <thead>
                        <tr>
                            <th width="300">Tienda</th>
                            <th>Nombre de Certificado</th>
                            <th width="140" class="text-center">Existencias</th>
                            <th width="140" class="text-center">Redimidos</th>
                            <th width="120" class="text-center">Estado</th>
                            <th width="120" class="text-center">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($certificados as $certificado): ?>
                            <tr>
                                <td><?= mb_convert_case(esc($certificado->subtitle), MB_CASE_TITLE, 'utf-8') ?></td>
                                <td><?= mb_convert_case(esc($certificado->title), MB_CASE_TITLE, 'utf-8') ?></td>
                                <td class="text-center"><?= $certificado->current_stock ?></td>
                                <td class="text-center"><?= $certificado->exchanges ?></td>
                                <td class="text-center"><?= $certificado->enabled ? 'Activo' : 'Inactivo' ?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('reportes/certificados/' . $certificado->id) ?>" class="btn btn-sm btn-primary">
                                        <i class="fas fa-cloud-download-alt me-1"></i> Reporte
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Listado de beneficios registrados y entregados</h5>
                    <table class="table table-striped" id="beneficios-table">
                        <thead>
                        <tr>
                            <th width="300">Titulo</th>
                            <th>Descripción del Beneficio</th>
                            <th width="140" class="text-center">Existencias</th>
                            <th width="140" class="text-center">Redimidos</th>
                            <th width="120" class="text-center">Estado</th>
                            <th width="120" class="text-center">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($beneficios as $beneficio): ?>
                            <tr>
                                <td><?= mb_convert_case(esc($beneficio->title), MB_CASE_TITLE, 'utf-8') ?></td>
                                <td><?= ucfirst(mb_strtolower(esc($beneficio->description))) ?></td>
                                <td class="text-center"><?= $beneficio->unlimited_stock ? 'Ilimitadas' : $beneficio->current_stock ?></td>
                                <td class="text-center"><?= $beneficio->amount ? $beneficio->amount : 0?></td>
                                <td class="text-center"><?= $beneficio->enabled ? 'Activo' : 'Inactivo' ?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('reportes/beneficios/' . $beneficio->id) ?>" class="btn btn-sm btn-primary">
                                        <i class="fas fa-cloud-download-alt me-1"></i> Reporte
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/vendors/simple-datatables/style.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/apexcharts/apexcharts.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?= base_url('assets/vendors/simple-datatables/simple-datatables.js') ?>"></script>
<script>
    let certificados_redimidos_por_mes = JSON.parse('<?= $data_certificados_redimidos_por_mes ?>')
    let beneficios_redimidos_por_mes = JSON.parse('<?= $data_beneficios_redimidos_por_mes ?>')
</script>
<script src="<?= base_url('assets/js/loyalty/charts/benefit.js') ?>"></script>
<?= $this->endSection() ?>

