<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Reporteador de dinero<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-sm-6 col-lg-8">
                <h3>Reporte de Dineros</h3>
                <p class="text-subtitle">Información detallada del dinero registrado y entregado en el plan de lealtad.</p>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="form-group">
                    <label for="selectorPeriodo">Cambiar periodo</label>
                    <select name="periodo" id="selectorPeriodo" class="form-select form-select-sm mb-2 mt-3">
                        <?php for($i = date('Y'); $i >= 2021; $i--): ?>
                            <option value="<?= $i ?>" <?= $i == $year ? 'selected': ''?>>Año <?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="fas fa-hand-holding-usd card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Dinero total ingresado por concepto de tickets registrados</h5>
                            <p class="card-info__subtitle">$ <?= number_format($dinero_total_en_tickets, '2', '.', ',') ?></p>
                        </div>
                        <div class="col-12">
                            <hr>
                            <a href="#" data-url="<?= base_url('reportes/dinero/tickets') ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Reporte general</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="fas fa-gift card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Dinero total entregado por concepto de redenciones</h5>
                            <p class="card-info__subtitle">$ <?= number_format($dinero_total_en_redenciones, '2', '.', ',') ?></p>
                        </div>
                        <div class="col-12">
                            <hr>
                            <a href="#" data-url="<?= base_url('reportes/dinero/redenciones') ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Reporte general</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="far fa-calendar-alt card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Seleccione el rango de fechas para generar los reportes</h5>
                            <input type="text" class="form-control form-control-sm d-block mb-2" name="daterange" />
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="card-report__link" style="margin-bottom: 1.2rem;">Generar reportes con base a la fecha seleccionada</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-6">
                <div class="chart-report">
                    <h5>Dinero ingresado en tickets por género - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="graficaDineroTicketsPorGenero"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($dinero_tickets_genero['dinero'] as $genero): ?>
                                <div class="inform d-flex align-items-center">
                                    <div class="inform__icon" >
                                        <i class="fas <?= $genero->gender == 'F' ? 'fa-female': 'fa-male' ?>" style="color: <?= $genero->gender == 'F' ? 'deeppink': 'deepskyblue' ?>"></i>
                                    </div>
                                    <div class="inform__description">
                                        <h5 class="inform__title">
                                            <?= $genero->gender == 'F' ? 'Mujeres': 'Hombres' ?>
                                            <!--a href="#" data-url="<?= base_url('reportes/dinero/tickets/generos/' . $genero->gender ) ?>" class="card-report__link link">
                                                <i class="fas fa-cloud-download-alt ms-1"></i>
                                            </a-->
                                        </h5>
                                        <p class="inform__subtitle">$ <?= number_format($genero->total, '2', '.', ',') ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/generos/F') ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Tickets mujeres</a>
                                </div>
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/generos/M') ?>" class="link btn btn-primary"><i class="fas fa-cloud-download-alt me-1"></i> Tickets hombres</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Género</th>
                                        <?php foreach ($dinero_tickets_genero['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dinero_tickets_genero['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $dinero_tickets_genero['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($dinero_tickets_genero['concentrado']['generos'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center">$<?= number_format($month['total'], 2, '.', ',') ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="chart-report">
                    <h5>Dinero entregado en redenciones por género - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="graficaDineroRedencionesPorGenero"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($dinero_redenciones_genero['dinero'] as $genero): ?>
                                <div class="inform d-flex align-items-center">
                                    <div class="inform__icon" >
                                        <i class="fas <?= $genero->gender == 'F' ? 'fa-female': 'fa-male' ?>" style="color: <?= $genero->gender == 'F' ? 'deeppink': 'deepskyblue' ?>"></i>
                                    </div>
                                    <div class="inform__description">
                                        <h5 class="inform__title">
                                            <?= $genero->gender == 'F' ? 'Mujeres': 'Hombres' ?>
                                            <!--a href="#" data-url="<?= base_url('reportes/socios/generos/' . $genero->gender) ?>" class="card-report__link link">
                                                <i class="fas fa-cloud-download-alt ms-1"></i>
                                            </a-->
                                        </h5>
                                        <p class="inform__subtitle">$ <?= number_format($genero->total, '2', '.', ',') ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/redenciones/genero/F') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Redenciones mujeres</a>
                                </div>
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/redenciones/genero/M') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Redenciones hombres</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Género</th>
                                        <?php foreach ($dinero_redenciones_genero['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dinero_redenciones_genero['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $dinero_redenciones_genero['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($dinero_redenciones_genero['concentrado']['generos'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center">$<?= number_format($month['total'], 2, '.', ',') ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-6">
                <div class="chart-report">
                    <h5>Dinero ingresado en tickets por nivel - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="graficaDineroTicketsPorNivel"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($dinero_tickets_nivel['dinero'] as $nivel): ?>
                            <div class="inform d-flex align-items-center">
                                <div class="inform__icon">
                                    <i class="fas fa-running"></i>
                                </div>
                                <div class="inform__description">
                                    <h5 class="inform__title">
                                        Socios nivel <?= $nivel->name ?>
                                        <!--a href="#" data-url="<?= base_url('reportes/dinero/tickets/niveles/' . $nivel->name) ?>" class="card-report__link link">
                                            <i class="fas fa-cloud-download-alt ms-1"></i>
                                        </a-->
                                    </h5>
                                    <p class="inform__subtitle">$ <?= number_format($nivel->total, '2','.',',') ?></p>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/niveles/Blue') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Tickets nivel Blue</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/niveles/Silver') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Tickets nivel Silver</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/niveles/Gold') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Tickets nivel Gold</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Género</th>
                                        <?php foreach ($dinero_tickets_nivel['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dinero_tickets_nivel['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $dinero_tickets_nivel['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($dinero_tickets_nivel['concentrado']['niveles'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center">$<?= number_format($month['total'], 2, '.', ',') ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="chart-report">
                    <h5>Dinero entregado en redenciones por nivel - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="graficaDineroRedencionesPorNivel"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($dinero_redenciones_nivel['dinero'] as $nivel): ?>
                                <div class="inform d-flex align-items-center">
                                    <div class="inform__icon">
                                        <i class="fas fa-running"></i>
                                    </div>
                                    <div class="inform__description">
                                        <h5 class="inform__title">
                                            Socios nivel <?= $nivel->name ?>
                                            <!--a href="#" data-url="<?= base_url('reportes/socios/nivel/') ?>" class="card-report__link link">
                                                <i class="fas fa-cloud-download-alt ms-1"></i>
                                            </a-->
                                        </h5>
                                        <p class="inform__subtitle">$ <?= number_format($nivel->total, '2','.',',') ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/redenciones/nivel/Blue') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Redenciones nivel Blue</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/redenciones/nivel/Silver') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Redenciones nivel Silver</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/redenciones/nivel/Gold') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Redenciones nivel Gold</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Género</th>
                                        <?php foreach ($dinero_redenciones_nivel['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dinero_redenciones_nivel['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $dinero_redenciones_nivel['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($dinero_redenciones_nivel['concentrado']['niveles'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center">$<?= number_format($month['total'], 2, '.', ',') ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Dinero ingresado en tickets por mes - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-12">
                            <div id="graficaDineroTicketsPorMes"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Dinero entregado en redenciones por mes - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-12">
                            <div id="graficaDineroRedencionesPorMes"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Dinero ingresado en tickets por rango de edad - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="graficaDineroTicketsPorRangoDeEdad"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <div class="row">
                                <?php foreach ($dinero_tickets_edad['dinero'] as $registro): ?>
                                        <div class="col-6">
                                            <div class="inform d-flex align-items-center">
                                                <div class="inform__icon">
                                                    <i class="fas fa-running"></i>
                                                </div>
                                                <div class="inform__description">
                                                    <h5 class="inform__title">
                                                        De <?= $registro->rango ?> años
                                                        <!--a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/' . $registro->rango) ?>" class="card-report__link link">
                                                            <i class="fas fa-cloud-download-alt ms-1"></i>
                                                        </a-->
                                                    </h5>
                                                    <p class="inform__subtitle">$ <?= number_format($registro->total,'2','.',',') ?></p>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/18 a 25') ?>" class="btn btn-primary link mb-3"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 18 a 25 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/26 a 35') ?>" class="btn btn-primary link mb-3"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 26 a 35 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/36 a 45') ?>" class="btn btn-primary link mb-3"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 36 a 45 años de edad </a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/46 a 55') ?>" class="btn btn-primary link mb-3"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 46 a 55 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/56 a 65') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 56 a 65 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/dinero/tickets/rango-edad/66 a 99') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Tickets de 66 a 99 años de edad</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Rango de Edad</th>
                                        <?php foreach ($dinero_tickets_edad['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dinero_tickets_edad['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $dinero_tickets_edad['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($dinero_tickets_edad['concentrado']['edades'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center">$<?= number_format($month['total'], 2, '.', ',') ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/apexcharts/apexcharts.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.js') ?>"></script>
<script>
    let dinero_tickets_por_genero = JSON.parse('<?= $porcentaje_dinero_tickets_genero ?>')
    let labels_dinero_tickets_genero = JSON.parse('<?= $labels_dinero_tickets_genero ?>')
    let dinero_redenciones_por_genero = JSON.parse('<?= $porcentaje_dinero_redenciones_genero ?>')
    let labels_dinero_redenciones_genero = JSON.parse('<?= $labels_dinero_redenciones_genero ?>')
    let dinero_tickets_por_nivel = JSON.parse('<?= $porcentaje_dinero_tickets_nivel ?>')
    let labels_dinero_tickets_nivel = JSON.parse('<?= $labels_dinero_tickets_nivel ?>')
    let dinero_redenciones_por_nivel = JSON.parse('<?= $porcentaje_dinero_redenciones_nivel ?>')
    let labels_dinero_redenciones_nivel = JSON.parse('<?= $labels_dinero_redenciones_nivel ?>')
    let dinero_tickets_por_mes = JSON.parse('<?= $data_dinero_tickets_mes ?>')
    let dinero_redenciones_por_mes = JSON.parse('<?=$data_dinero_redenciones_mes ?>')
    let dinero_tickets_por_rango_de_edad = JSON.parse('<?= $porcentaje_dinero_tickets_edad ?>')

</script>
<script src="<?= base_url('assets/js/loyalty/charts/money.js') ?>"></script>
<?= $this->endSection() ?>


