<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Reporteador de socios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-sm-6 col-lg-8">
                <h3>Reporte de Socios</h3>
                <p class="text-subtitle">Información detallada de los socios registrados en el plan de lealtad.</p>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="form-group">
                    <label for="selectorPeriodo">Cambiar periodo</label>
                    <select name="periodo" id="selectorPeriodo" class="form-select form-select-sm mb-2 mt-3">
                        <?php for($i = date('Y'); $i >= 2021; $i--): ?>
                        <option value="<?= $i ?>" <?= $i == $year ? 'selected': ''?>>Año <?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="fas fa-users card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Número de socios totales registrados en el Plan de Lealtad</h5>
                            <p class="card-info__subtitle"><?= $socios_totales ?></p>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <a href="#" data-url="<?= base_url('reportes/socios') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Reporte</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="fas fa-shopping-cart card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Informe general de compras totales por tienda</h5>

                            <select name="tienda" id="selectorTienda" class="form-select form-select-sm mb-2 mt-3">
                                <option value="" selected disabled>Seleccione una tienda</option>
                                <?php foreach ($tiendas as $tienda): ?>
                                <option value="<?= esc($tienda->id) ?>"><?= esc($tienda->name) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <a href="#" data-url="<?= base_url('reportes/socios/tienda') ?>" data-select="tienda" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Reporte</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card-info">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center">
                            <i class="far fa-calendar-alt card-info__logo"></i>
                        </div>
                        <div class="col-8">
                            <h5 class="card-info__title">Seleccione el rango de fechas para generar los reportes</h5>
                            <input type="text" class="form-control form-control-sm d-block mb-2" name="daterange" />
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="card-report__link" style="margin-bottom: 1.2rem;">Generar reportes con base a la fecha seleccionada</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6">
                <div class="chart-report">
                    <h5>Socios registrados por género - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="chart-visitors-profile"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($socios_genero['clientes'] as $genero): ?>
                                <div class="inform d-flex align-items-center">
                                    <div class="inform__icon" >
                                        <i class="fas <?= $genero->gender == 'F' ? 'fa-female': 'fa-male' ?>" style="color: <?= $genero->gender == 'F' ? 'deeppink': 'deepskyblue' ?>"></i>
                                    </div>
                                    <div class="inform__description">
                                        <h5 class="inform__title">

                                            <?= $genero->gender == 'F' ? 'Mujeres registradas': 'Hombres registrados' ?>
                                            <!--a href="#" data-url="<?= base_url('reportes/socios/genero/' . $genero->gender) ?>" class="card-report__link link">
                                                <i class="fas fa-cloud-download-alt ms-1"></i>
                                            </a-->
                                        </h5>
                                        <p class="inform__subtitle"><?= $genero->total ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/socios/genero/F') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Reporte mujeres</a>
                                </div>
                                <div class="col-6">
                                    <a href="#" data-url="<?= base_url('reportes/socios/genero/M') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Reporte hombres</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Género</th>
                                        <?php foreach ($socios_genero['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($socios_genero['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $socios_genero['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($socios_genero['concentrado']['generos'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center"><?= $month['total'] ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12" style="margin-top: 2.5rem;"></div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="chart-report">
                    <h5>Socios registrados por nivel - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="chart-levels"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <?php foreach ($socios_nivel['clientes'] as $nivel): ?>
                                <div class="inform d-flex align-items-center">
                                    <div class="inform__icon">
                                        <i class="fas fa-running"></i>
                                    </div>
                                    <div class="inform__description">
                                        <h5 class="inform__title">
                                             Nivel <?= strtolower($nivel->name) ?>
                                            <!--a href="#" data-url="<?= base_url('reportes/socios/nivel/' . $nivel->name) ?>" class="card-report__link link">
                                                <i class="fas fa-cloud-download-alt ms-1"></i>
                                            </a-->
                                        </h5>
                                        <p class="inform__subtitle">
                                            <?= $nivel->total ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/socios/nivel/Blue') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Socios nivel Blue</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/socios/nivel/Gold') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Socios nivel Gold</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" data-url="<?= base_url('reportes/socios/nivel/Silver') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Socios nivel Silver</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Nivel</th>
                                        <?php foreach ($socios_nivel['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($socios_nivel['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $socios_nivel['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($socios_nivel['concentrado']['niveles'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center"><?= $month['total'] ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="chart-report">
                    <h5>Socios registrados por rango de edad - Año <?= $year ?></h5>
                    <div class="row">
                        <div class="col-6">
                            <div id="chart-edades"></div>
                        </div>
                        <div class="col-6 d-flex justify-content-center flex-column">
                            <div class="row">
                                <?php foreach ($socios_edad['clientes'] as $clase): ?>
                                    <?php foreach ($clase as $clave => $valor): ?>
                                        <div class="col-6">
                                            <div class="inform d-flex align-items-center">
                                        <div class="inform__icon">
                                            <i class="fas fa-running"></i>
                                        </div>
                                        <div class="inform__description">
                                            <h5 class="inform__title">
                                                De <?= str_replace('-', ' a ', $clave) ?> años
                                                <!--a href="#" data-url="<?= base_url('reportes/socios/rangoedad/' . $clave) ?>" class="card-report__link link">
                                                    <i class="fas fa-cloud-download-alt ms-1"></i>
                                                </a-->
                                            </h5>
                                            <p class="inform__subtitle">
                                                <?= $valor ?></p>
                                        </div>
                                    </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="row text-center">
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/18-29') ?>" class="btn btn-primary mb-3 link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 18 a 29 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/30-39') ?>" class="btn btn-primary mb-3 link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 30 a 39 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/40-49') ?>" class="btn btn-primary mb-3 link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 40 a 49 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/50-59') ?>" class="btn btn-primary mb-3 link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 50 a 59 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/60-69') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 60 a 69 años de edad</a>
                                </div>
                                <div class="col-3">
                                    <a href="#" data-url="<?= base_url('reportes/socios/rangoedad/70-79') ?>" class="btn btn-primary link"><i class="fas fa-cloud-download-alt me-1"></i> Descargar socios de 70 a 79 años de edad</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4">
                            <h6 class="my-3">Tabla de concentrados</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Rango de Edad</th>
                                        <?php foreach ($socios_edad['concentrado']['meses'] as $mes): ?>
                                            <th><?= $mes ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($socios_edad['concentrado']['periodo'] as $index => $periodo): ?>
                                        <tr>
                                            <th>
                                                <span class="badge <?= $socios_edad['concentrado']['backgrounds'][$index] ?>">
                                                    <?= mb_strtoupper($socios_edad['concentrado']['edades'][$index]); ?>
                                                    <span>
                                            </th>
                                            <?php foreach ($periodo as $month): ?>
                                                <td class="text-center"><?= $month['total'] ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6">
                <div class="chart-report">
                    <h5>Socios registrados por región - Año <?= $year ?></h5>
                    <div id="chart-regiones"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="chart-report">
                    <h5>Socios registrados por mes - Año <?= $year ?></h5>
                    <div id="chart-meses"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/apexcharts/apexcharts.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.js') ?>"></script>
<script>
    let ser = '<?= $porcentaje_socios_genero ?>'
    let labels_socios_genero = '<?= $labels_socios_genero ?>'
    let niveles = '<?= $porcentaje_socios_nivel ?>'
    let labels_socios_nivel = '<?= $labels_socios_nivel ?>'
    let edades = '<?= $porcentaje_socios_edad ?>'
    let labels_socios_edad = '<?= $labels_socios_edad ?>'
    let regiones = JSON.parse('<?= $data_socios_region ?>')
    let meses = JSON.parse('<?= $data_socios_mes ?>')
</script>
<script src="<?= base_url('assets/js/loyalty/charts/partner.js') ?>"></script>
<?= $this->endSection() ?>

