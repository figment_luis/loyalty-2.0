<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Reporteador de puntos<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-sm-6 col-lg-8">
                <h3>Reporte de Puntos</h3>
                <p class="text-subtitle">Información detallada de los puntos disponibles, redimidos, expirados en el plan de lealtad.</p>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="form-group">
                    <label for="selectorPeriodo">Cambiar periodo</label>
                    <select name="periodo" id="selectorPeriodo" class="form-select form-select-sm mb-2 mt-3">
                        <?php for($i = date('Y'); $i >= 2021; $i--): ?>
                            <option value="<?= $i ?>" <?= $i == $year ? 'selected': ''?>>Año <?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col-6">
                        <div class="card-info">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-hand-holding-usd card-info__logo"></i>
                                </div>
                                <div class="col-8">
                                    <h5 class="card-info__title">Puntos disponibles en el plan de lealtad</h5>
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="card-info__subtitle"><?= number_format($puntos_disponibles, '0', '.', ',') ?></p>
                                        </div>
                                        <div class="col-6 ">
                                            <a href="<?= base_url('reportes/puntos') ?>" class="btn btn-primary btn-sm float-end"><i class="fas fa-cloud-download-alt me-1"></i> Reporte</a>
                                        </div>
                                    </div>
                                </div>
                                <!--div class="col-12">
                                    <hr>
                                    <p style="height: 22px;">Información a partir de la última fecha de corte</p>
                                </div-->
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card-info">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-gift card-info__logo"></i>
                                </div>
                                <div class="col-8">
                                    <h5 class="card-info__title">Puntos redimidos en el plan de lealtad</h5>
                                    <p class="card-info__subtitle"><?= number_format($puntos_redimidos, '0', '.', ',') ?></p>
                                </div>
                                <!--div class="col-12">
                                    <hr>
                                    <p style="height: 22px;">Información a partir de la última fecha de corte</p>
                                </div-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="chart-report">
                            <h5>Total puntos acreditados - Año <?= $year ?></h5>
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <?php foreach ($concentrado_puntos_disponibles['meses'] as $mes): ?>
                                                    <th class="text-center"><?= $mes ?></th>
                                                <?php endforeach; ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php foreach ($concentrado_puntos_disponibles['periodo'] as $periodo): ?>
                                                    <?php foreach ($periodo as $mes): ?>
                                                        <td class="text-center"><?= number_format($mes['total'], 0, '.', ',') ?></td>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="chart-report">
                            <h5>Total puntos redimidos - Año <?= $year ?></h5>
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <?php foreach ($concentrado_puntos_redimidos['meses'] as $mes): ?>
                                                    <th class="text-center"><?= $mes ?></th>
                                                <?php endforeach; ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php foreach ($concentrado_puntos_redimidos['periodo'] as $periodo): ?>
                                                    <?php foreach ($periodo as $mes): ?>
                                                        <td class="text-center">
                                                            <?php if($mes['total'] < 0): ?>
                                                                <span class="badge rounded-pill bg-danger" data-bs-toggle="tooltip" data-bs-html="true" title="Se cancelaron redenciones por <?= number_format($mes['total'] * -1, 0, '.', ',') ?> puntos" style="cursor: pointer"><?= number_format($mes['total'], 0, '.', ',') ?></span>
                                                            <?php else: ?>
                                                                <?= number_format($mes['total'], 0, '.', ',') ?>
                                                            <?php endif; ?>
                                                        </td>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="chart-report">
                    <h5>Historico</h5>
                    <p>Seleccione un rango de fechas para generar el reporte histórico de puntos</p>
                    <div class="row" id="selector_fecha" style="display: none">
                        <div class="col">
                            <label for="start_date">Fecha de inicio</label>
                            <select name="start_date" id="start_date" class="form-select form-select-sm w-100"></select>
                        </div>
                        <div class="col">
                            <label for="end_date">Fecha de corte</label>
                            <select name="end_date" id="end_date" class="form-select form-select-sm w-100"></select>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-sm my-3" id="generar-historico"><i class="fas fa-cogs me-1"></i> Generar</button>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6" id="info-start-date"></div>
                        <div class="col-6" id="info-end-date"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 historico" style="display: none;">
            <div class="col-12">
                <div class="chart-report">
                    <h5 class="historico-inicio">Concentrado de Puntos por Cliente</h5>
                    <div class="row"></div>
                    <table class="table table-striped" id="puntos-inicio-table"></table>
                </div>
            </div>
            <div class="mt-4 col-12">
                <div class="chart-report">
                    <h5 class="historico-fin">Concentrado de Puntos por Cliente</h5>
                    <div class="row"></div>
                    <table class="table table-striped" id="puntos-fin-table"></table>
                </div>
            </div>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/vendors/simple-datatables/style.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/apexcharts/apexcharts.js') ?>"></script>
<script src="<?= base_url('assets/vendors/simple-datatables/simple-datatables.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/daterangepicker/daterangepicker.js') ?>"></script>
<script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>
<script src="<?= base_url('assets/js/loyalty/charts/point.js') ?>"></script>
<?= $this->endSection() ?>



