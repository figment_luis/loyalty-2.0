<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Estado de cuenta<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-9">
                <?= $this->include('partials/partner-data-full-alt') ?>
            </div>
            <div class="col-3">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= base_url('/operaciones/editar-socio/' . $client['client_id'] ) ?>"">
                            <div class="card card-dashboard overflow-hidden">
                                <div class="card-body p-4">
                                    <h6>Actualizar Perfil</h6>
                                    <p class="mb-0">Actualizar los datos de este socio en el plan de lealtad.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="<?= base_url('/operaciones/cambiar-tarjeta/' . $client['client_id'] ) ?>">
                            <div class="card card-dashboard overflow-hidden">
                                <div class="card-body p-4">
                                    <h6>Cambiar Tarjeta</h6>
                                    <p class="mb-0">Cambiar la tarjeta de este socio en el plan de lealtad.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-4">Historial</h3>
                <div class="list-group list-group-horizontal-sm mb-1 text-center history-table-nav" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-transacciones" data-bs-toggle="list" href="#transacciones" role="tab" aria-selected="true">Transacciones</a>
                    <a class="list-group-item list-group-item-action" id="list-monday-list" data-bs-toggle="list" href="#puntos-expirados" role="tab" aria-selected="false">Puntos expirados</a>
                    <a class="list-group-item list-group-item-action" id="list-tuesday-list" data-bs-toggle="list" href="#tarjetas" role="tab" aria-selected="false">Tarjetas</a>
                    <a class="list-group-item list-group-item-action" id="list-tuesday-list" data-bs-toggle="list" href="#beneficios" role="tab" aria-selected="false">Beneficios</a>
                    <a class="list-group-item list-group-item-action" id="list-tuesday-list" data-bs-toggle="list" href="#redenciones" role="tab" aria-selected="false">Redenciones</a>
                </div>
                <div class="tab-content text-justify history-table-container">
                    <div class="tab-pane fade active show" id="transacciones" role="tabpanel" aria-labelledby="list-sunday-list">
                        <table class="table table-striped history-table" id="transacciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Tienda</th>
                                <th>Ticket</th>
                                <th>Monto de Ticket</th>
                                <th>Puntos Acreditados</th>
                                <th>Puntos Redimidos</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($transactions as $transaction): ?>
                            <tr>
                                <td><?= esc($transaction->date) ?></td>
                                <td><?= esc($transaction->card_number) ?></td>
                                <td><?= esc($transaction->store) ?></td>
                                <td><?= esc($transaction->ticket) ?></td>
                                <td>$ <?= number_format(esc($transaction->amount), 2, '.', ',') ?></td>
                                <td><?= number_format(esc($transaction->points),0,'.',',') ?></td>
                                <td><?= number_format(esc($transaction->redeemed),0,'.',',') ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="puntos-expirados" role="tabpanel" aria-labelledby="list-monday-list">
                        <table class="table table-striped history-table" id="puntos-expirados-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Puntos Expirados</th>
                                <th>Puntos Redimidos</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($expirePoints as $expire): ?>
                                <tr>
                                    <td><?= $expire['created_at'] ?></td>
                                    <td><?= $expire['points_expired'] ?></td>
                                    <td><?= $expire['points_redeemed'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tarjetas" role="tabpanel" aria-labelledby="list-tuesday-list">
                        <table class="table table-striped history-table" id="tarjetas-table">
                            <thead>
                            <tr>
                                <th>Fecha de Asignación</th>
                                <th>Número de Tarjeta</th>
                                <th>Tipo</th>
                                <th>Nivel</th>
                                <th>Estado Actual</th>
                                <th>Razón de Cámbio</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($cards as $card): ?>
                                <tr>
                                    <td><?= $card->date ?></td>
                                    <td><?= $card->card_number ?></td>
                                    <td><?= $card->type ?></td>
                                    <td><?= $card->level ?></td>
                                    <td><?= $card->status ?></td>
                                    <td><?= $card->reason ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="beneficios" role="tabpanel" aria-labelledby="list-tuesday-list">
                        <table class="table table-striped history-table" id="beneficios-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Beneficio Canjeado</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($benefits as $benefit): ?>
                                <tr>
                                    <td><?= $benefit['created_at'] ?></td>
                                    <td><?= $benefit['number'] ?></td>
                                    <td><?= $benefit['title'] ?></td>
                                    <td><?= $benefit['amount'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="redenciones" role="tabpanel" aria-labelledby="list-tuesday-list">
                        <table class="table table-striped history-table" id="redenciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Certificado Canjeado</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($certificates as $certificate): ?>
                                <tr>
                                    <td><?= $certificate['created_at'] ?></td>
                                    <td><?= $certificate['number'] ?></td>
                                    <td><?= $certificate['title'] ?></td>
                                    <td><?= $certificate['amount'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/vendors/simple-datatables/style.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/simple-datatables/simple-datatables.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/estado-de-cuenta.js') ?>"></script>
<?= $this->endSection() ?>


