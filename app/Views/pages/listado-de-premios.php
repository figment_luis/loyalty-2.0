<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Listado de certificados<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <?php if ($certificates): ?>
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12 order-md-1 order-last">
                <h3>Listado de Certificados</h3>
                <p class="text-subtitle">Se muestran las recompensas que un socio puede redimir con base en sus puntos disponibles.</p>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <section class="section">
        <div class="row">
            <?php if ($certificates): ?>
                <?php foreach ($certificates as $certificate): ?>
                    <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
                        <div class="certificate">
                            <img src="<?= base_url(esc($certificate->image)) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($certificate->title)) ?>" title="<?= strtoupper(esc($certificate->title)) ?>">
                            <h5 class="certificate__title"><?= esc($certificate->title) ?></h5>
                            <h6 class="certificate__subtitle"><?= esc($certificate->subtitle) ?></h6>
                            <ul class="p-0 list-unstyled certificate__list">
                                <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= number_format(esc($certificate->points),0,'.', ',') ?></li>
                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($certificate->current_stock) ?></li>
                            </ul>
                            <p class="certificate__description"><?= esc($certificate->description) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-12 text-center">
                    <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                    <h2 class="listado-premios__title-error">Upss!</h2>
                    <p class="listado-premios__message">Coméntale al cliente que todos los certificados han volado, son muchas las recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                        <br>Pero este suceso no debe preocuparle. <strong>Mañana habrá mas certificados para canjerar.</strong></p>
                </div>
            <?php endif; ?>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<?= $this->endSection() ?>
