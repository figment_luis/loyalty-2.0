<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Reimprimir tickets<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-7">
                <h3>Reimprimir Tickets</h3>
                <p class="mb-4">Seleccione el tipo de ticket que desea reimprimir</p>
                <div class="list-group list-group-horizontal-sm mb-1 text-center history-table-nav" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-redenciones" data-bs-toggle="list" href="#redenciones" role="tab" aria-selected="true">Tickets por entrega de certificados</a>
                    <a class="list-group-item list-group-item-action" id="list-beneficios" data-bs-toggle="list" href="#beneficios" role="tab" aria-selected="false">Ticket por entrega de beneficios</a>
                    <a class="list-group-item list-group-item-action" id="list-transacciones" data-bs-toggle="list" href="#transacciones" role="tab" aria-selected="false">Ticket por acreditación de puntos</a>
                </div>
                <div class="tab-content text-justify history-table-container">
                    <div class="tab-pane fade active show" id="redenciones" role="tabpanel" aria-labelledby="list-sunday-list">
                        <table class="table table-striped history-table" id="redenciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Concierge</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tickets_redemptions_certificates as $trc): ?>
                                <tr>
                                    <td><?= $trc->created_at ?></td>
                                    <td><?= $trc->card_number ?></td>
                                    <td><?= $trc->concierge ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnReimprimirTicketCertificado"
                                                data-ticket="<?= $trc->id ?>"
                                                data-client="<?= $trc->client ?>"
                                                data-card_number="<?= $trc->card_number?>"
                                                data-points_redeemed="<?=$trc->points_redeemed ?>"
                                                data-old_date="<?= $trc->created_at ?>">
                                            Reimprimir
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="beneficios" role="tabpanel" aria-labelledby="list-tuesday-list">
                        <table class="table table-striped history-table" id="beneficios-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Concierge</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tickets_redemptions_benefits as $trb): ?>
                                <tr>
                                    <td><?= $trb->created_at ?></td>
                                    <td><?= $trb->card_number ?></td>
                                    <td><?= $trb->concierge ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnReimprimirTicketBeneficio"
                                                data-ticket="<?= $trb->id ?>"
                                                data-client="<?= $trb->client ?>"
                                                data-card_number="<?= $trb->card_number?>"
                                                data-old_date="<?= $trb->created_at ?>">
                                            Reimprimir
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="transacciones" role="tabpanel" aria-labelledby="list-sunday-list">
                        <table class="table table-striped history-table" id="transacciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Número de Tarjeta</th>
                                <th>Concierge</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tickets_redemptions_transactions as $trt): ?>
                                <tr>
                                    <td><?= $trt->created_at ?></td>
                                    <td><?= $trt->card_number ?></td>
                                    <td><?= $trt->concierge ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnReimprimirTicketAcreditacionPuntos"
                                                data-ticket="<?= $trt->id ?>"
                                                data-client="<?= $trt->client ?>"
                                                data-card_number="<?= $trt->card_number?>"
                                                data-credited_points="<?=$trt->credited_points ?>"
                                                data-level="<?=$trt->level ?>"
                                                data-points_at_that_time="<?=$trt->points_at_that_time ?>"
                                                data-old_date="<?= $trt->created_at ?>">
                                            Reimprimir
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-4 offset-1">
                <?= $this->include('partials/partner-data-full') ?>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/vendors/simple-datatables/style.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/simple-datatables/simple-datatables.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/reimprimir-tickets.js') ?>"></script>
<?= $this->endSection() ?>

