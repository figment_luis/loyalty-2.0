<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Cambiar tarjeta<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="page-heading flex-grow-1">
        <div class="page-title mb-4">
            <div class="row">
                <div class="col-7">
                    <form action="<?= base_url('cambiar-tarjeta') ?>" method="post" class="form-registrarUsuario" id="formCambiarTarjeta">
                        <h3>Cambiar Tarjeta</h3>
                        <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="client_id" class="form-control form-control-lg" id="client_id" value="<?= $client['client_id'] ?>" readonly>
                                <input type="hidden" name="card_id" class="form-control form-control-lg" id="card_id" value="<?= $client['card_id'] ?>" readonly>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="slcReason">Razón del Cambio <span class="required">*</span></label>
                                    <select name="reason" class="form-select form-select-lg"
                                            data-required="La razón de cambio es un dato requerido"
                                            id="slcReason" required>
                                        <option value="" selected disabled>Seleccione una razón</option>
                                        <!-- todo: Llenar esta sección con datos provenientes de la DB -->
                                        <?php foreach ($reasons as $reason): ?>
                                            <option value="<?= $reason->id ?>"><?= $reason->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="card_number_current">Tarjeta Actual</label>
                                    <input type="text"
                                           name="title"
                                           class="form-control form-control-lg"
                                           value="<?= $client['card_number'] ?>"
                                           id="card_number_current"
                                           disabled>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="card_number">Tarjeta Nueva <span class="required">*</span></label>
                                    <input type="text"
                                           name="card_number"
                                           class="form-control form-control-lg"
                                           data-required="El nuevo número de tarjeta es un dato requerido"
                                           data-toolong="El nuevo número de tarjeta no debe exceder los 15 digitos"
                                           data-toshort="El nuevo número de tarjeta debe ser de al menos 15 digitos"
                                           data-pattern="El nuevo número de tarjeta no parece ser un dato válido"
                                           id="card_number"
                                           placeholder="Ingrese el número de tarjeta"
                                           pattern="[0-9A-Z-]{15}"
                                           maxlength="15"
                                           minlength="15"
                                           required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>¿Desea asignar una Tarjeta Digital?</label>
                                    <button class="btn btn-primary btn-lg w-100" id="btnCardDigitalNumber">Generar número de tarjeta</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-5">
                                <button type="reset" class="btn btn-lg btn-secondary me-3 " id="btnCancel">Cancelar</button>
                                <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit position-relative" id="btnSubmit">
                                    <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-4 offset-1">
                    <?= $this->include('partials/partner-data-full') ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
    <script src="<?= base_url('assets/js/libs/cleave/cleave.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/loyalty/cambiar-tarjeta.js') ?>"></script>
<?= $this->endSection() ?>