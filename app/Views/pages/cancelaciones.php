<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Cancelaciones<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-7">
                <h3>Cancelaciones</h3>
                <p class="mb-4">Seleccione el tipo de operación que desea cancelar</p>
                <div class="list-group list-group-horizontal-sm mb-1 text-center history-table-nav" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-acreditaciones" data-bs-toggle="list" href="#acreditaciones" role="tab" aria-selected="true">Acreditaciones (Tickets)</a>
                    <a class="list-group-item list-group-item-action" id="list-redenciones" data-bs-toggle="list" href="#redenciones" role="tab" aria-selected="false">Redenciones</a>
                    <a class="list-group-item list-group-item-action" id="list-beneficios" data-bs-toggle="list" href="#beneficios" role="tab" aria-selected="false">Beneficios</a>
                </div>
                <div class="tab-content text-justify history-table-container">
                    <div class="tab-pane fade active show" id="acreditaciones" role="tabpanel" aria-labelledby="list-sunday-list">
                        <table class="table table-striped history-table" id="acreditaciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tienda</th>
                                <th>Monto</th>
                                <th>Puntos</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($receipts as $receipt): ?>
                                <tr id="receipt-<?= $receipt->receipt_id ?>">
                                    <td><?= $receipt->date ?></td>
                                    <td><?= mb_convert_case($receipt->store, MB_CASE_TITLE, "UTF-8"); ?></td>
                                    <td>$ <?= number_format($receipt->amount,'2','.',',') ?></td>
                                    <td><?= $receipt->points ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnCancelarAcreditacion"
                                                data-client_id="<?= $receipt->client_id ?>"
                                                data-receipt_id="<?= $receipt->receipt_id ?>"
                                                data-client="<?= $receipt->client ?>"
                                                data-date="<?= $receipt->date ?>"
                                                data-store="<?= $receipt->store ?>"
                                                data-amount="<?= $receipt->amount ?>"
                                                data-points="<?= $receipt->points ?>">
                                            Cancelar
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="redenciones" role="tabpanel" aria-labelledby="list-tuesday-list">
                        <table class="table table-striped history-table" id="redenciones-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Certificado</th>
                                <th>Cantidad</th>
                                <th>Puntos</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($redemptions as $redemption): ?>
                                <tr id="redemption-<?= $redemption->redemption_id ?>">
                                    <td><?= $redemption->date ?></td>
                                    <td><?= mb_convert_case($redemption->certificate, MB_CASE_TITLE, "UTF-8"); ?></td>
                                    <td><?= $redemption->amount ?></td>
                                    <td><?= $redemption->points ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnCancelarRedencion"
                                                data-client_id="<?= $redemption->client_id ?>"
                                                data-redemption_id="<?= $redemption->redemption_id ?>"
                                                data-client="<?= mb_convert_case($redemption->client, MB_CASE_TITLE, "UTF-8"); ?>"
                                                data-date="<?= $redemption->date ?>"
                                                data-certificate="<?= mb_convert_case($redemption->certificate, MB_CASE_TITLE, "UTF-8"); ?>"
                                                data-amount="<?= $redemption->amount ?>"
                                                data-points="<?= $redemption->points ?>">
                                            Cancelar
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="beneficios" role="tabpanel" aria-labelledby="list-sunday-list">
                        <table class="table table-striped history-table" id="beneficios-table">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Beneficio</th>
                                <th>Cantidad</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($benefits as $benefit): ?>
                                <tr id="benefit-<?= $benefit->redemption_id ?>">
                                    <td><?= $benefit->date ?></td>
                                    <td><?= mb_convert_case($benefit->benefit, MB_CASE_TITLE, "UTF-8"); ?></td>
                                    <td><?= $benefit->amount ?></td>
                                    <td>
                                        <button class="btn btn-sm btn-primary btnCancelarBeneficio"
                                                data-client_id="<?= $benefit->client_id ?>"
                                                data-redemption_id="<?= $benefit->redemption_id ?>"
                                                data-client="<?= mb_convert_case($benefit->client, MB_CASE_TITLE, "UTF-8"); ?>"
                                                data-date="<?= $benefit->date ?>"
                                                data-benefit="<?= mb_convert_case($benefit->benefit, MB_CASE_TITLE, "UTF-8"); ?>"
                                                data-amount="<?= $benefit->amount ?>">
                                            Cancelar
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-4 offset-1">
                <?= $this->include('partials/partner-data-full') ?>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/vendors/simple-datatables/style.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendors/simple-datatables/simple-datatables.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/cancelaciones.js') ?>"></script>
<?= $this->endSection() ?>

