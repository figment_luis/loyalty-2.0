<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Registrar socios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Registrar Nuevo Socio</h3>
                <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
            </div>
        </div>
    </div>
    <section class="section">
        <form action="<?= base_url('client-register') ?>" method="post" class="form-registrarUsuario" id="formRegistrarUsuario">
            <div class="row">
                <div class="col-12">
                    <h5>Tarjeta</h5>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-4">
                                        <label for="card_number">Número de Tarjeta <span class="required">*</span></label>
                                        <input type="text"
                                               name="card_number"
                                               class="form-control form-control-lg"
                                               data-required="El número de tarjeta es un dato requerido"
                                               data-toolong="El número de tarjeta no debe exceder los 15 digitos"
                                               data-toshort="El número de tarjeta debe ser de al menos 15 digitos"
                                               data-pattern="El número de tarjeta no parece ser un dato válido"
                                               id="card_number"
                                               placeholder="Ingrese el número de tarjeta"
                                               pattern="[0-9A-Z-]{15}"
                                               maxlength="15"
                                               minlength="15"
                                               required>
                                    </div>
                                    <div class="col-4 levelCard" id="levelCard">
                                        <label for="slcLevel">Nivel de Tarjeta Asignada <span class="required">*</span></label>
                                        <select name="card_level_id" class="form-select form-select-lg"
                                                data-required="El nivel de tarjeta asignada es un dato requerido"
                                                id="slcLevel" disabled required>
                                            <option value="" selected disabled>Seleccione una opción</option>
                                            <?php foreach ($cardLevels as $level): ?>
                                                <option value="<?= $level->id ?>"><?= $level->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <label>¿Desea asignar una Tarjeta Digital?</label>
                                        <button class="btn btn-primary btn-lg w-100" id="btnCardDigitalNumber">Generar número de tarjeta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-5">Datos del nuevo socio</h5>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="first_name">Nombre(s) <span class="required">*</span></label>
                                <input type="text"
                                       name="first_name"
                                       class="form-control form-control-lg"
                                       data-required="El nombre es un dato requerido"
                                       data-toolong="El nombre no debe exceder los 30 caracteres"
                                       data-toshort="El nombre debe ser de al menos 2 caracteres"
                                       data-pattern="El nombre no parece ser un dato válido"
                                       id="first_name"
                                       placeholder="Nombre del nuevo socio"
                                       pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{2,30}"
                                       minlength="2"
                                       maxlength="30"
                                       required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="last_name">Apellidos <span class="required">*</span></label>
                                <input type="text"
                                       name="last_name"
                                       class="form-control form-control-lg"
                                       data-required="Los apellidos son un dato requerido"
                                       data-toolong="Los apellidos no deben exceder los 50 caracteres"
                                       data-toshort="Los apellidos deben ser de al menos 5 caracteres"
                                       data-pattern="Los apellidos no parecen ser un dato válido"
                                       id="last_name"
                                       placeholder="Apellidos del nuevo socio"
                                       pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{5,50}"
                                       minlength="5"
                                       maxlength="50"
                                       required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="slcGender">Género <span class="required">*</span></label>
                                <select name="gender" class="form-select form-select-lg"
                                        data-required="El género es un dato requerido"
                                        id="slcGender" required>
                                    <option value="" selected disabled>Seleccione una opción</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="birthday">Fecha de Nacimiento  <span class="required">*</span></label>
                                <input type="date" name="birthday" class="form-control form-control-lg"
                                       data-required="La fecha de nacimiento es un dato requerido"
                                       data-bad="La fecha de nacimiento es incorrecta"
                                       data-rangeoverflow="Solo se pueden registrar usuarios mayores de edad"
                                       data-rangeunderflow="Solo se pueden registar usuarios menores de 91 años de edad"
                                       id="birthday" step="1" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="password">Contraseña (de 5 a 10 caracteres) <span class="required">*</span></label>
                                <input type="password"
                                       name="password"
                                       class="form-control form-control-lg"
                                       data-required="La contraseña es un dato requerido"
                                       data-toolong="La contraseña no debe exceder los 10 caracteres"
                                       data-toshort="La contraseña debe ser de al menos 5 caracteres"
                                       id="password"
                                       placeholder="Contraseña para el acceso"
                                       minlength="5"
                                       maxlength="10"
                                       required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="confirm_password">Confirmar Contraseña  <span class="required">*</span></label>
                                <input type="password"
                                       name="confirm_password"
                                       class="form-control form-control-lg"
                                       data-required="La confirmación de contraseña es un dato requerido"
                                       data-toolong="La confirmación de contraseña no debe exceder los 10 caracteres"
                                       data-toshort="La confirmación de contraseña debe ser de al menos 5 caracteres"
                                       id="confirm_password"
                                       placeholder="Confirme la contraseña"
                                       minlength="5"
                                       maxlength="10"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="mt-5">Dirección</h5>
            <hr>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="slcEstado">Estado  <span class="required">*</span></label>
                        <select name="estado" class="form-select form-select-lg"
                                data-required="El Estado es un dato requerido"
                                id="slcEstado" required>
                            <option value="" selected disabled>Seleccione un estado</option>
                            <?php foreach($states as $state): ?>
                                <option value="<?= $state['id'] ?>"><?= $state['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="slcMunicipio">Municipio o Delegación  <span class="required">*</span></label>
                        <select name="municipio" class="form-select form-select-lg"
                                data-required="El Municipio es un dato requerido"
                                id="slcMunicipio" required disabled>
                            <option value="" selected disabled>Seleccione un municipio</option>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="slcColonia">Colonia  <span class="required">*</span></label>
                        <select name="colonia" class="form-select form-select-lg"
                                data-required="La Colonia es un dato requerido"
                                id="slcColonia" required disabled>
                            <option value="" selected disabled>Seleccione una colonia</option>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="codigo_postal">Código Postal  <span class="required">*</span></label>
                        <input type="text"
                               name="codigo_postal"
                               class="form-control form-control-lg"
                               data-required="El código postal es un dato requerido"
                               data-toolong="El código postal no debe exceder los 5 caracteres"
                               data-toshort="El código postal debe ser de al menos 5 caracteres"
                               data-pattern="El código postal no parece ser un dato válido"
                               id="codigo_postal"
                               placeholder="Código Postal"
                               pattern="[0-9]{5}"
                               minlength="5"
                               maxlength="5"
                               required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="street">Calle</label>
                        <input type="text"
                               name="street"
                               class="form-control form-control-lg"
                               data-toolong="El nombre de la calle no debe exceder los 60 caracteres"
                               data-toshort="El nombre de la calle debe ser de al menos 5 caracteres"
                               data-pattern="El nombre de la calle no parece ser un dato válido"
                               id="street"
                               pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ. ]{5,60}"
                               minlength="5"
                               maxlength="60"
                               placeholder="Calle o Avenida">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="exterior">Número exterior</label>
                        <input type="text"
                               name="exterior"
                               class="form-control form-control-lg"
                               data-toolong="El número exterior no debe exceder los 16 caracteres"
                               data-toshort="El número exterior debe ser de al menos 1 caracter"
                               data-pattern="El número exterior no parece ser un dato válido"
                               id="exterior"
                               pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ.# ]{1,16}"
                               minlength="1"
                               maxlength="16"
                               placeholder="Número exterior">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="interior">Número interior</label>
                        <input type="text"
                               name="interior"
                               class="form-control form-control-lg"
                               data-toolong="El número interior no debe exceder los 16 caracteres"
                               data-toshort="El número interior debe ser de al menos 1 caracter"
                               data-pattern="El número interior no parece ser un dato válido"
                               id="interior"
                               pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ.# ]{1,16}"
                               minlength="1"
                               maxlength="16"
                               placeholder="Número interior">
                    </div>
                </div>
            </div>
            <h5 class="mt-5">Teléfono y Correo electrónico</h5>
            <hr>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="mobile">Teléfono Móvil <span class="required">*</span></label>
                        <input type="text"
                               name="mobile"
                               class="form-control form-control-lg"
                               data-required="El teléfono móvil es un dato requerido"
                               data-toolong="El teléfono móvil no debe exceder los 10 dígitos"
                               data-toshort="El teléfono móvil debe ser de al menos 10 dígitos"
                               data-pattern="El teléfono móvil no parece ser un dato válido"
                               id="mobile"
                               placeholder="Teléfono móvil"
                               pattern="[0-9]{10}"
                               minlength="10"
                               maxlength="10"
                               required>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="telephone">Teléfono Fijo</label>
                        <input type="text"
                               name="telephone"
                               class="form-control form-control-lg"
                               data-toolong="El teléfono fijo no debe exceder los 10 dígitos"
                               data-toshort="El teléfono fijo debe ser de al menos 10 dígitos"
                               data-pattern="El teléfono fijo no parece ser un dato válido"
                               id="telephone"
                               pattern="[0-9]{10}"
                               minlength="10"
                               maxlength="10"
                               placeholder="Teléfono fijo">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="email">Email <span class="required">*</span></label>
                        <input type="email"
                               name="email"
                               class="form-control form-control-lg"
                               data-required="El email es un dato requerido"
                               data-toolong="El email no debe exceder los 60 caracteres"
                               data-toshort="El email debe ser de al menos 10 caracteres"
                               data-type="El email no parecen ser un dato válido"
                               id="email"
                               placeholder="Correo electrónico principal"
                               minlength="10"
                               maxlength="60"
                               required>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="email_alternative">Email Alternativo</label>
                        <input type="email"
                               name="email_alternative"
                               class="form-control form-control-lg"
                               data-toolong="El email alternativo no debe exceder los 60 caracteres"
                               data-toshort="El email alternativo debe ser de al menos 10 caracteres"
                               data-type="El email alternativo no parecen ser un dato válido"
                               id="email_alternative"
                               minlength="10"
                               maxlength="60"
                               placeholder="Correo electrónico alternativo">
                    </div>
                </div>
            </div>
            <h5 class="mt-5">Información Legal</h5>
            <hr>
            <div class="row">
                <div class="col-12">
                    <div class="form-check">
                        <div class="checkbox">
                            <input type="checkbox" name="newsletter" class="form-check-input" value="1" id="newsletter">
                            <label for="newsletter">Recibir ofertas, invitaciones y promociones</label>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <div class="checkbox">
                            <input type="checkbox" class="form-check-input" name="terms_and_conditions" value="1"
                                   data-required="Los términos y condiciones son un dato requerido"
                                   id="terms_and_conditions" required checked>
                            <label for="terms_and_conditions">Acepto los términos y condiciones, así como la política de privacidad</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-5">
                    <button type="reset" class="btn btn-lg btn-secondary" id="btnCancel">Cancelar</button>
                    <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit me-3 position-relative" id="btnSubmit">
                        <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                        Enviar
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/libs/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/moment/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/cleave.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/addons/cleave-phone.mx.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/registrar-socio.js') ?>"></script>
<?= $this->endSection() ?>
