<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Catálogo de certificados<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <?php if ($certificates): ?>
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12">
                <?= $this->include('partials/partner-data-single-alt') ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <section class="section">
        <div class="row">
            <?php if ($certificates): ?>
            <div class="col-12 my-4">
                <h3>Catálogo de Certificados</h3>
                <p class="text-subtitle">Se muestran únicamente las recompensas que el usuario puede redimir con base en sus puntos disponibles.</p>
            </div>
                <?php foreach ($certificates as $certificate): ?>
                    <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
                        <div class="certificate">
                            <img src="<?= base_url(esc($certificate['image'])) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($certificate['title'])) ?>" title="<?= strtoupper(esc($certificate['title'])) ?>">
                            <h5 class="certificate__title"><?= esc($certificate['title']) ?></h5>
                            <h6 class="certificate__subtitle"><?= esc($certificate['subtitle']) ?></h6>
                            <ul class="p-0 list-unstyled certificate__list">
                                <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= number_format(esc($certificate['points']),0,'.',',') ?></li>
                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($certificate['current_stock']) ?></li>
                            </ul>
                            <p class="certificate__description"><?= esc($certificate['description']) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-5 text-center">
                    <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid mb-4">
                    <h2 class="listado-premios__title-error">Upss!</h2>
                    <p class="listado-premios__message">Malas noticias para <strong><?= $client['name'] ?></strong>, coméntale que no tiene los puntos suficientes para realizar una redención.</p>
                    <a href="<?= base_url('socios/acreditar-puntos/' . $client['client_id']) ?>" class="btn btn-lg btn-primary">Acreditar puntos</a>
                </div>
                <div class="col-6 offset-1">
                    <?= $this->include('partials/partner-data-medium') ?>
                    <h4 class="mt-4">Coméntale al socio lo que se está perdiendo</h4>
                    <p class="mb-4">Si acredita unos cuantos puntos mas, puede redimir alguno de estos certificados.</p>
                    <div class="row">
                        <?php foreach ($certificates_shift as $c): ?>
                            <div class="col-6">
                                <div class="certificate">
                                    <img src="<?= base_url(esc($c->image)) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($c->title)) ?>" title="<?= strtoupper(esc($c->title)) ?>">
                                    <h5 class="certificate__title"><?= esc($c->title) ?></h5>
                                    <h6 class="certificate__subtitle"><?= esc($c->subtitle) ?></h6>
                                    <ul class="p-0 list-unstyled certificate__list">
                                        <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= esc($c->points) ?></li>
                                        <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($c->current_stock) ?></li>
                                    </ul>
                                    <p class="certificate__description"><?= esc($c->description) ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<?= $this->endSection() ?>
