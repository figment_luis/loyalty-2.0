<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Registrar concierge<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Registrar Nuevo Concierge</h3>
                <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
            </div>
        </div>
    </div>
    <section class="section">
        <form action="<?= base_url('concierge-register') ?>" method="post" class="form-registrarUsuario" id="formRegistrarConcierge">
            <div class="row">
                <div class="col-12">
                    <h5>Datos del nuevo concierge</h5>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="first_name">Nombre(s) <span class="required">*</span></label>
                                <input type="text"
                                       name="first_name"
                                       class="form-control form-control-lg"
                                       data-required="El nombre es un dato requerido"
                                       data-toolong="El nombre no debe exceder los 30 caracteres"
                                       data-toshort="El nombre debe ser de al menos 2 caracteres"
                                       data-pattern="El nombre no parece ser un dato válido"
                                       id="first_name"
                                       placeholder="Nombre del nuevo concierge"
                                       pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{2,30}"
                                       minlength="2"
                                       maxlength="30"
                                       required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="last_name">Apellidos <span class="required">*</span></label>
                                <input type="text"
                                       name="last_name"
                                       class="form-control form-control-lg"
                                       data-required="Los apellidos son un dato requerido"
                                       data-toolong="Los apellidos no deben exceder los 50 caracteres"
                                       data-toshort="Los apellidos deben ser de al menos 5 caracteres"
                                       data-pattern="Los apellidos no parecen ser un dato válido"
                                       id="last_name"
                                       placeholder="Apellidos del nuevo concierge"
                                       pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{5,50}"
                                       minlength="5"
                                       maxlength="50"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="mt-5">Datos de Acceso</h5>
            <p>Indique las credenciales que le permitirán a este Concierge acceder al sistema</p>
            <hr>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="email">Email <span class="required">*</span></label>
                        <input type="email"
                               name="email"
                               class="form-control form-control-lg"
                               data-required="El email es un dato requerido"
                               data-toolong="El email no debe exceder los 60 caracteres"
                               data-toshort="El email debe ser de al menos 10 caracteres"
                               data-type="El email no parecen ser un dato válido"
                               id="email"
                               placeholder="Correo electrónico para el acceso"
                               minlength="10"
                               maxlength="60"
                               required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="password">Contraseña (de 5 a 10 caracteres) <span class="required">*</span></label>
                        <input type="password"
                               name="password"
                               class="form-control form-control-lg"
                               data-required="La contraseña es un dato requerido"
                               data-toolong="La contraseña no debe exceder los 10 caracteres"
                               data-toshort="La contraseña debe ser de al menos 5 caracteres"
                               id="password"
                               placeholder="Contraseña para el acceso"
                               minlength="5"
                               maxlength="10"
                               required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="confirm_password">Confirmar Contraseña  <span class="required">*</span></label>
                        <input type="password"
                               name="confirm_password"
                               class="form-control form-control-lg"
                               data-required="La confirmación de contraseña es un dato requerido"
                               data-toolong="La confirmación de contraseña no debe exceder los 10 caracteres"
                               data-toshort="La confirmación de contraseña debe ser de al menos 5 caracteres"
                               id="confirm_password"
                               placeholder="Confirme la contraseña"
                               minlength="5"
                               maxlength="10"
                               required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-5">
                    <button type="reset" class="btn btn-lg btn-secondary" id="btnCancel">Cancelar</button>
                    <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit me-3 position-relative" id="btnSubmit">
                        <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                        Enviar
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/libs/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/moment/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/cleave.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/addons/cleave-phone.mx.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/registrar-concierge.js') ?>"></script>
<?= $this->endSection() ?>
