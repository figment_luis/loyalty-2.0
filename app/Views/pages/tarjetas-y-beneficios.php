<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Información de tarjetas<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12">
                <h3>Información de tarjetas</h3>
                <p class="text-subtitle">Información detallada de las tarjetas disponibles y sus beneficios.</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <?php foreach ($cards as $card): ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="cardBenefit">
                    <figure>
                        <img src="<?= base_url(esc($card->image)) ?>" alt="NIVEL <?= strtoupper(esc($card->name)) ?>" title="NIVEL <?= strtoupper(esc($card->name)) ?>" class="img-fluid">
                    </figure>
                    <figcaption>
                        <h5><?= esc($card->name) ?></h5>
                        <p><?= esc($card->description) ?></p>
                        <p><i class="fas fa-coins"></i><span class="ms-2"><?= (int) esc($card->factor) ?> Punto<?= $card->factor > 1 ? 's' : ''?> por cada $50 pesos</span></p>
                    </figcaption>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<?= $this->endSection() ?>

