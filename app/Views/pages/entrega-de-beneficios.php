<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Entrega de beneficios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1 ">
    <div class="page-title mb-4">
        <div class="row">
            <?php if (count($benefitsAll)): ?>
                <?php if (count($benefits)): ?>
                    <div class="col-7">
                        <?= $this->include('partials/partner-data-single') ?>
                        <form action="<?= base_url('redemptions') ?>" method="post" class="form-registrarUsuario" id="formEntregaDeBeneficios">
                            <div class="d-flex justify-content-between align-items-center mt-4">
                                <h3>Entrega de Beneficios</h3>
                                <button type="button" id="btnImprimirTicketAlternative" class="btn btn-primary">Imprimir ticket</button>
                            </div>
                            <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <input type="hidden" name="client_id" class="form-control form-control-lg" id="client_id" value="<?= esc($client['client_id']) ?>" readonly>
                                    <input type="hidden" name="card_id" class="form-control form-control-lg" id="card_id" value="<?= esc($client['card_id']) ?>" readonly>
                                    <input type="hidden" name="level_id" class="form-control form-control-lg" id="card_id" value="<?= esc($cardInfo['card_level_id']) ?>" readonly>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="slcBenefit">Beneficio <span class="required">*</span></label>
                                        <select name="benefit_id" class="form-select form-select-lg"
                                                data-required="El beneficio es un dato requerido"
                                                id="slcBenefit" required>
                                            <option value="" selected disabled>Seleccione un beneficio</option>
                                            <?php foreach ($benefits as $benefit): ?>
                                                <!-- todo: Preguntar con Backend si puede fltrarme los datos con base al nivel de socio
                                                 caso contrario, usar este condicional -->
                                                <?php if ($benefit['level_id'] == $cardInfo['card_level_id']): ?>
                                                    <option value="<?= esc($benefit['id']) ?>"
                                                            data-title="<?= esc($benefit['title']) ?>"
                                                            data-subtitle="<?= esc($benefit['subtitle']) ?>"
                                                            data-stock="<?= esc($benefit['current_stock']) ?>"
                                                            data-description="<?= esc($benefit['description']) ?>"
                                                            data-image="<?= base_url(esc($benefit['image'])) ?>"
                                                            data-unlimited="<?= esc($benefit['unlimited_stock']) ?>"
                                                            data-day="<?= esc($benefit['rule_days']) ?>"
                                                            data-week="<?= esc($benefit['rule_week']) ?>"
                                                            data-month="<?= esc($benefit['rule_months']) ?>"
                                                            data-shop="<?= esc($benefit['rule_shop']) ?>"
                                                    ><?= esc($benefit['title']) ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="quantity">Cantidad <span class="required">*</span></label>
                                        <input type="number" name="amount" step="1" min="1" class="form-control form-control-lg"
                                               data-required="La cantidad es un dato requerido"
                                               data-rangeoverflow="La cantidad no debe exceder las unidades disponibles"
                                               data-rangeunderflow="La cantidad mímina debe ser de 1 unidad"
                                               data-step="La cantidad debe estar expresada en números enteros"
                                               id="quantity" placeholder="Número de unidades a canjear" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mt-5">
                                    <button type="reset" class="btn btn-lg btn-secondary me-3 " id="btnCancel">Cancelar</button>
                                    <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit position-relative" id="btnSubmit">
                                        <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                                        Enviar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-4 offset-1">
                        <div class="certificate mt-4 preview" id="benefit-view">
                            <img src="#" class="img-fluid certificate__image" alt="vista previa">
                            <h5 class="certificate__title"></h5>
                            <h6 class="certificate__subtitle"></h6>
                            <ul class="p-0 list-unstyled certificate__list">
                                <li class="certificate__item"></li>
                            </ul>
                            <p class="certificate__description"></p>
                            <div class="certificate__rules">
                                <h6 class="certificate__rules-title"><i class="fas fa-info-circle"></i> Condiciones para entregrar este beneficio</h6>
                                <ul class="certificate__rules-item"></ul>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-5 text-center">
                        <img src="<?= base_url('assets/images/svg/404-benefits.svg')?>" alt="No hay premios registrados" class="img-fluid mb-4">
                        <h2 class="listado-premios__title-error">Upss!</h2>
                        <p class="listado-premios__message">Malas noticias para <strong><?= $client['name'] ?></strong>, coméntale que no tiene el nivel suficiente para recibir un beneficio.</p>
                        <a href="<?= base_url('socios/acreditar-puntos/' . $client['client_id']) ?>" class="btn btn-lg btn-primary">Acreditar puntos</a>
                    </div>
                    <div class="col-6 offset-1">
                        <?= $this->include('partials/partner-data-medium-alt') ?>
                        <h4 class="mt-4">Coméntale al socio lo que se está perdiendo</h4>
                        <p class="mb-4">Si acredita unos cuantos puntos mas, puede subir de nivel y obtener grandes beneficios.</p>
                        <div class="row">
                            <?php foreach ($benefitsAll as $b): ?>
                                <div class="col-6">
                                    <div class="certificate">
                                        <img src="<?= base_url(esc($b->image)) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($b->title)) ?>" title="<?= strtoupper(esc($b->title)) ?>">
                                        <h5 class="certificate__title"><?= esc($b->title) ?></h5>
                                        <h6 class="certificate__subtitle"><?= esc($b->subtitle) ?></h6>
                                        <ul class="p-0 list-unstyled certificate__list">
                                            <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($b->current_stock) ?></li>
                                        </ul>
                                        <p class="certificate__description"><?= esc($b->description) ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <div class="col-12 text-center">
                    <img src="<?= base_url('assets/images/svg/404-benefits.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                    <h2 class="listado-premios__title-error">Upss!</h2>
                    <p class="listado-premios__message">Coméntale a <strong><?= $client['name'] ?></strong>, que no encontramos ningún beneficio disponible por el momento, pero no que no prepupe.
                        <br>
                        <strong>Mañana de seguro que hay más.</strong></p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/loyalty/entregar-beneficios.js') ?>"></script>
<?= $this->endSection() ?>

