<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Listado de beneficios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <?php if ($newBenefits): ?>
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12 order-md-1 order-last">
                <h3>Listado de Beneficios</h3>
                <p class="text-subtitle">Se muestran los beneficios que un socio puede recibir con base en su nivel dentro del plan de lealtad.</p>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <section class="section">
        <div class="row">
            <?php if ($newBenefits): ?>
                <?php foreach ($newBenefits as $benefit): ?>
                    <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
                        <div class="certificate">
                            <img src="<?= base_url(esc($benefit['image'])) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($benefit['title'])) ?>" title="<?= strtoupper(esc($benefit['title'])) ?>">
                            <h5 class="certificate__title"><?= esc($benefit['title']) ?></h5>
                            <h6 class="certificate__subtitle"><?= esc($benefit['subtitle']) ?></h6>
                            <ul class="p-0 list-unstyled certificate__list">
                                <li class="certificate__item"><i class="fas fa-layer-group"></i> Niveles: <?= esc($benefit['levels']) ?></li>
                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($benefit['unlimited_stock']) ? 'Ilimitado' : esc($benefit['current_stock']) ?></li>
                            </ul>
                            <p class="certificate__description"><?= esc($benefit['description']) ?></p>
                            <?php if($benefit['rule_days'] || $benefit['rule_week'] || $benefit['rule_months'] || $benefit['rule_shop']): ?>
                                <div class="certificate__rules">
                                    <h6 class="certificate__rules-title"><i class="fas fa-info-circle me-1"></i> Condiciones para entregrar este beneficio</h6>
                                    <ul class="certificate__rules-item">
                                        <?php if ($benefit['rule_days']): ?>
                                            <li>Limitado a <?= $benefit['rule_days'] ?> entregas por día.</li>
                                        <?php endif; ?>
                                        <?php if ($benefit['rule_week']): ?>
                                            <li>Limitado a <?= $benefit['rule_week'] ?> entregas por semana.</li>
                                        <?php endif; ?>
                                        <?php if ($benefit['rule_months']): ?>
                                            <li>Limitado a <?= $benefit['rule_months'] ?> entregas por mes.</li>
                                        <?php endif; ?>
                                        <?php if ($benefit['rule_shop']): ?>
                                            <li>Limitado a socios que hayan acreditado un monto mínimo de $150 en compras durante los últimos 15 días.</li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-12 text-center">
                    <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                    <h2 class="listado-premios__title-error">Upss!</h2>
                    <p class="listado-premios__message">Coméntale al cliente que todos los beneficios han volado, son muchos los recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                        <br>Pero este suceso no debería preocuparle. <strong>Mañana habrá mas beneficios para entregar.</strong></p>
                </div>
            <?php endif; ?>
        </div>
    </section>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/loyalty/listado-de-beneficios.js') ?>"></script>
<?= $this->endSection() ?>
