<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Redenciones<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <?php if ($certificates): ?>
            <div class="col-7">
                <?= $this->include('partials/partner-data-single') ?>
                <form action="<?= base_url('redemptions') ?>" method="post" class="form-registrarUsuario" id="formRedenciones">
                    <div class="d-flex justify-content-between align-items-center mt-4">
                        <h3>Redenciones</h3>
                        <button type="button" id="btnImprimirTicketAlternative" class="btn btn-primary">Imprimir ticket</button>
                    </div>
                    <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="client_id" class="form-control form-control-lg" id="client_id" value="<?= esc($client['client_id']) ?>" readonly>
                            <input type="hidden" name="card_id" class="form-control form-control-lg" id="card_id" value="<?= esc($client['card_id']) ?>" readonly>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="slcCertificate">Certificado <span class="required">*</span></label>
                                <select name="certificate_id" class="form-select form-select-lg"
                                        data-required="El certificado es un dato requerido"
                                        id="slcCertificate" required>
                                    <option value="" selected disabled>Seleccione un certificado</option>
                                    <?php foreach ($certificates as $certificate): ?>
                                        <!-- todo: Llenar esta sección con datos provenientes de la DB -->
                                        <option value="<?= esc($certificate['id']) ?>"
                                                data-title="<?= esc($certificate['title']) ?>"
                                                data-subtitle="<?= esc($certificate['subtitle']) ?>"
                                                data-description="<?= esc($certificate['description']) ?>"
                                                data-image="<?= base_url(esc($certificate['image'])) ?>"
                                                data-points="<?= esc($certificate['points']) ?>"
                                                data-stock="<?= esc($certificate['current_stock']) ?>"
                                                data-points-available="<?= esc($points['points_available']) ?>"
                                        ><?= ucwords(strtolower(esc($certificate['title']))) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="quantity">Cantidad <span class="required">*</span></label>
                                <input type="number" name="amount" step="1" min="1" class="form-control form-control-lg"
                                       data-required="La cantidad es un dato requerido"
                                       data-rangeoverflow="La cantidad no debe exceder las unidades o puntos disponibles"
                                       data-rangeunderflow="La cantidad mímina debe ser de 1 unidad"
                                       data-step="La cantidad debe estar expresada en números enteros"
                                       id="quantity" placeholder="Número de unidades a canjear" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="points_to_redimir">Total de puntos a redimir </label>
                                <input type="text" class="form-control form-control-lg" id="points_to_redimir" disabled >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-5">
                            <button type="reset" class="btn btn-lg btn-secondary me-3 " id="btnCancel">Cancelar</button>
                            <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit position-relative" id="btnSubmit">
                                <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                                Enviar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4 offset-1">
                    <div class="certificate mt-4 preview" id="certificate-view">
                        <img src="#" class="img-fluid certificate__image" alt="vista previa">
                        <h5 class="certificate__title"></h5>
                        <h6 class="certificate__subtitle"></h6>
                        <ul class="p-0 list-unstyled certificate__list">
                            <li class="certificate__item"></li>
                            <li class="certificate__item"></li>
                        </ul>
                        <p class="certificate__description"></p>
                    </div>
            </div>
            <?php else: ?>
                <div class="col-5 text-center">
                    <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid mb-4">
                    <h2 class="listado-premios__title-error">Upss!</h2>
                    <p class="listado-premios__message">Malas noticias para <strong><?= $client['name'] ?></strong>, coméntale que no tiene los puntos suficientes para realizar una redención.</p>
                    <a href="<?= base_url('socios/acreditar-puntos/' . $client['client_id']) ?>" class="btn btn-lg btn-primary">Acreditar puntos</a>
                </div>
                <div class="col-6 offset-1">
                    <?= $this->include('partials/partner-data-medium') ?>
                    <h4 class="mt-4">Coméntale al socio lo que se está perdiendo</h4>
                    <p class="mb-4">Si acredita unos cuantos puntos mas, puede redimir alguno de estos certificados.</p>
                    <div class="row">
                        <?php foreach ($certificates_shift as $c): ?>
                            <div class="col-6">
                                <div class="certificate">
                                    <img src="<?= base_url(esc($c->image)) ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($c->title)) ?>" title="<?= strtoupper(esc($c->title)) ?>">
                                    <h5 class="certificate__title"><?= esc($c->title) ?></h5>
                                    <h6 class="certificate__subtitle"><?= esc($c->subtitle) ?></h6>
                                    <ul class="p-0 list-unstyled certificate__list">
                                        <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= esc($c->points) ?></li>
                                        <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($c->current_stock) ?></li>
                                    </ul>
                                    <p class="certificate__description"><?= esc($c->description) ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/loyalty/redenciones.js') ?>"></script>
<?= $this->endSection() ?>

