<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Acreditar puntos<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-7">
                <form action="#" method="post" class="form-registrarUsuario" id="formAcreditarPuntos">
                    <h3>Acreditar Puntos</h3>
                    <p class="text-subtitle"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
                    <hr>
                    <div class="row">
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="client_id" class="form-control form-control-lg" id="client_id" value="<?= esc($client['client_id']) ?>" readonly>
                                <input type="hidden" name="card_id" class="form-control form-control-lg" id="card_id" value="<?= esc($client['card_id']) ?>" readonly>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="number">Número de ticket <span class="required">*</span></label>
                                    <input type="text"
                                           name="number"
                                           class="form-control form-control-lg"
                                           data-required="El número de ticket es un dato requerido"
                                           data-toolong="El número de ticket no debe exceder los 45 caracteres"
                                           data-toshort="El número de ticket debe ser de al menos 1 caracter"
                                           id="number"
                                           placeholder="Número o código del ticket"
                                           autocomplete="off"
                                           minlength="1"
                                           maxlength="45"
                                           required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="amount">Monto de la compra <span class="required">*</span></label>
                                    <input type="text"
                                           name="amount"
                                           class="form-control form-control-lg"
                                           data-required="El monto de compra es un dato requerido"
                                           data-toolong="El monto de compra no debe exceder los 7 dígitos"
                                           data-toshort="El monto de compra debe ser de al menos 2 dígitos"
                                           data-pattern="El monto de compra no parece ser una cantidad válida"
                                           id="amount"
                                           placeholder="Monto total de la compra"
                                           autocomplete="off"
                                           minlength="2"
                                           maxlength="7"
                                           pattern="[0-9,.]{2,7}"
                                           required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="slcStore">Tienda participante <span class="required">*</span></label>
                                    <select name="store_id" class="form-select form-select-lg"
                                            data-required="La tienda participante es un dato requerido"
                                            id="slcStore" required>
                                        <option value="" selected disabled>Seleccione una tienda</option>
                                        <?php foreach ($stores as $store): ?>
                                            <option value="<?= esc($store->id) ?>"><?= ucwords(esc($store->name)) ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="date">Fecha del ticket <span class="required">*</span></label>
                                    <input type="datetime-local"
                                           name="date"
                                           class="form-control form-control-lg"
                                           data-required="La fecha del ticket es un dato requerido"
                                           data-rangeoverflow="La fecha del ticket es superior a la fecha y hora actual"
                                           data-rangeunderflow="La fecha del ticket ha expirado"
                                           id="date"
                                           autocomplete="off"
                                           required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="puntos-a-acreditar">Puntos a acreditar </label>
                                    <input type="text"
                                           class="form-control form-control-lg"
                                           id="puntos-a-acreditar"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-5">
                                <button type="reset" class="btn btn-lg btn-secondary" id="btnCancel">Cancelar</button>
                                <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit me-3 position-relative" id="btnSubmit">
                                    <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4 offset-1">
                <?= $this->include('partials/partner-data-full') ?>
                <button type="button" id="btnImprimirTicketAlternative" class="btn btn-primary mt-4">Imprimir ticket</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
<link rel="stylesheet" href="<?= base_url('assets/vendors/toastify/toastify.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/libs/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/moment/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/cleave.min.js') ?>"></script>
<script src="<?= base_url('assets/vendors/toastify/toastify.js') ?>"></script>
<script>const FACTOR = '<?= $client['factor'] ?>>'</script>
<script src="<?= base_url('assets/js/loyalty/acreditar-puntos.js') ?>"></script>
<?= $this->endSection() ?>

