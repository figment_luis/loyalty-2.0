<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Actualizar socios<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading flex-grow-1">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-7 ">
                <h3>Editar Socio</h3>
                <p class="text-subtitle mb-4"><span class="required">*</span> Introduzca los datos que a continuación se requieren.</p>
                <form action="<?= base_url('editar-socio') ?>" method="post" class="form-registrarUsuario" id="formEditarUsuario">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="client_id" class="form-control form-control-lg" id="client_id" value="<?= esc($clientInfo->id) ?>" readonly>
                        </div>
                        <div class="col-12">
                            <h5>Datos del socio</h5>
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="first_name">Nombre(s) <span class="required">*</span></label>
                                        <input type="text"
                                               name="first_name"
                                               class="form-control form-control-lg"
                                               data-required="El nombre es un dato requerido"
                                               data-toolong="El nombre no debe exceder los 30 caracteres"
                                               data-toshort="El nombre debe ser de al menos 2 caracteres"
                                               data-pattern="El nombre no parece ser un dato válido"
                                               id="first_name"
                                               value="<?= esc($clientInfo->first_name) ?>"
                                               placeholder="Nombre del nuevo socio"
                                               pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{2,30}"
                                               minlength="2"
                                               maxlength="30"
                                               required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="last_name">Apellidos <span class="required">*</span></label>
                                        <input type="text"
                                               name="last_name"
                                               class="form-control form-control-lg"
                                               data-required="Los apellidos son un dato requerido"
                                               data-toolong="Los apellidos no deben exceder los 50 caracteres"
                                               data-toshort="Los apellidos deben ser de al menos 5 caracteres"
                                               data-pattern="Los apellidos no parecen ser un dato válido"
                                               id="last_name"
                                               value="<?= esc($clientInfo->last_name) ?>"
                                               placeholder="Apellidos del nuevo socio"
                                               pattern="[A-Za-zÀ-ÖØ-öø-ÿ ]{5,50}"
                                               minlength="5"
                                               maxlength="50"
                                               required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="slcGender">Género <span class="required">*</span></label>
                                        <select name="gender" class="form-select form-select-lg"
                                                data-required="El género es un dato requerido"
                                                id="slcGender" required>
                                            <option value="" selected disabled>Seleccione una opción</option>
                                            <option value="M" <?= esc($clientInfo->gender) == 'M' ? 'selected' : '' ?> >Masculino</option>
                                            <option value="F" <?= esc($clientInfo->gender) == 'F' ? 'selected' : '' ?> >Femenino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="birthday">Fecha de Nacimiento  <span class="required">*</span></label>
                                        <input type="date" name="birthday" class="form-control form-control-lg"
                                               value="<?= esc($clientInfo->birthday) ? date('Y-m-d', strtotime(esc($clientInfo->birthday))) : '' ?>"
                                               data-required="La fecha de nacimiento es un dato requerido"
                                               data-bad="La fecha de nacimiento es incorrecta"
                                               data-rangeoverflow="Solo se pueden registrar usuarios mayores de edad"
                                               data-rangeunderflow="Solo se pueden registar usuarios menores de 91 años de edad"
                                               id="birthday" step="1" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-5">Contraseña</h5>
                    <p>Deje los campos vacios si desea conservar la contraseña actual</p>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="password">Contraseña (de 5 a 10 caracteres)</label>
                                <input type="password"
                                       name="password"
                                       class="form-control form-control-lg"
                                       id="password"
                                       placeholder="Contraseña para el acceso"
                                       minlength="0"
                                       maxlength="10">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="confirm_password">Confirmar Contraseña </label>
                                <input type="password"
                                       name="confirm_password"
                                       class="form-control form-control-lg"
                                       id="confirm_password"
                                       placeholder="Confirme la contraseña"
                                       minlength="0"
                                       maxlength="10">
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-5">Dirección</h5>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="slcEstado">Estado  <span class="required">*</span></label>
                                <select name="estado" class="form-select form-select-lg"
                                        data-required="El Estado es un dato requerido"
                                        id="slcEstado" required>
                                    <option value="" selected disabled>Seleccione un estado</option>
                                    <?php foreach($states as $state): ?>
                                        <option value="<?= esc($state['id']) ?>" <?= $clientInfo->estado == $state['id'] ? 'selected' : '' ?>><?= esc($state['name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="slcMunicipio">Municipio o Delegación  <span class="required">*</span></label>
                                <select name="municipio" class="form-select form-select-lg"
                                        data-required="El Municipio es un dato requerido"
                                        id="slcMunicipio" required>
                                    <option value="" selected disabled>Seleccione un municipio</option>
                                    <?php foreach($municipios as $municipio): ?>
                                        <option value="<?= esc($municipio['id']) ?>" <?= $clientInfo->municipio == $municipio['id'] ? 'selected' : '' ?>><?= esc($municipio['name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="slcColonia">Colonia  <span class="required">*</span></label>
                                <select name="colonia" class="form-select form-select-lg"
                                        data-required="La Colonia es un dato requerido"
                                        id="slcColonia" required>
                                    <option value="" selected disabled>Seleccione una colonia</option>
                                    <?php foreach($colonias as $colonia): ?>
                                        <option value="<?= esc($colonia['id']) ?>" <?= $clientInfo->colonia == $colonia['id'] ? 'selected' : '' ?>><?= esc($colonia['name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="street">Calle</label>
                                <input type="text"
                                       name="street"
                                       class="form-control form-control-lg"
                                       data-toolong="El nombre de la calle no debe exceder los 60 caracteres"
                                       data-toshort="El nombre de la calle debe ser de al menos 5 caracteres"
                                       data-pattern="El nombre de la calle no parece ser un dato válido"
                                       id="street"
                                       value="<?= esc($clientInfo->street) ?>"
                                       pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ. ]{5,60}"
                                       minlength="5"
                                       maxlength="60"
                                       placeholder="Calle o Avenida">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="exterior">Número exterior</label>
                                <input type="text"
                                       name="exterior"
                                       class="form-control form-control-lg"
                                       data-toolong="El número exterior no debe exceder los 16 caracteres"
                                       data-toshort="El número exterior debe ser de al menos 1 caracter"
                                       data-pattern="El número exterior no parece ser un dato válido"
                                       id="exterior"
                                       value="<?= esc($clientInfo->exterior) ?>"
                                       pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ.# ]{1,16}"
                                       minlength="1"
                                       maxlength="16"
                                       placeholder="Número exterior">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="interior">Número interior</label>
                                <input type="text"
                                       name="interior"
                                       class="form-control form-control-lg"
                                       data-toolong="El número interior no debe exceder los 16 caracteres"
                                       data-toshort="El número interior debe ser de al menos 1 caracter"
                                       data-pattern="El número interior no parece ser un dato válido"
                                       id="interior"
                                       value="<?= esc($clientInfo->interior) ?>"
                                       pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ.# ]{1,16}"
                                       minlength="1"
                                       maxlength="16"
                                       placeholder="Número interior">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="codigo_postal">Código Postal  <span class="required">*</span></label>
                                <input type="text"
                                       name="codigo_postal"
                                       class="form-control form-control-lg"
                                       data-required="El código postal es un dato requerido"
                                       data-toolong="El código postal no debe exceder los 5 caracteres"
                                       data-toshort="El código postal debe ser de al menos 5 caracteres"
                                       data-pattern="El código postal no parece ser un dato válido"
                                       id="codigo_postal"
                                       value="<?= esc($clientInfo->codigo_postal) ?>"
                                       placeholder="Código Postal"
                                       pattern="[0-9]{5}"
                                       minlength="5"
                                       maxlength="5"
                                       required>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-5">Teléfono y Correo electrónico</h5>
                    <p>Solo los Gerentes de Mercadotecnia están autorizados para editar el Email Principal</p>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="mobile">Teléfono Móvil <span class="required">*</span></label>
                                <input type="text"
                                       name="mobile"
                                       class="form-control form-control-lg"
                                       data-required="El teléfono móvil es un dato requerido"
                                       data-toolong="El teléfono móvil no debe exceder los 10 dígitos"
                                       data-toshort="El teléfono móvil debe ser de al menos 10 dígitos"
                                       data-pattern="El teléfono móvil no parece ser un dato válido"
                                       id="mobile"
                                       value="<?= esc($clientInfo->mobile) ?>"
                                       placeholder="Teléfono móvil"
                                       pattern="[0-9]{10}"
                                       minlength="10"
                                       maxlength="10"
                                       required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="telephone">Teléfono Fijo</label>
                                <input type="text"
                                       name="telephone"
                                       class="form-control form-control-lg"
                                       data-toolong="El teléfono fijo no debe exceder los 10 dígitos"
                                       data-toshort="El teléfono fijo debe ser de al menos 10 dígitos"
                                       data-pattern="El teléfono fijo no parece ser un dato válido"
                                       id="telephone"
                                       value="<?= esc($clientInfo->telephone) ?>"
                                       pattern="[0-9]{10}"
                                       minlength="10"
                                       maxlength="10"
                                       placeholder="Teléfono fijo">
                            </div>
                        </div>
                        <?php if(session()->get('role') == 1 || session()->get('role') == 3): ?>
                            <!-- Gerentes pueden cambiar el correo electrónico principal de una cuenta en el plan de lealtad -->
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="email"
                                           name="email"
                                           class="form-control form-control-lg"
                                           data-required="El email es un dato requerido"
                                           data-toolong="El email no debe exceder los 60 caracteres"
                                           data-toshort="El email debe ser de al menos 10 caracteres"
                                           data-type="El email no parecen ser un dato válido"
                                           id="email"
                                           value="<?= esc($clientInfo->email) ?>"
                                           placeholder="Correo electrónico principal"
                                           minlength="10"
                                           maxlength="60"
                                           required>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email_alternative">Email Alternativo</label>
                                <input type="email"
                                       name="email_alternative"
                                       class="form-control form-control-lg"
                                       data-toolong="El email alternativo no debe exceder los 60 caracteres"
                                       data-toshort="El email alternativo debe ser de al menos 10 caracteres"
                                       data-type="El email alternativo no parecen ser un dato válido"
                                       id="email_alternative"
                                       value="<?= esc($clientInfo->email_alternative) ?>"
                                       minlength="10"
                                       maxlength="60"
                                       placeholder="Correo electrónico alternativo">
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-5">Información Legal</h5>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-check">
                                <div class="checkbox">
                                    <input type="checkbox" name="newsletter"
                                           class="form-check-input"
                                           value="1"
                                        <?= $clientInfo->newsletter == 1 ? 'checked' : '' ?>
                                           id="newsletter">
                                    <label for="newsletter">Recibir ofertas, invitaciones y promociones</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <div class="checkbox">
                                    <input type="checkbox" class="form-check-input" name="terms_and_conditions"
                                           value="1"
                                           data-required="Los términos y condiciones son un dato requerido"
                                           id="terms_and_conditions"
                                           required checked>
                                    <label for="terms_and_conditions">Acepto los términos y condiciones, así como la política de privacidad</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-5">
                            <button type="reset" class="btn btn-lg btn-secondary" id="btnCancel">Cancelar</button>
                            <button type="submit" name="send" class="btn btn-lg btn-primary btnSubmit me-3 position-relative" id="btnSubmit">
                                <img src="<?= base_url('assets/images/loader.svg') ?>" alt="loader" title="Loader" class="loader" id="loader">
                                Enviar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4 offset-1">
                <?= $this->include('partials/partner-data-full') ?>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/js/libs/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/moment/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/cleave.min.js') ?>"></script>
<script src="<?= base_url('assets/js/libs/cleave/addons/cleave-phone.mx.js') ?>"></script>
<script src="<?= base_url('assets/js/loyalty/editar-socio.js') ?>"></script>
<?= $this->endSection() ?>
