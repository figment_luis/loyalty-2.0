<!-- Extends to Main Layout -->
<?= $this->extend('layout/main-layout') ?>

<!-- PAGE TITLE -->
<?= $this->section('title') ?>Panel de control<?= $this->endSection() ?>

<!----------------------------------------->
<!-- INSERT THE MAIN CONTENT OF THE PAGE -->
<!----------------------------------------->
<?= $this->section('content') ?>
<div class="page-heading flex-grow-1 d-flex flex-column">
    <div class="page-title mb-4">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Panel de Control</h3>
                <p class="text-subtitle">Selecciona una tarea.</p>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <!--nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Layout Vertical Navbar</li>
                    </ol>
                </nav-->
            </div>
        </div>
    </div>

    <section class="section flex-grow-1">
        <div class="row">
            <div class="col-md-6 col-lg-4 ">
                <a href="<?= base_url('clientes/registrar-usuario') ?>">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="far fa-edit position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Registrar Socio</h6>
                                    <p class="mb-0">Registre un nuevo socio en el plan de lealtad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 ">
                <a href="#"
                   data-bs-title="Acreditar Puntos"
                   data-bs-url="client-search"
                   data-bs-redirect="socios/acreditar-puntos"
                   data-bs-toggle="modal" data-bs-target="#searchModal">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-tag position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Acreditar Puntos</h6>
                                    <p class="mb-0">Registrar los tickets de un socio registrado en el plan de lealtad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 ">
                <a href="#"
                   data-bs-title="Redenciones"
                   data-bs-url="client-search"
                   data-bs-redirect="socios/redenciones"
                   data-bs-toggle="modal" data-bs-target="#searchModal">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-gift position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Redenciones</h6>
                                    <p class="mb-0">Entregar certificados a un socio registrado en el plan de lealtad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 ">
                <a href="#"
                   data-bs-title="Entrega de Beneficios"
                   data-bs-url="client-search"
                   data-bs-redirect="socios/entrega-de-beneficios"
                   data-bs-toggle="modal" data-bs-target="#searchModal">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-trophy position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Entregar Benficios</h6>
                                    <p class="mb-0">Entregar beneficios a un socio registrado en el plan de lealtad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 ">
                <a href="#"
                   data-bs-title="Consultar Estado de Cuenta"
                   data-bs-url="client-search"
                   data-bs-redirect="socios/estado-de-cuenta"
                   data-bs-toggle="modal" data-bs-target="#searchModal">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-user position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Estado de Cuenta</h6>
                                    <p class="mb-0">Consultar el estado de cuenta de un socio registrado en el plan de lealtad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 ">
                <a href="#"
                   data-bs-title="Catalogo de Certificados"
                   data-bs-url="client-search"
                   data-bs-redirect="socios/catalogo-de-premios"
                   data-bs-toggle="modal" data-bs-target="#searchModal">
                    <div class="card card-dashboard overflow-hidden">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <i class="fas fa-th-list position-relative"></i>
                                </div>
                                <div class="col-8 p-4">
                                    <h6>Catálogo de Certificados</h6>
                                    <p class="mb-0">Consultar el catálogo de certificados que puede redimir un socio</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <footer class="dashboard-footer">
        <div class="footer clearfix mb-0 d-flex justify-content-center">
            <p class="text-center w-50"><?= APP_COPYRIGHT ?></p>
        </div>
    </footer>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<?= $this->endSection() ?>
